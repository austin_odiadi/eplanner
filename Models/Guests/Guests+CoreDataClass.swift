//
//  Guests+CoreDataClass.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 08/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//
//

import Foundation
import CoreData

//@objc(Guests)
public class Guests: NSManagedObject, Codable {

    enum CodingKeys: String, CodingKey {
        case men
        case women
        case isSet
        case contacts
        case children
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        do {
            try container.encode(men , forKey: .men)
            try container.encode(women , forKey: .women)
            try container.encode(isSet , forKey: .isSet)
            try container.encode(contacts , forKey: .contacts)
            try container.encode(children , forKey: .children)
        }
        catch let error {
            print("\(error)")
        }
    }
    
    public required convenience init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let managedObjectContext = decoder.userInfo[contextUserInfoKey] as? StackContext,
            let entity = NSEntityDescription.entity(forEntityName: "Guests", in: managedObjectContext) else {
                fatalError("Failed to decode Guests!")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            men         = try values.decode(Int64?.self, forKey: .men)!
            women       = try values.decode(Int64?.self, forKey: .women)!
            children    = try values.decode(Int64?.self, forKey: .children)!
            isSet       = try values.decodeIfPresent(Int64?.self, forKey: .isSet)!!
            contacts    = Set(try values.decode([Contacts].self, forKey: .contacts))
        }
        catch let error {
            print("\(error)")
        }
    }
}
