//
//  Guest.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 08/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation

protocol GuestDataProtocol: class {
    var title: String? { get set }
    var value: Int64? { get set }
}

class GuestData: GuestDataProtocol {
    var title: String?
    var value: Int64?
    
    init(title: String, value: Int64) {
        self.title = title
        self.value = value
    }
    
    class func data(with event: Event) -> [GuestData] {
        var guest: [GuestData] = [GuestData]()
        
        guest.append(GuestData(title: "Men", value: (event.guests?.men)!))
        guest.append(GuestData(title: "Women", value: (event.guests?.women)!))
        guest.append(GuestData(title: "Children", value: (event.guests?.children)!))
        
        return guest
    }
}

struct Guest: Codable {
    
    var men: Int64?
    var isSet: Int64?
    var women: Int64?
    var children: Int64?
    var contacts: [Contact]?
    var _event = Indirect<Event?>(nil)
    
    var event: Event? {
        get { return _event.value }
        set { _event.value = newValue }
    }
    
    private enum CodingKeys: String, CodingKey {
        case men
        case women
        case isSet
        case event
        case children
        case contacts
    }
    
    init() { }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        do {
            try container.encode(men, forKey: .men)
            try container.encode(women, forKey: .women)
            try container.encode(event, forKey: .event)
            try container.encode(isSet , forKey: .isSet)
            try container.encode(contacts, forKey: .contacts)
            try container.encode(children, forKey: .children)
        }
        catch let error {
            print("\(error)")
        }
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            men         = try values.decodeIfPresent(Int64.self, forKey: .men)
            women       = try values.decodeIfPresent(Int64.self, forKey: .women)
            event       = try values.decodeIfPresent(Event.self, forKey: .event)
            isSet       = try values.decodeIfPresent(Int64.self, forKey: .isSet)
            children    = try values.decodeIfPresent(Int64.self, forKey: .children)
            contacts    = try values.decodeIfPresent([Contact].self, forKey: .contacts)
        }
        catch let error {
            print("\(error)")
        }
    }
}

extension Guest {
    static func defaultGuest() -> Json {
        return [#keyPath(Guests.men): 0,
                #keyPath(Guests.women): 0,
                #keyPath(Guests.isSet): 0,
                #keyPath(Guests.children): 0,
                #keyPath(Guests.contacts): []]
    }
}

extension Array where Element: GuestData {
    
    func updateAsJson() -> Json {
        var guest: Json = Json()
        self.forEach { guest[$0.title!.lowercased()] = $0.value }
        
        return guest
    }
}

extension GuestData {
    func icon() -> UIImage? {
        switch self.title?.lowercased() {
        case "men": return UIImage(named: "manIcon")
        case "women": return UIImage(named: "womanIcon")
        case "children": return UIImage(named: "childrenIcon")
        default: return nil;
        }
    }
}

