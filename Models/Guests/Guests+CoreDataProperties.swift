//
//  Guests+CoreDataProperties.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 08/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//
//

import Foundation
import CoreData


extension Guests {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Guests> {
        return NSFetchRequest<Guests>(entityName: "Guests")
    }
    @NSManaged public var isSet: Int64
    @NSManaged public var men: Int64
    @NSManaged public var women: Int64
    @NSManaged public var children: Int64
    @NSManaged public var event: Events?
    @NSManaged public var contacts: Set<Contacts>?

}

// MARK: Generated accessors for contacts
extension Guests {

    @objc(insertObject:inContactsAtIndex:)
    @NSManaged public func insertIntoContacts(_ value: Contacts, at idx: Int)

    @objc(removeObjectFromContactsAtIndex:)
    @NSManaged public func removeFromContacts(at idx: Int)

    @objc(insertContacts:atIndexes:)
    @NSManaged public func insertIntoContacts(_ values: [Contacts], at indexes: NSIndexSet)

    @objc(removeContactsAtIndexes:)
    @NSManaged public func removeFromContacts(at indexes: NSIndexSet)

    @objc(replaceObjectInContactsAtIndex:withObject:)
    @NSManaged public func replaceContacts(at idx: Int, with value: Contacts)

    @objc(replaceContactsAtIndexes:withContacts:)
    @NSManaged public func replaceContacts(at indexes: NSIndexSet, with values: [Contacts])

    @objc(addContactsObject:)
    @NSManaged public func addToContacts(_ value: Contacts)

    @objc(removeContactsObject:)
    @NSManaged public func removeFromContacts(_ value: Contacts)

    @objc(addContacts:)
    @NSManaged public func addToContacts(_ values: NSOrderedSet)

    @objc(removeContacts:)
    @NSManaged public func removeFromContacts(_ values: NSOrderedSet)

}
