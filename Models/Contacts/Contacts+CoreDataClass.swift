//
//  Contacts+CoreDataClass.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 08/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//
//

import Foundation
import CoreData

//@objc(Contacts)
public class Contacts: NSManagedObject, Codable {

    enum CodingKeys: String, CodingKey {
        case name
        case phoneNumber
        case email
        case guest
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        do {
            try container.encode(name , forKey: .name)
            try container.encode(email , forKey: .email)
            try container.encode(phoneNumber , forKey: .phoneNumber)
        }
        catch { }
    }
    
    public required convenience init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let managedObjectContext = decoder.userInfo[contextUserInfoKey] as? StackContext,
            let entity = NSEntityDescription.entity(forEntityName: "Contacts", in: managedObjectContext) else {
                fatalError("Failed to decode Contacts!")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do {
            name        = try values.decodeIfPresent(String?.self, forKey: .name)!
            email       = try values.decodeIfPresent(String?.self, forKey: .email)!
            phoneNumber = try values.decode(Int64.self, forKey: .phoneNumber)
        }
        catch let error {
            print("\(error)")
        }
    }
}
