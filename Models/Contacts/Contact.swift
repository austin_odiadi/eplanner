//
//  Contact.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 08/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation

protocol ContactDataProtocol: class {
    
    var name: String? { get set }
    var email: String? { get set }
    var phoneNumber: String? { get set }
}

class ContactData: ContactDataProtocol {
    var name: String?
    var email: String?
    var phoneNumber: String?
    
    
    init(name: String?, email: String? = nil, phoneNumber: String? = nil) {
        self.name = name
        self.email = email
        self.phoneNumber = phoneNumber
    }
    
    class func data(with event: Event) -> [ContactData] {
        var contact: [ContactData] = [ContactData]()
        
        event.guests?.contacts?.forEach{
            contact.append(ContactData(name: $0.name, email: $0.email, phoneNumber: "\($0.phoneNumber ?? 0)"))
        }
        
        return contact
    }
}

struct Contact: Codable {
    
    var name: String?
    var email: String?
    var phoneNumber: Int64?
    
    enum CodingKeys: String, CodingKey {
        case name
        case email
        case phoneNumber
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        do {
            try container.encode(name , forKey: .name)
            try container.encode(email , forKey: .email)
            try container.encode(phoneNumber , forKey: .phoneNumber)
        }
        catch let error {
            print("\(error)")
        }
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do {
            name        = try values.decodeIfPresent(String?.self, forKey: .name)!
            email       = try values.decodeIfPresent(String?.self, forKey: .email)!
            phoneNumber = try values.decodeIfPresent(Int64.self, forKey: .phoneNumber)
        }
        catch let error {
            print("\(error)")
        }
    }
}

extension Array where Element: Contacts {
    
    func updateAsJson() -> [Json] {
        var contact: Json = Json()
        var json: [Json] = [Json]()
        
        self.forEach {
            contact[#keyPath(Contacts.name)] = $0.name
            contact[#keyPath(Contacts.email)] = $0.email
            contact[#keyPath(Contacts.phoneNumber)] = $0.phoneNumber
            
            json.append(contact)
        }
        
        return json
    }
}
