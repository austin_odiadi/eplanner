//
//  Contacts+CoreDataProperties.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 08/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//
//

import Foundation
import CoreData


extension Contacts {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contacts> {
        return NSFetchRequest<Contacts>(entityName: "Contacts")
    }

    @NSManaged public var name: String?
    @NSManaged public var phoneNumber: Int64
    @NSManaged public var email: String?
    @NSManaged public var guest: Guests?

}
