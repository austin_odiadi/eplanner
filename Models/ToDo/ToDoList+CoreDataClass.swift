//
//  ToDoList+CoreDataClass.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//
//

import Foundation
import CoreData


public class ToDoList: NSManagedObject, Codable {

    enum CodingKeys: String, CodingKey {
        case id
        case note
        case name
        case date
        case reminder
        case completed
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        do {
            try container.encode(id , forKey: .id)
            try container.encode(note , forKey: .note)
            try container.encode(name , forKey: .name)
            try container.encode(date , forKey: .date)
            try container.encode(reminder , forKey: .reminder)
            try container.encode(completed , forKey: .completed)
        }
        catch { }
    }
    
    public required convenience init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let managedObjectContext = decoder.userInfo[contextUserInfoKey] as? StackContext,
            let entity = NSEntityDescription.entity(forEntityName: "ToDoList", in: managedObjectContext) else {
                fatalError("Failed to decode ToDoList!")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do {
            id          = try values.decodeIfPresent(Int64?.self, forKey: .id)!!
            note        = try values.decodeIfPresent(String?.self, forKey: .note)!
            name        = try values.decodeIfPresent(String?.self, forKey: .name)!
            date        = try values.decodeIfPresent(String?.self, forKey: .date)!
            reminder    = try values.decodeIfPresent(Int64.self, forKey: .reminder)!
            completed   = try values.decodeIfPresent(Int64.self, forKey: .completed)!
        }
        catch let error {
            print("\(error)")
        }
    }
}
