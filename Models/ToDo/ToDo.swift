//
//  ToDo.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation

protocol ToDoDataProtocol: class {
    var title: String? { get set }
    var value: String? { get set }
    var index: Int? { get set }
}

class ToDoData: ToDoDataProtocol {
    var index: Int?
    var value: String?
    var title: String?
    
    init(title: String, value: String, index: Int) {
        self.title = title
        self.value = value
        self.index = index
    }
    
    class func data(with todo: ToDo) -> [[ToDoData]] {
        var toDoData: [[ToDoData]] = [[ToDoData]]()
        
        var description: [ToDoData] = [ToDoData]()
        description.append(ToDoData(title: "To-Do Title", value: todo.name!, index: 0))
        description.append(ToDoData(title: "Due Date", value: todo.date!, index: 1))
        
        var note: [ToDoData] = [ToDoData]()
        note.append(ToDoData(title: "E.g. Remind me to call the tailor", value: todo.note!, index: 2))

        toDoData.append(description)
        toDoData.append(note)
        
        return toDoData
    }
}

extension Array where Element: ToDoData {
    
    func asJson() -> [Json] {
        var todo: Json = Json()
        var json: [Json] = [Json]()
        let index: Int64 = PlannerCommons.ToDoIndex.current()
        
        self.forEach {
            switch $0.index {
            case 0: todo[#keyPath(ToDoList.name)] = $0.value; break
            case 1: todo[#keyPath(ToDoList.date)] = $0.value; break
            case 2: todo[#keyPath(ToDoList.note)] = $0.value; break
            default: break
            }
        }
        
        todo[#keyPath(ToDoList.completed)] = 0
        todo[#keyPath(ToDoList.reminder)] = 0
        todo[#keyPath(ToDoList.id)] = index + 1
        PlannerCommons.ToDoIndex.set(eventIndex: index + 1)
        json.append(todo)
        return json
    }
    
    func updateAsJson() -> Json {
        var todo: Json = Json()
        
        self.forEach {
            switch $0.index {
            case 0: todo[#keyPath(ToDoList.name)] = $0.value; break
            case 1: todo[#keyPath(ToDoList.date)] = $0.value; break
            case 2: todo[#keyPath(ToDoList.note)] = $0.value; break
            default: break
            }
        }
        
        return todo
    }
}

protocol ToDoProtocol {
    static func updateReminderAsJson(reminder: Bool) -> Json
    static func updateCompletedAsJson(completed: Bool) -> Json
}

struct ToDo: Codable {
    var id: Int64?
    var note: String?
    var name: String?
    var date: String?
    var reminder: Int64?
    var completed: Int64?
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case note
        case name
        case date
        case reminder
        case completed
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        do {
            try container.encode(id , forKey: .id)
            try container.encode(note , forKey: .note)
            try container.encode(name , forKey: .name)
            try container.encode(date , forKey: .date)
            try container.encode(reminder , forKey: .reminder)
            try container.encode(completed , forKey: .completed)
        }
        catch { }
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do {
            id          = try values.decodeIfPresent(Int64?.self, forKey: .id)!!
            note        = try values.decodeIfPresent(String?.self, forKey: .note)!
            name        = try values.decodeIfPresent(String?.self, forKey: .name)!
            date        = try values.decodeIfPresent(String?.self, forKey: .date)!
            reminder    = try values.decodeIfPresent(Int64?.self, forKey: .reminder)!
            completed   = try values.decodeIfPresent(Int64?.self, forKey: .completed)!
        }
        catch let error {
            print("\(error)")
        }
    }
}

extension ToDo {
    func subtitle() -> String {
        if (date?.isEmpty)! {
            return "Due date unspecified"
        }
        
        let currentUnixTime = Date().timeIntervalSince1970
        let secondsLeftUntilToDo: TimeInterval = toDoUnixTime() - currentUnixTime
        let countdownTime = CountDownManager.getCountdownTime(from: secondsLeftUntilToDo)
        
        if countdownTime.days > 0 {
            if countdownTime.days > 1 {
                return "DUE IN " + "\(countdownTime.days)" + " days"
            }
            else {
                return "DUE IN " + "\(countdownTime.days)" + " day"
            }
        }
        else {
            let days = abs(countdownTime.days)
            
            if days > 1 {
                return "DUE " + "\(days)" + " days ago"
            }
            else {
                return "DUE " + "\(days)" + " day ago"
            }
        }
    }
    
    func toDoUnixTime() -> TimeInterval {
        guard let endTime = self.date, !endTime.isEmpty else {
            return 0
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return (dateFormatter.date(from: endTime)?.timeIntervalSince1970)!
    }
    
    func statusMatch() -> String{
        if (date?.isEmpty)! {
            return "Open"
        }
        
        let currentUnixTime = Date().timeIntervalSince1970
        let secondsLeftUntilToDo: TimeInterval = toDoUnixTime() - currentUnixTime
        let countdownTime = CountDownManager.getCountdownTime(from: secondsLeftUntilToDo)
        
        return countdownTime.days > 0 ? "Open" : "Overdue"
    }
}

extension ToDo: ToDoProtocol {
    static func updateReminderAsJson(reminder: Bool) -> Json {
        return ["reminder" : reminder ? 1 : 0]
    }
    
    static func updateCompletedAsJson(completed: Bool) -> Json {
        return ["completed" : completed ? 1 : 0]
    }
}
