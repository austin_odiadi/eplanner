//
//  ToDoList+CoreDataProperties.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 14/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//
//

import Foundation
import CoreData


extension ToDoList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ToDoList> {
        return NSFetchRequest<ToDoList>(entityName: "ToDoList")
    }

    @NSManaged public var id: Int64
    @NSManaged public var note: String?
    @NSManaged public var name: String?
    @NSManaged public var date: String?
    @NSManaged public var completed: Int64
    @NSManaged public var reminder: Int64

}
