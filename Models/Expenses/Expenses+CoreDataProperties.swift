//
//  Expenses+CoreDataProperties.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 19/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//
//

import Foundation
import CoreData


extension Expenses {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Expenses> {
        return NSFetchRequest<Expenses>(entityName: "Expenses")
    }

    @NSManaged public var id: Int64
    @NSManaged public var name: String?
    @NSManaged public var cost: Double
    @NSManaged public var paid: Int64
    @NSManaged public var deposit: Double
    @NSManaged public var note: String?

}
