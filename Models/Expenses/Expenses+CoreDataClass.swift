//
//  Expenses+CoreDataClass.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 19/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//
//

import Foundation
import CoreData


public class Expenses: NSManagedObject, Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case note
        case name
        case paid
        case cost
        case deposit
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        do {
            try container.encode(id , forKey: .id)
            try container.encode(note , forKey: .note)
            try container.encode(name , forKey: .name)
            try container.encode(paid , forKey: .paid)
            try container.encode(cost , forKey: .cost)
            try container.encode(deposit , forKey: .deposit)
        }
        catch { }
    }
    
    public required convenience init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let managedObjectContext = decoder.userInfo[contextUserInfoKey] as? StackContext,
            let entity = NSEntityDescription.entity(forEntityName: "Expenses", in: managedObjectContext) else {
                fatalError("Failed to decode Expenses!")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do {
            id      = try values.decodeIfPresent(Int64?.self, forKey: .id)!!
            note    = try values.decodeIfPresent(String?.self, forKey: .note)!
            name    = try values.decodeIfPresent(String?.self, forKey: .name)!
            paid    = try values.decodeIfPresent(Int64?.self, forKey: .paid)!!
            cost    = try values.decodeIfPresent(Double.self, forKey: .cost)!
            deposit = try values.decodeIfPresent(Double.self, forKey: .deposit)!
        }
        catch let error {
            print("\(error)")
        }
    }
}
