//
//  Expense.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 19/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation

protocol ExpenseProtocol: class {
    var title: String? { get set }
    var value: String? { get set }
    var index: Int? { get set }
}

class ExpenseData: ExpenseProtocol {
    
    var title: String?
    var value: String?
    var index: Int?
    
    init(title: String, value: String, index: Int) {
        self.title = title
        self.value = value
        self.index = index
    }
    
    class func data(with expense: Expense? = nil) -> [ExpenseData] {
        var expenseData: [ExpenseData] = [ExpenseData]()
        
        expenseData.append(ExpenseData(title: "Name", value: expense?.name ?? "", index: 0))
        expenseData.append(ExpenseData(title: "Paid?", value: expense?.paidOptionText() ?? "", index: 1))
        expenseData.append(ExpenseData(title: "Deposit", value: expense?.depositText() ?? "", index: 2))
        expenseData.append(ExpenseData(title: "Note", value: expense?.note ?? "", index: 3))
        expenseData.append(ExpenseData(title: "Cost", value: "\(expense?.cost ?? 0)", index: 4))
        
        return expenseData
    }
}

extension Array where Element: ExpenseData {
    
    func asJson() -> [Json] {
        var expense: Json = Json()
        var json: [Json] = [Json]()
        let index: Int64 = PlannerCommons.ExpenseIndex.current()
        
        self.forEach {
            switch $0.index {
            case 0: expense[#keyPath(Expenses.name)] = $0.value; break
            case 1: expense[#keyPath(Expenses.paid)] = expensePaidOption.index(of: $0.value!); break
            case 2: expense[#keyPath(Expenses.deposit)] = $0.value?.currencyUnformat(); break
            case 3: expense[#keyPath(Expenses.note)] = $0.value; break
            case 4: expense[#keyPath(Expenses.cost)] =  $0.value?.currencyUnformat(); break
            default: break
            }
        }

        expense[#keyPath(Expenses.id)] = index + 1
        PlannerCommons.ExpenseIndex.set(eventIndex: index + 1)
        json.append(expense)
        return json
    }
    
    func updateAsJson() -> Json {
        var expense: Json = Json()
    
        self.forEach {
            switch $0.index {
            case 0: expense[#keyPath(Expenses.name)] = $0.value; break
            case 1: expense[#keyPath(Expenses.paid)] = expensePaidOption.index(of: $0.value!); break
            case 2: expense[#keyPath(Expenses.deposit)] = $0.value?.currencyUnformat(); break
            case 3: expense[#keyPath(Expenses.note)] = $0.value; break
            case 4: expense[#keyPath(Expenses.cost)] =  $0.value?.currencyUnformat(); break
            default: break
            }
        }
        return expense
    }
}

struct Expense: Codable {
    var id: Int64?
    var note: String?
    var name: String?
    var paid: Int64?
    var cost: Double?
    var deposit: Double?
    
    enum CodingKeys: String, CodingKey {
        case id
        case note
        case name
        case paid
        case cost
        case deposit
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        do {
            try container.encode(id , forKey: .id)
            try container.encode(note , forKey: .note)
            try container.encode(name , forKey: .name)
            try container.encode(paid , forKey: .paid)
            try container.encode(cost , forKey: .cost)
            try container.encode(deposit , forKey: .deposit)
        }
        catch { }
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do {
            id      = try values.decodeIfPresent(Int64?.self, forKey: .id)!!
            note    = try values.decodeIfPresent(String?.self, forKey: .note)!
            name    = try values.decodeIfPresent(String?.self, forKey: .name)!
            paid    = try values.decodeIfPresent(Int64?.self, forKey: .paid)!!
            cost    = try values.decodeIfPresent(Double.self, forKey: .cost)!
            deposit = try values.decodeIfPresent(Double.self, forKey: .deposit)!
        }
        catch let error {
            print("\(error)")
        }
    }
}

extension Expense {
    func paidOptionText() -> String {
        
        return expensePaidOption[Int(paid!)]
    }
    
    func depositText() -> String {
        let dp: Double = Double(deposit!)
        return dp > 0 ? "\(dp)".currencyFormat() : ""
    }
}
