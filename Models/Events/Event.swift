//
//  Event.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation

protocol EventDataProtocol: class {
    var title: String? { get set }
    var value: String? { get set }
    var icon: UIImage? { get set }
    var type: EventRows? { get set }
}

class EventData: EventDataProtocol {
    
    var icon: UIImage?
    var title: String?
    var value: String?
    var type: EventRows?
    
    init(event: Event? = nil, type: EventRows? = nil) {
        switch type {
        case .eventTitle?:
            self.value = event?.title ?? ""
            break;
        case .eventType?:
            self.title = "Type"
            self.value = event?.formattedType()
            self.icon = UIImage(named: "eventIcon")
            break
        case .eventTime?:
            self.title = "Time"
            self.value = event?.formattedTime()
            self.icon = UIImage(named: "timeIcon")
            break
        case .eventDate?:
            self.title = "Date"
            self.value = event?.formattedDate()
            self.icon = UIImage(named: "dateIcon")
            break
        case .eventDateAndTime?:
            self.title = "When"
            self.value = event?.dateAndTime()
            break
        default: break
        }
        
        self.type  = type
    }
    
    class func data(with type: [EventRows], event: Event) -> [EventData] {
        var eventData: [EventData] = [EventData]()
        
        type.forEach {
            eventData.append(EventData(event: event, type: $0))
        }
        
        return eventData
    }
}

extension Array where Element: EventData {
    
    func asJson() -> [Json] {
        var event: Json = Json()
        var json: [Json] = [Json]()
        var index: Int64 = PlannerCommons.EventIndex.current()
        
        event[#keyPath(Events.guests)] = Guest.defaultGuest()
        event[#keyPath(Events.calculations)] = Calculation.defaultCalculation()
        self.forEach {
            switch $0.type {
            case .eventTitle?:       event[#keyPath(Events.title)] = $0.value; break
            case .eventType?:        event[#keyPath(Events.type)] = $0.value; break
            case .eventDateAndTime?: event[#keyPath(Events.date)] = $0.value; break
            default: break
            }
            
            index = index + 1
            event[#keyPath(Events.id)] = index
            event[#keyPath(Events.image)] = "eventDrinksImg"
        }
        
        json.append(event)
        PlannerCommons.EventIndex.set(eventIndex: index)
        return json
    }
    
    func updateAsJson() -> Json {
        var event: Json = Json()
        self.forEach {
            switch $0.type {
            case .eventTitle?:       event[#keyPath(Events.title)] = $0.value; break
            case .eventType?:        event[#keyPath(Events.type)] = $0.value; break
            case .eventDateAndTime?: event[#keyPath(Events.date)] = $0.value; break
            default: break
            }
        }
        
        return event
    }
}

struct Event: Codable {
    
    var id: Int64?
    var type: String?
    var date: String?
    var title: String?
    var image: String?
    var guests: Guest?
    var calculation: Calculation?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case type
        case date
        case title
        case image
        case guests
        case calculation
    }
    
    init() { }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        do {
            try container.encode(id, forKey: .id)
            try container.encode(type, forKey: .type)
            try container.encode(date, forKey: .date)
            try container.encode(title, forKey: .title)
            try container.encode(image, forKey: .image)
            try container.encode(guests, forKey: .guests)
            try container.encode(calculation, forKey: .calculation)
        }
        catch let error {
            print("\(error)")
        }
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            guests      = try values.decode(Guest.self, forKey: .guests)
            id          = try values.decodeIfPresent(Int64.self, forKey: .id)
            type        = try values.decodeIfPresent(String.self, forKey: .type)
            date        = try values.decodeIfPresent(String.self, forKey: .date)
            title       = try values.decodeIfPresent(String.self, forKey: .title)
            image       = try values.decodeIfPresent(String.self, forKey: .image)
            calculation = try values.decodeIfPresent(Calculation.self, forKey: .calculation)
        }
        catch let error {
            print("\(error)")
        }
    }
}

extension Event {
    func time() -> String {
        return displayTimeFormater(self.date)
    }
    
    func formattedTime() -> String {
        guard self.date != optionDisplayText else {
            return time()
        }
        
       return "from " + time()
    }
    
    func formattedDate() -> String {
        return  displayDateFormater(self.date)
    }
    
    func dateAndTime() -> String {
        return self.date ?? optionDisplayText
    }
    
    func formattedType() -> String {
        return self.type ?? optionDisplayText
    }
    
    func displayDateFormater(_ date: String?) -> String {
        guard let dateStr: String = date, !dateStr.isEmpty, dateStr != optionDisplayText else {
            return "Not Specified"
        }

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = "EEEE, MMM d, yyyy"
        return  dateFormatter.string(from: date!)
        
    }
    
    func displayTimeFormater(_ time: String?) -> String {
        guard let timeStr: String = time, !timeStr.isEmpty, timeStr != optionDisplayText else {
            return "Not Specified"
        }
        
        return timeStr.components(separatedBy: " ").last!
    }
}
