//
//  Events+CoreDataClass.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 02/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//
//

import Foundation
import CoreData

public class Events: NSManagedObject, Codable {

    enum CodingKeys: String, CodingKey {
        case id
        case type
        case date
        case title
        case image
        case guests
        case calculations
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        do {
            try container.encode(id , forKey: .id)
            try container.encode(type , forKey: .type)
            try container.encode(date , forKey: .date)
            try container.encode(title , forKey: .title)
            try container.encode(image , forKey: .image)
            try container.encode(guests , forKey: .guests)
            try container.encode(calculations , forKey: .calculations)
        }
        catch { }
    }
    
    public required convenience init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
              let managedObjectContext = decoder.userInfo[contextUserInfoKey] as? StackContext,
              let entity = NSEntityDescription.entity(forEntityName: "Events", in: managedObjectContext) else {
                fatalError("Failed to decode Events!")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do {
            id              = try values.decode(Int64?.self, forKey: .id)!
            type            = try values.decodeIfPresent(String?.self, forKey: .type)!
            date            = try values.decodeIfPresent(String?.self, forKey: .date)!
            title           = try values.decodeIfPresent(String?.self, forKey: .title)!
            image           = try values.decodeIfPresent(String?.self, forKey: .image)!
            guests          = try values.decodeIfPresent(Guests?.self, forKey: .guests)!
            calculations    = try values.decodeIfPresent(Calculations?.self, forKey: .calculations)!
            
        }
        catch let error {
            print("\(error)")
        }
    }
}
