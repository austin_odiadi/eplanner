//
//  Events+CoreDataProperties.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 15/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//
//

import Foundation
import CoreData


extension Events {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Events> {
        return NSFetchRequest<Events>(entityName: "Events")
    }

    @NSManaged public var date: String?
    @NSManaged public var id: Int64
    @NSManaged public var title: String?
    @NSManaged public var type: String?
    @NSManaged public var image: String?
    @NSManaged public var guests: Guests?
    @NSManaged public var calculations: Calculations?

}
