//
//  Calculations+CoreDataClass.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 15/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//
//

import Foundation
import CoreData


public class Calculations: NSManagedObject, Codable {

    enum CodingKeys: String, CodingKey {
        case isSet
        case wine
        case beer
        case juice
        case water
        case liquor
        case duration
        case behavoir
        case sparkling
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        do {
            try container.encode(wine , forKey: .wine)
            try container.encode(beer , forKey: .beer)
            try container.encode(juice , forKey: .juice)
            try container.encode(water , forKey: .water)
            try container.encode(liquor , forKey: .liquor)
            try container.encode(isSet , forKey: .isSet)
            try container.encode(duration , forKey: .duration)
            try container.encode(behavoir , forKey: .behavoir)
            try container.encode(sparkling , forKey: .sparkling)
        }
        catch let error {
            print("\(error)")
        }
    }
    
    public required convenience init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let managedObjectContext = decoder.userInfo[contextUserInfoKey] as? StackContext,
            let entity = NSEntityDescription.entity(forEntityName: "Calculations", in: managedObjectContext) else {
                fatalError("Failed to decode Calculations!")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do {
            wine        = try values.decodeIfPresent(Int64?.self, forKey: .wine)!!
            beer        = try values.decodeIfPresent(Int64?.self, forKey: .beer)!!
            water       = try values.decodeIfPresent(Int64?.self, forKey: .water)!!
            juice       = try values.decodeIfPresent(Int64?.self, forKey: .juice)!!
            liquor      = try values.decodeIfPresent(Int64?.self, forKey: .liquor)!!
            isSet       = try values.decodeIfPresent(Int64?.self, forKey: .isSet)!!
            duration    = try values.decodeIfPresent(Int64?.self, forKey: .duration)!!
            behavoir    = try values.decodeIfPresent(Int64?.self, forKey: .behavoir)!!
            sparkling   = try values.decodeIfPresent(Int64?.self, forKey: .sparkling)!!
            
        }
        catch let error {
            print("\(error)")
        }
    }
}
