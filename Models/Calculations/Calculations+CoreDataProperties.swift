//
//  Calculations+CoreDataProperties.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 15/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//
//

import Foundation
import CoreData


extension Calculations {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Calculations> {
        return NSFetchRequest<Calculations>(entityName: "Calculations")
    }

    @NSManaged public var isSet: Int64
    @NSManaged public var wine: Int64
    @NSManaged public var beer: Int64
    @NSManaged public var liquor: Int64
    @NSManaged public var sparkling: Int64
    @NSManaged public var juice: Int64
    @NSManaged public var water: Int64
    @NSManaged public var duration: Int64
    @NSManaged public var behavoir: Int64
    @NSManaged public var event: Events?

}
