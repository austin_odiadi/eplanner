//
//  Calculation.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 15/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation

protocol CalculationDataProtocol: class {

    var title: String? { get set }
    var value: Int64? { get set }
    var icon: UIImage? { get set }
    var type: CalculationTypes? { get set }
}

class CalculationData: CalculationDataProtocol {
    
    var id: Int64?
    var icon: UIImage?
    var title: String?
    var value: Int64?
    var type: CalculationTypes?
    
    init(id: Int64? = 0, title: String? = nil, value: Int64, type: CalculationTypes) {
        self.id    = id
        self.title = title
        self.value = value
        self.type  = type
        self.icon  = UIImage(named: type.icon())
    }
    
    class func data(with calculation: Calculation) -> [[CalculationData]] {
        return getCalculation(with: calculation)
    }
}

extension CalculationData {
    
    class func newCalculation() -> [[CalculationData]] {
        return getCalculation()
    }
    
    class func getCalculation(with calculation: Calculation? = nil) -> [[CalculationData]] {
        var calculationData: [[CalculationData]] = [[CalculationData]]()
        
        
        var drinks: [CalculationData] = [CalculationData]()
        drinks.append(CalculationData(title: "Wine", value: calculation?.wine ?? 0, type: .wine))
        drinks.append(CalculationData(title: "Beer", value: calculation?.beer ?? 0, type: .beer))
        drinks.append(CalculationData(title: "Juice", value: calculation?.juice ?? 0, type: .juice))
        drinks.append(CalculationData(title: "Water", value: calculation?.water ?? 0, type: .water))
        drinks.append(CalculationData(title: "Liquor", value: calculation?.liquor ?? 0, type: .liquor))
        drinks.append(CalculationData(title: "Sparkling", value: calculation?.sparkling ?? 0, type: .sparkling))
        calculationData.append(drinks)
        
        calculationData.append([CalculationData(value: calculation?.duration ?? 0, type: .duration)])
        calculationData.append([CalculationData(value: calculation?.behavoir ?? 0, type: .behavoir)])
        
        return calculationData
    }
}

extension CalculationData {
    class func deleteAsJson() -> Json {
        return[#keyPath(Calculations.isSet): 0]
    }
}

extension Array where Element: CalculationData {
    
    func asJson() -> Json {
        var calculation: Json = Json()
    
        self.forEach {
            switch $0.type {
            case .wine?: calculation[#keyPath(Calculations.wine)] = $0.value; break
            case .beer?: calculation[#keyPath(Calculations.beer)] = $0.value; break
            case .juice?: calculation[#keyPath(Calculations.juice)] = $0.value; break
            case .water?: calculation[#keyPath(Calculations.water)] = $0.value; break
            case .liquor?: calculation[#keyPath(Calculations.liquor)] = $0.value; break
            case .duration?: calculation[#keyPath(Calculations.duration)] = $0.value; break
            case .behavoir?: calculation[#keyPath(Calculations.behavoir)] = $0.value; break
            case .sparkling?: calculation[#keyPath(Calculations.sparkling)] = $0.value; break
            default: break
            }
        }
        
        calculation[#keyPath(Calculations.isSet)] = 1
        return calculation
    }
    
    func updateAsJson() -> Json {
        return self.asJson()
    }
}

struct Calculation: Codable {
    
    var wine: Int64?
    var beer: Int64?
    var juice: Int64?
    var water: Int64?
    var liquor: Int64?
    var isSet: Int64?
    var duration: Int64?
    var behavoir: Int64?
    var sparkling: Int64?
    var _event = Indirect<Event?>(nil)
    
    var event: Event? {
        get { return _event.value }
        set { _event.value = newValue }
    }
    
    enum CodingKeys: String, CodingKey {
        case wine
        case beer
        case juice
        case water
        case liquor
        case isSet
        case event
        case duration
        case behavoir
        case sparkling
    }
    
    init() { }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        do {
            try container.encode(wine , forKey: .wine)
            try container.encode(beer , forKey: .beer)
            try container.encode(isSet , forKey: .isSet)
            try container.encode(juice , forKey: .juice)
            try container.encode(water , forKey: .water)
            try container.encode(event , forKey: .event)
            try container.encode(liquor , forKey: .liquor)
            try container.encode(duration , forKey: .duration)
            try container.encode(behavoir , forKey: .behavoir)
            try container.encode(sparkling , forKey: .sparkling)
        }
        catch let error {
            print("\(error)")
        }
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            event       = try values.decode(Event.self, forKey: .event)
            wine        = try values.decodeIfPresent(Int64?.self, forKey: .wine)!!
            beer        = try values.decodeIfPresent(Int64?.self, forKey: .beer)!!
            water       = try values.decodeIfPresent(Int64?.self, forKey: .water)!!
            juice       = try values.decodeIfPresent(Int64?.self, forKey: .juice)!!
            liquor      = try values.decodeIfPresent(Int64?.self, forKey: .liquor)!!
            isSet       = try values.decodeIfPresent(Int64?.self, forKey: .isSet)!!
            duration    = try values.decodeIfPresent(Int64?.self, forKey: .duration)!!
            behavoir    = try values.decodeIfPresent(Int64?.self, forKey: .behavoir)!!
            sparkling   = try values.decodeIfPresent(Int64?.self, forKey: .sparkling)!!
        }
        catch let error {
            print("\(error)")
        }
    }
}

extension Calculation {
    static func defaultCalculation() -> Json {
        return [#keyPath(Calculations.isSet): 0,
                #keyPath(Calculations.wine): 0,
                #keyPath(Calculations.beer): 0,
                #keyPath(Calculations.water): 0,
                #keyPath(Calculations.juice): 0,
                #keyPath(Calculations.liquor): 0,
                #keyPath(Calculations.duration): 0,
                #keyPath(Calculations.behavoir): 0,
                #keyPath(Calculations.sparkling): 0]
    }
    
    func numberOfDrinks() -> String {
        return "\(wine! + beer! + water! + juice! + liquor! + sparkling!)"
    }
    
    func drinkHabit() -> String {
        return (DrinkingHabit(rawValue: behavoir!)?.description())!
    }
    
    func durationValue() -> String {
        return (EventDuration(rawValue: duration!)?.description())!
    }
    
    func fullDurationValue() -> String {
        return (EventDuration(rawValue: duration!)?.fullDescription())!
    }
}

