//
//  PlannerConfigs.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 31/03/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation


typealias EventType = String

let optionDisplayText               = "Select"
let kEventIndex                     = "EventIndex"
let kToDoIndex                      = "ToDoIndex"
let kExpenseIndex                   = "ExpenseIndex"
let kInitialLoginCompleted          = "IntialLogin"
let kEventIsLive                    = "Event started!"
let kEventIsEnded                   = "Event ended!"
let eventDetailsIdentifier          = "EventDetailsCellIdentifier"
let editGuestIdentifier             = "EditGuestsCellIdentifier"
let contactsIdentifier              = "contactIdentifier"
let labelIdentifier                 = "LabelCellIdentifier"
let textFieldIdentifier             = "TextFieldCellIdentifier"
let todoIdentifier                  = "ToDoIdentifier"
let drinksIdentifier                = "DrinksCellIdentifier"
let sliderIdentifier                = "sliderIdentifier"
let calculationCellIdentifier       = "CalculationCellIdentifier"
let calculationDetailIdentifer      = "CalculationDetailIdentifer"
let expensesCellIdentifier          = "ExpensesCellIdentifier"

// In order of appearance
let eventTypes: [EventType] = ["Birthday Party",
                               "Barbecue Party",
                               "Theme Party",
                               "Children's Birthday Party",
                               "Wedding",
                               "Silver Wedding",
                               "Golden Wedding",
                               "Stag & Hen Night",
                               "Anniversary",
                               "Baptism",
                               "Communion & Confirmation",
                               "Youth Invitation",
                               "Other"]

enum EventIcons: Int {
    case birthday = 0
    case barbecue
    case theme
    case childrensParty
    case wedding
    case silverWedding
    case goldenWedding
    case stagAndHenNight
    case anniversary
    case baptism
    case communion
    case youth
    case other
    
    func iconName() -> String {
        switch self {
        case .birthday: return "eventBirthdayImg"
        case .barbecue: return "barbecueImg"
        case .theme: return "themeImg"
        case .childrensParty: return "childrensPartyImg"
        case .wedding: return "weddingImg"
        case .silverWedding: return "sliverWeddingImg"
        case .goldenWedding: return "goldWeddingImg"
        case .stagAndHenNight: return "stagHenImg"
        case .anniversary: return "anniversaryImg"
        case .baptism: return "baptismImg"
        case .communion: return "communionImg"
        case .youth: return "youthImg"
        case .other: return "otherImg"
        }
    }
    
    static func icon(for index: Int) -> UIImage {
        return UIImage(named: (EventIcons(rawValue: index)?.iconName())!)! 
    }
}


let expensePaidOption = ["No", "Yes", "Deposit"]

let defaultTableRowHeight: CGFloat = 51.0
let defaultTableSectionHeight: CGFloat = 40.0

enum PlannerViewController {
    case none, first, second, third, fourth, splash, surf, mainTab, events, todoList, guests, expenses, calculations, createEvent, eventDetails, editGuests, contacts, createToDo, toDoDetails, createCalculation, calculationDetail, createExpense, expenseDetail
    
    func description() -> String {
        switch self {
        case .none:         return ""
        case .splash:       return "Splash"
        case .surf:         return "Surf"
        case .first:        return "First"
        case .second:       return "Second"
        case .third:        return "Third"
        case .fourth:       return "Fourth"
        case .mainTab:      return "MainTabBar"
        case .events:       return "Events"
        case .todoList:     return "ToDoList"
        case .guests:       return "Guests"
        case .expenses:     return "Expenses"
        case .calculations: return "Calculations"
        case .createEvent:  return "CreateEvent"
        case .eventDetails: return "EventDetails"
        case .editGuests:   return "EditGuests"
        case .contacts:     return "Contacts"
        case .createToDo:   return "CreateToDo"
        case .toDoDetails:  return "ToDoDetails"
        case .createExpense:    return "CreateExpense"
        case .expenseDetail:    return "ExpenseDetail"
        case .createCalculation:    return "CreateCalculation"
        case .calculationDetail:    return "CalculationDetail"
        }
    }
}

enum DataStackSaveError: Error {
    case failed
    case noChanges
}

enum DataStackCommonsSaveError: Error {
    case decodeFailure
    case serializationFailure
}

enum EventRows: Int {
    case eventTitle
    case eventType
    case eventDate
    case eventTime
    case eventDateAndTime
}

enum CreateEventLoadType: Int {
    case new
    case existing
}

enum CreateToDoLoadType: Int {
    case new
    case existing
}

enum PickerType: Int {
    case date
    case custom
}

enum CalculationTypes: Int {
    case wine
    case beer
    case juice
    case water
    case liquor
    case sparkling
    case duration
    case behavoir
    case id
    
    func icon() -> String {
        switch self {
        case .id: return ""
        case .wine: return "wine"
        case .beer: return "beer"
        case .juice: return "juice"
        case .water: return "water"
        case .liquor: return "liquor"
        case .duration: return "durationIcon"
        case .behavoir: return "drinkBehaviourIcon"
        case .sparkling: return "sparkling"
        }
    }
}

enum DrinkingHabit: Int64 {
    case veryFew = 0
    case few
    case normal
    case plenty
    case binge
    
    func description() -> String {
        switch self {
        case .veryFew: return "FEW"
        case .few: return "FEW"
        case .normal: return "NORMAL"
        case .plenty: return "PLENTY"
        case .binge: return "BINGE"
        }
    }
}

enum EventDuration: Int64 {
    case two = 0
    case twoToFour
    case fourToSix
    case sixToEight
    case openEnd
    
    func description() -> String {
        switch self {
        case .two: return "2"
        case .twoToFour: return "2-4"
        case .fourToSix: return "4-6"
        case .sixToEight: return "6-8"
        case .openEnd: return "OPEN"
        }
    }
    
    func fullDescription() -> String {
        switch self {
        case .two: return "2 HOURS"
        case .twoToFour: return "2-4 HOURS"
        case .fourToSix: return "4-6 HOURS"
        case .sixToEight: return "6-8 HOURS"
        case .openEnd: return "OPEN END"
        }
    }
}

enum CreateCalculationLoadType: Int {
    case new
    case existing
}

enum CreateExpenseLoadType: Int {
    case new
    case existing
}

