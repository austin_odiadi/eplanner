//
//  PlannerCommons.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 31/03/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

final class PlannerCommons {
    
    class func initialLoadComplete() -> Promise<Bool> {
        return Promise.value(UserDefaults.standard.bool(forKey: kInitialLoginCompleted))
    }
    
    class func setInitialLoadComplete(loaded: Bool) -> Guarantee<Bool> {
        return Guarantee {_ in
             UserDefaults.standard.set(loaded, forKey: kInitialLoginCompleted)
        }
    }
    
    class func format(date: Date) -> String? {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let dateString: String = dateFormatterGet.string(from: date) as String? {
            return dateString
        }
        
        return nil
    }
    
    class EventIndex {
        class func current() -> Int64 {
            return Int64(UserDefaults.standard.integer(forKey: kEventIndex))
        }
        
        class func set(eventIndex index: Int64) {
            UserDefaults.standard.set(index, forKey: kEventIndex)
        }
    }
    
    class ToDoIndex {
        class func current() -> Int64 {
            return Int64(UserDefaults.standard.integer(forKey: kToDoIndex))
        }
        
        class func set(eventIndex index: Int64) {
            UserDefaults.standard.set(index, forKey: kToDoIndex)
        }
    }
    
    class ExpenseIndex {
        class func current() -> Int64 {
            return Int64(UserDefaults.standard.integer(forKey: kExpenseIndex))
        }
        
        class func set(eventIndex index: Int64) {
            UserDefaults.standard.set(index, forKey: kExpenseIndex)
        }
    }

    class View {
        
        class func show(view: UIView, show: Bool, animated: Bool) {
            if animated {
                view.alpha = show ? 0.0 : 1.0
                view.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    view.alpha = show ? 1.0 : 0.0
                }, completion:nil)
            }
            else{
                view.isHidden = !show
            }
        }
    }
}

public extension UIView {
    func border(color: UIColor, size: CGSize, fill: UIColor? = .clear, lineWidth: CGFloat) {
        self.layer.sublayers?.forEach {
            if $0.name == "BorderName" { $0.removeFromSuperlayer() }
        }
        
        let borderShape         = CAShapeLayer()
        borderShape.name        = "BorderName"
        borderShape.frame       = self.bounds
        borderShape.path        = UIBezierPath(roundedRect:self.bounds, byRoundingCorners:[UIRectCorner.topLeft, UIRectCorner.topRight, UIRectCorner.bottomLeft, UIRectCorner.bottomRight], cornerRadii:size).cgPath
        borderShape.strokeColor = color.cgColor
        borderShape.fillColor   = fill!.cgColor
        borderShape.lineWidth   = lineWidth
        self.layer.addSublayer(borderShape)
    }
    
    func makeCircular(){
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }
    
    func roundedCorners(radius: CGFloat? = 2) {
        self.layer.cornerRadius = radius!
        self.clipsToBounds = true
    }
    
    @discardableResult
    func loadNib<T : UIView>() -> T? {
        guard let view = Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?[0] as? T else {
            return nil
        }
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|[view]|", options: [], metrics:nil, views:["view":view]))
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|[view]|", options: [], metrics:nil, views:["view":view]))
        return view
    }
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize? = CGSize(width: -1, height: 1), radius: CGFloat = 1, scale: Bool = true) {
        self.layer.masksToBounds        = false
        self.layer.shadowColor          = color.cgColor
        self.layer.shadowOpacity        = opacity
        self.layer.shadowOffset         = offSet!
        self.layer.shadowRadius         = radius
        
        self.layer.shadowPath           = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize      = true
        self.layer.rasterizationScale   = scale ? UIScreen.main.scale : 1
    }
    
    func viewSizeToFit() {
        self.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|[view]|", options: [], metrics:nil, views:["view":self]))
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|[view]|", options: [], metrics:nil, views:["view":self]))
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    class func plannerBackgroundColor() -> UIColor {
        return UIColor(red: 225/255, green: 225/255, blue: 232/255, alpha: 1)
    }
}

extension NSLayoutConstraint {
    /**
     Change multiplier constraint
     
     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
     */
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem!,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}

extension String {
    func currencyFormat() -> String {
        
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        
        if let integer = Double(self) {
            let number = NSNumber(value: integer)
            return formatter.string(from: number)!
        }
        
        return self
    }
    func currencyUnformat() -> Double {
        
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        formatter.currencySymbol = Locale.current.currencySymbol
        formatter.decimalSeparator = Locale.current.groupingSeparator
        return formatter.number(from: self)?.doubleValue ?? 0.00
    }
    
    func removeCurrencyFormatIfAny() -> String {
        if let first = self.first, let firstStr = "\(first)" as? String {
            if firstStr == Locale.current.currencySymbol {
                let val = self.currencyUnformat()
     
                if val > 0 {
                    return String(val)
                }
                else {
                    var newStr = self.replacingOccurrences(of: ",", with: "")
                    newStr = newStr.replacingOccurrences(of: ".", with: "")
                    
                    return String(newStr.dropFirst())
                }
            }
        }
        
        return self
    }
   
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
    
    struct NumFormatter {
        static let instance = NumberFormatter()
    }
    
    var doubleValue: Double? {
        return NumFormatter.instance.number(from: self)?.doubleValue
    }
    
    var integerValue: Int? {
        return NumFormatter.instance.number(from: self)?.intValue
    }
}
