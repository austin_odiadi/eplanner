//
//  PlannerTableView.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 04/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
protocol PLTableViewDelegate: class {
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath, object: Any?)
}

class PLTableView: UIView {
    
    var tableData: PLTableData?
    weak var delegate: PLTableViewDelegate?
    @IBOutlet private weak var tableView: UITableView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadNib()
        
        initialize()
    }
    
    func initialize() {
        self.tableView.tableFooterView = UIView()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")

    }
    
    func loadData(data: PLTableData?) {
        self.tableData = data
        
        self.tableData?.registeredCell!(tableView)
        self.tableView.reloadData()
    }
    
    func update(object: Any, at indexPath: IndexPath) {
        if var data = tableData?.data as [Any]? {
            tableData?.updater!(&data, object, indexPath)
        }
        
        tableView.reloadData()
    }
}

extension PLTableView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData?.numberOfRow(at: section) ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableData?.numberOfSection() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return (tableData?.configuredCell!(self, tableView, indexPath, tableData?.data))!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        /*
        if #available(iOS 11, *) {
            tableView.layoutMargins = .zero
            tableView.separatorInset = .zero
            tableView.preservesSuperviewLayoutMargins = false
        }
        */
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableData?.sizes.rowHeight ?? 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.tableView(tableView, didSelectRowAt: indexPath, object: tableData?.data?[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        delegate?.tableView(tableView, didDeselectRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return tableData?.viewForSection(at: section, inView: self)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return tableData?.heightForHeader(at: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return tableData?.heightForFooter(at: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        view.tintColor = .clear
    }
}

extension PLTableView: TextfieldTableViewCellDelegate {
    func cell(_ cell: TextfieldTableViewCell, didBeginEditing title: String) {
    }
    
    func cell(_ cell: TextfieldTableViewCell, didEndEditing title: String) {
        
        if let indexPath = tableView.indexPath(for: cell), var data = tableData?.data as [Any]? {
            tableData?.updater!(&data, title, indexPath)
        }
    }
}

extension PLTableView {
    func scroll(enable: Bool) {
        tableView.isScrollEnabled = enable
    }
    
    func reloadData() {
        tableView.reloadData()
    }
}
