//
//  PLTableData.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 04/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation

typealias IndexForSection = (_ dataSource: [Any]) ->(Int)?
typealias ViewForSection = (_ view: PLTableView, _ dataSource: [Any], _ section: Int) ->(UIView)?
typealias IndexForRow = (_ dataSource: [Any], _ section: Int) ->(Int)?
typealias CellRegistration = (_ tableView: UITableView) ->(Void)?
typealias DataSourceUpdate = (_ dataSource: inout [Any], _ update: Any, _ indexPath: IndexPath) ->(Void)?
typealias CellConfiguration = (_ view: PLTableView, _ tableView: UITableView, _ indexPath: IndexPath, _ data: Any?) ->(UITableViewCell)?

final class PLTableDataSize {
    var rowHeight: CGFloat = defaultTableRowHeight
    var footerHeight: CGFloat = 0
    var headerHeight: CGFloat = defaultTableSectionHeight
    
    init() { }
    
    init(_ rowHeight: CGFloat, _ headerHeight: CGFloat? = 0, _ footerHeight: CGFloat? = 0) {
        self.rowHeight = rowHeight
        self.headerHeight = headerHeight!
        self.footerHeight = footerHeight!
    }
}

final class PLTableData: NSCopying {
    
    var data: [Any]?
    var sizes: PLTableDataSize
    var updater: DataSourceUpdate?
    var registeredCell: CellRegistration?
    var configuredCell: CellConfiguration?
    
    internal var indexForRow: IndexForRow?
    internal var indexForSection: IndexForSection?
    internal var viewForSection: ViewForSection?
    
    init(data: [Any]? = nil,
         configuredCell: CellConfiguration? = nil,
         registeredCell: CellRegistration? = nil,
         sizes: PLTableDataSize? = nil,
         updater: DataSourceUpdate? = nil,
         indexForRow: IndexForRow? = nil,
         indexForSection: IndexForSection? = nil,
         viewForSection: ViewForSection? = nil) {
        
        self.data = data
        self.updater = updater
        self.sizes = sizes ?? PLTableDataSize()
        self.configuredCell = configuredCell
        self.registeredCell = registeredCell
        self.indexForRow = indexForRow
        self.indexForSection = indexForSection
        self.viewForSection = viewForSection
    }
    
    func numberOfRow(at section: Int) -> Int? {
        if (indexForRow != nil), let dataSource = data {
            return indexForRow!(dataSource, section)
        }
        
        return data?.count
    }
    
    func numberOfSection() -> Int? {
        if (indexForSection != nil), let dataSource = data {
            return indexForSection!(dataSource)
        }
        
        return 1
    }
    
    func viewForSection(at section: Int, inView: PLTableView) -> UIView? {
        if (viewForSection != nil), let dataSource = data {
            return viewForSection!(inView, dataSource, section)
        }
        
        return nil
    }
    
    func heightForHeader(at section: Int) -> CGFloat {
        if (viewForSection == nil) {
            return 0
        }
        
        return sizes.headerHeight
    }
    
    func heightForFooter(at section: Int) -> CGFloat {
        return sizes.footerHeight
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = PLTableData(data: data,
                               configuredCell: configuredCell,
                               registeredCell: registeredCell,
                               sizes: sizes,
                               updater: updater,
                               indexForRow: indexForRow,
                               indexForSection: indexForSection,
                               viewForSection: viewForSection)
        return copy
    }
}
