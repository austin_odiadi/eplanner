//
//  PickerView.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 05/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

protocol PickerViewDelegate: class {
    func picker(_ view: PickerView, _ selection: Any?, type: PickerType)
    func picker(_ view: PickerView, _ selection: Any?, _ done: Bool, _ type: PickerType)
}

class PickerView: UIView {
    
    var picker: Picker<Any>?
    var pickerType: PickerType?
    var datePicker: UIDatePicker?
    weak var delegate: PickerViewDelegate?
    @IBOutlet weak var textField: UITextField!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }
    
    func loadPicker(picker: Picker<Any>) {
        self.picker = picker
        self.picker?.pickerDelegate = self
        
        pickerType = .custom
        textField.inputAccessoryView = toolBar()
        textField.inputView = self.picker
        
        
        textField.becomeFirstResponder()
    }
    
    func loadDatePicker(picker: UIDatePicker) {
        datePicker = picker
        
        pickerType = .date
        textField.inputAccessoryView = toolBar()
        textField.inputView = datePicker
        
        datePicker?.addTarget(self, action: .datePickerChanged, for: UIControl.Event.valueChanged)
        textField.becomeFirstResponder()
    }
    
    convenience init(picker: UIDatePicker?) {
        self.init(frame: .zero)
    }
    
    @objc func doneButton() {
        var picked: Any? = nil
        
        switch pickerType {
        case .date?:
            picked = datePicker?.date
            break
        case .custom?:
            picked = picker?.selectedItem
            break
        case .none:
            break
        }
        
        delegate?.picker(self, picked, true, pickerType!)
    }
    
    @objc func datePickerChanged() {
        delegate?.picker(self, datePicker?.date, type: pickerType!)
    }
    
    func toolBar() -> UIToolbar {
        let toolbar: UIToolbar = UIToolbar();
        toolbar.sizeToFit()

        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: .doneButton)
        toolbar.setItems([spaceButton, doneButton], animated: false)
        
        return toolbar
    }
}

extension PickerView: PickerDelegate {
    
    func picker(picker: Picker<Any>, didSelectItem item: Any, atIndex: Int) {
        delegate?.picker(self, item, type: pickerType!)
    }
}

fileprivate extension Selector {
    static let doneButton = #selector(PickerView.doneButton)
    static let datePickerChanged = #selector(PickerView.datePickerChanged)
    
}
