//
//  Picker.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 05/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation

protocol PickerDelegate: class {
    func picker(picker: Picker<Any>, didSelectItem item: Any, atIndex: Int)
}

class PickerSource : NSObject, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var data: [[String]] = []
    var seletedIndexPath: IndexPath?
    weak var pickerDelegate: PickerDelegate?
    var selectionUpdated: ((_ component: Int, _ row: Int) -> Void)?
    
    // MARK: UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data[component].count
    }
    
    // MARK: UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(data[component][row])"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selectionUpdated?(component, row)
        seletedIndexPath = IndexPath(row: row, section: component)
        pickerDelegate?.picker(picker: pickerView as! Picker<Any>, didSelectItem: data[component][row], atIndex: component)
    }
}

class Picker<T> : UIPickerView {
    
    var data: [[T]] = [] {
        didSet {
            source.data = data.map { $0.map { "\($0)" } }
            reloadAllComponents()
        }
    }
    
    weak var pickerDelegate: PickerDelegate? {
        didSet {
            source.pickerDelegate = pickerDelegate
        }
    }
    
    var seletedIndexPath: IndexPath? {
        get { return source.seletedIndexPath }
    }
    
    var selectedItem: T? {
        get {
            if let indexPath = seletedIndexPath {
                return data[indexPath.section][indexPath.row]
            }
            
            guard let first = data.first?.first as T? else {
                return nil
            }
            
            return first
        }
    }
    
    var selectionUpdated: ((_ selections: [T?]) -> Void)?
    
    private let source = PickerSource()
    
    // MARK: Initialization
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    convenience init(data: [[T]]) {
        self.init(frame: .zero)
        defer {
            self.data = data
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        dataSource = source
        delegate = source
        source.selectionUpdated = { [weak self] component, row in
            if let _self = self {
                var selections: [T?] = []
                for (idx, componentData) in _self.data.enumerated() {
                    let selectedRow = _self.selectedRow(inComponent: idx)
                    if selectedRow >= 0 {
                        selections.append(componentData[selectedRow])
                    } else {
                        selections.append(nil)
                    }
                }
                _self.selectionUpdated?(selections)
            }
        }
    }
}
