//
//  HeaderView.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 10/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

protocol HeaderViewDelegate: class {
    func headerView(_ headerView: HeaderView, didPushButton button: UIButton)
}

class HeaderView: UIView {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var button: UIButton!
    
    weak var delegate: HeaderViewDelegate?
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }
    
    convenience init(frame: CGRect? = .zero, title: String, showButton: Bool? = true, delegate: HeaderViewDelegate? = nil) {
        self.init(frame: frame!)
        
        self.title?.text = title
        self.delegate = delegate
        self.button?.isHidden = !showButton!
        
        defer {
            self.sizeToFit()
        }
    }
    
    @IBAction func buttonPushed(_ sender: UIButton) {
        delegate?.headerView(self, didPushButton: sender)
    }
}
