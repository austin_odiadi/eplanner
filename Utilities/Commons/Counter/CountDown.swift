//
//  CountDown.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 09/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

public typealias CDObject  = (eventStart: String?, eventEnd: String)
typealias Countdown = (days: Int, hours: Int, minutes: Int, seconds: Int)

class CountDownManager {
    static func getCountdownTime(from secondsUntilEvent: Double) -> Countdown {
        
        let days = Int(secondsUntilEvent / 86400)
        let hours = Int(secondsUntilEvent.truncatingRemainder(dividingBy: 86400) / 3600)
        let minutes = Int(secondsUntilEvent.truncatingRemainder(dividingBy: 3600) / 60)
        let seconds = Int(secondsUntilEvent.truncatingRemainder(dividingBy: 60))
        return (days, hours, minutes, seconds)
    }
}

class CountDown: UIView {
    var event: CDObject?
    
    @IBOutlet weak var daysLeftLabel: UILabel!
    @IBOutlet weak var hoursLeftLabel: UILabel!
    @IBOutlet weak var minutesLeftLabel: UILabel!
    @IBOutlet weak var secondsLeftLabel: UILabel!
    
    @IBOutlet weak var countdownView: UIView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadNib()
        
        initialize()
    }
    
    override required init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    convenience init(event: CDObject) {
        self.init(frame: .zero)
        defer {
            startCountDown(event: event)
        }
    }
    
    func initialize() { }
    
    func startCountDown(event: CDObject) {
        self.event = event
        
        _ = updateCountdown()
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            if !self.updateCountdown() {
                timer.invalidate()
            }
        }
    }
    
    func updateCountdown() -> Bool {
        let secondsLeftUntilEvent = self.eta()
        
        if secondsLeftUntilEvent <= -7200 {
            self.updateUI(withFallbackText: kEventIsEnded)
            return false
        }
        else if secondsLeftUntilEvent < 0 {
            self.updateUI(withFallbackText: kEventIsLive)
            return false
        }
        
        let countdownTime = CountDownManager.getCountdownTime(from: secondsLeftUntilEvent)
        self.updateUI(withCountdownTime: countdownTime)
        
        return true
    }
    
    func eta() -> TimeInterval {
        let currentUnixTime = Date().timeIntervalSince1970
        return self.eventUnixTime() - currentUnixTime
    }
    
    func eventUnixTime() -> TimeInterval {
        guard let endTime = event?.eventEnd, !endTime.isEmpty, endTime != optionDisplayText else {
            return 0
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return (dateFormatter.date(from: endTime)?.timeIntervalSince1970)!
    }
    
    func updateUI(withFallbackText fallbackText: String) {
        
        infoView.alpha = 1.0
        countdownView.alpha = 0.0
        infoLabel.text = fallbackText
    }
    
    func updateUI(withCountdownTime countdownTime: Countdown) {
        daysLeftLabel.text = countdownTime.days.stringWithLeadingZeros
        hoursLeftLabel.text = countdownTime.hours.stringWithLeadingZeros
        minutesLeftLabel.text = countdownTime.minutes.stringWithLeadingZeros
        secondsLeftLabel.text = countdownTime.seconds.stringWithLeadingZeros
    }
}


extension Int {
    var stringWithLeadingZeros: String {
        return String(format: "%02d", self)
    }
    
}
