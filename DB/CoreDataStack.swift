//
//  CoreDataStack.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 02/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import CoreData
import Foundation
import PromiseKit

typealias StackContext = NSManagedObjectContext

protocol CoreDataStackProtocol: class {
    func save() -> Promise<Bool>
    func context() -> StackContext
}

class CoreDataStack {
    
    fileprivate let model: String
    fileprivate var writeContext: StackContext
    
    lazy var main: StackContext = {
        return container.viewContext
    }()
    
    lazy var container: NSPersistentContainer = {
        var container = NSPersistentContainer(name: self.model)
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        
        return container
    }()
    
    init(model: String? = nil) {
        self.model = model!
        
        writeContext = StackContext.init(concurrencyType: .privateQueueConcurrencyType)
        writeContext.persistentStoreCoordinator = container.persistentStoreCoordinator
        
        setMergePolicies()
    }
    
    func setMergePolicies () {
        
        main.mergePolicy  = NSMergePolicy(merge: .mergeByPropertyObjectTrumpMergePolicyType)
        writeContext.mergePolicy = NSMergePolicy(merge: .mergeByPropertyObjectTrumpMergePolicyType)
        NotificationCenter.default.addObserver(self, selector: .objectsDidChange, name: NSNotification.Name.NSManagedObjectContextObjectsDidChange, object: nil)
    }
    
    @objc func objectsDidChange(notification: Notification)  {

        guard notification.userInfo != nil,
            let context = notification.object,
            context as! StackContext != self.writeContext else {
            return
        }
        
        self.main.perform {
            self.main.mergeChanges(fromContextDidSave: notification)
        }
    }
}

extension CoreDataStack: CoreDataStackProtocol {
    func save() -> Promise<Bool> {
        return firstly {
            self.writeContext.commit()
        }.then{ _ in
            return self.main.commit()
        }
    }
    
    
    func context() -> StackContext {
        let newContext: StackContext = {
            let context: StackContext = StackContext.init(concurrencyType: .privateQueueConcurrencyType)
            
            context.parent = self.writeContext
            context.mergePolicy  = NSMergePolicy(merge: .mergeByPropertyObjectTrumpMergePolicyType)
            
            return context
        }()
        
        return newContext
    }
}

extension NSManagedObjectContext {
    
    func commit() -> Promise<Bool> {
        return Promise<Bool> { seal in
            
            guard self.hasChanges else {
                seal.fulfill(true)
                return
            }

            do { try self.save() }
            catch _ as NSError {
                seal.reject(DataStackSaveError.failed)
                return
            }
            
            seal.fulfill(true)
        }
    }
}


fileprivate extension Selector {
    static let objectsDidChange = #selector(CoreDataStack.objectsDidChange(notification:))
}
