//
//  DataStackCommons.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 02/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//


import UIKit
import CoreData
import Foundation
import PromiseKit

typealias Json = [String:Any]

class DataStackCommons {
    static let shared = DataStackCommons()
    var dataStack: CoreDataStack
    
    private init() {
        dataStack = CoreDataStack(model: "UPlanner")
    }
}

extension DataStackCommons {
    
    func get<U: NSManagedObject, T: Codable>(context: StackContext? = DataStackCommons.shared.dataStack.main, predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor]? = nil) -> Promise<(U?, [T]?)> {
        
        let request: NSFetchRequest<U> = U.fetchRequest() as! NSFetchRequest<U>
        request.predicate = predicate
        request.sortDescriptors = sortDescriptors
        
        return Promise<(U?, [T]?)> { seal in
            context?.perform {
                do {
                    let result = try request.execute() as [U]?
            
                    do {
                        let jsonData = try JSONSerialization.data(withJSONObject: result?.toJson() as Any, options: [])
                        
                        let decoder = JSONDecoder()
                        decoder.userInfo[CodingUserInfoKey.context!] = context
                        seal.resolve(.fulfilled((nil, try! decoder.decode([T].self, from: jsonData))))
                    }
                    catch let error {
                        print("\(error)")
                    }
                }
                catch { }
            }
        }
    }

    func insert<T: Codable>(context: StackContext? = DataStackCommons.shared.dataStack.main, json: [Json]) -> Promise<(T?, Bool)> {
       
        return Promise<(T?, Bool)> { seal in
            do {
                let decoder = JSONDecoder()
                decoder.userInfo[CodingUserInfoKey.context!] = context
                
                let jsonData = try JSONSerialization.data(withJSONObject: json, options: [])
                
                do {
                    _ = try decoder.decode([T].self, from: jsonData)
                    context?.commit().done { _ in
                        DataStackCommons.shared.dataStack.save().done { success in
                            seal.fulfill((nil, success))
                        }.cauterize()
                    }.cauterize()
                }
                catch { seal.reject(DataStackCommonsSaveError.decodeFailure) }
            }
            catch { seal.reject(DataStackCommonsSaveError.serializationFailure) }
        }
    }
    
    func batchUpdate<T: NSManagedObject>(context: StackContext? = DataStackCommons.shared.dataStack.main, predicate: NSPredicate? = nil, update: Json) -> Promise<(T?, Bool)> {
        
        let entityDescription = NSEntityDescription.entity(forEntityName: T.entity().name!, in: context!)
        let batchUpdateRequest = NSBatchUpdateRequest(entity: entityDescription!)
        
        batchUpdateRequest.predicate = predicate
        batchUpdateRequest.propertiesToUpdate = update
        batchUpdateRequest.resultType = .updatedObjectIDsResultType
        
        return Promise<(T?, Bool)> { seal in
            do {
                let batchUpdateResult = try context!.execute(batchUpdateRequest) as! NSBatchUpdateResult
                let objectIDs = batchUpdateResult.result as! [NSManagedObjectID]
                
                objectIDs.forEach { context?.refresh((context?.object(with: $0))!, mergeChanges: false) }
                seal.fulfill((nil, objectIDs.count > 0))
                
            } catch let error {
                print("\(error)")
            }
        }
    }
    
    func update<T: NSManagedObject>(context: StackContext? = DataStackCommons.shared.dataStack.main, predicate: NSPredicate? = nil, update: Json) -> Promise<(T?, Bool)> {
        
        let request: NSFetchRequest<T> = T.fetchRequest() as! NSFetchRequest<T>
        request.predicate = predicate
        
        return Promise<(T?, Bool)> { seal in
            context?.perform {
                do {
                    let result = try request.execute() as [T]?
                    result?.forEach { mo in
                        mo.attributeNames().forEach { key in
                            if let value = update[key] { mo.setValue(value, forKey: key) }
                        }
                    }
                    
                    context?.commit().done { success in
                        seal.resolve(.fulfilled((nil, success)))
                    }.cauterize()
                }
                catch let error {
                    print("\(error)")
                }
            }
        }
    }
    
    func delete<T: NSManagedObject>(context: StackContext? = DataStackCommons.shared.dataStack.main, predicate: NSPredicate? = nil)  -> Promise<(T?, Bool)> {

        let request: NSFetchRequest<T> = T.fetchRequest() as! NSFetchRequest<T>
        request.predicate = predicate
        
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: request as! NSFetchRequest<NSFetchRequestResult>)
        batchDeleteRequest.resultType = .resultTypeCount
        
        return Promise<(T?, Bool)> { seal in
            do {
                _ = try context!.execute(batchDeleteRequest) as! NSBatchDeleteResult
                seal.fulfill((nil, true))
                
            } catch let error {
                print("\(error)")
            }
        }
    }
}

extension NSManagedObject {
    func attributeNames() -> [String] {
        return self.entity.attributesByName.keys.map { return $0 }
    }
    
    func relationshipAttributeNames() -> [String] {
        return self.entity.relationshipsByName.keys.map { return $0 }
    }
}

extension Array where Element: NSManagedObject {
    
    func toJson() -> [[String : Any]] {
        return convert(foundKeys: [String]())
    }
    
    func convert(foundKeys: [String]) -> [[String : Any]] {
        
        var foundKeysVar: [String] = foundKeys
        var array: [[String: Any]] = [[String: Any]]()
        
        self.forEach { mo in
            foundKeysVar.append(mo.entity.name!)
            var dictionary: [String: Any] = [String: Any]()
            
            mo.attributeNames().forEach { dictionary[$0] = mo.value(forKey: $0) }
            mo.relationshipAttributeNames().forEach {
                if let relationship = mo.value(forKey: $0)  {
                    
                    if let relationshipMO: NSManagedObject = relationship as? NSManagedObject {
                        
                        if !foundKeys.contains(relationshipMO.entity.name!) {
                            dictionary[$0] = [relationshipMO].convert(foundKeys: foundKeysVar).first
                        }
                    }
                    else if let relationshipSet: NSSet = relationship as? NSSet,
                        let array: [NSManagedObject] = relationshipSet.allObjects as? [NSManagedObject] {
                        
                        dictionary[$0] = array.convert(foundKeys: foundKeysVar)
                    }
                }
            }
            
            array.append(dictionary)
        }
        
        return array
    }
}
