EPlanner
========

To Run:
=======
1. Navigate to project path via commandline
2. Run "pod install"
3. Run app on device or simulator

Pod used
========

Fabric
PromiseKit
Crashlytics
StepSlider
JTMaterialSpinner 3.0

Software Architecture
=====================
Viper

DB Tables
=========
Events
Guests
Contacts
Calculations
Expenses
TodoList

Nik
====
    