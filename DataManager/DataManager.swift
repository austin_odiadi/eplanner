//
//  DataManager.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

protocol EventsDataManagerProtocol {
    
    func saveEvents(events: [Json]) -> Promise<Bool>
    func deleteEvent(withId id: Int64) -> Promise<Bool>
    func fetchEvents(predicate: NSPredicate?) -> Promise<[Event]?>
    func updateEvent(withId id: Int64, update: [EventData]) -> Promise<Event?>
}


protocol GuestsDataManagerProtocol {
    func updateGuest(guest: [GuestData], contact: [ContactData], withId id: Int64) -> Promise<Bool>
}

protocol ToDoListDataManagerProtocol {
    
    func saveToDos(todos: [ToDoData]) -> Promise<Bool>
    func deleteToDo(withId id: Int64) -> Promise<Bool>
    func fetchToDos(predicate: NSPredicate?) -> Promise<[ToDo]?>
    func updateReminder(reminder: Bool, id: Int64) -> Promise<ToDo?>
    func updateCompleted(completed: Bool, id: Int64) -> Promise<ToDo?>
    func updateToDo(withId id: Int64, update: [ToDoData]) -> Promise<ToDo?>
}

protocol CalculationDataManagerProtocol {

    func deleteCalculation(withId id: Int64) -> Promise<Bool>
    func fetchCalculations(predicate: NSPredicate?) -> Promise<[Calculation]?>
    func saveCalculation(withId id: Int64, calculation: [AnyObject]) -> Promise<Bool>
    func updateCalculation(withId id: Int64, calculation: [AnyObject]) -> Promise<Calculation?>
}

protocol ExpensesDataManagerProtocol {

    func deleteExpense(withId id: Int64) -> Promise<Bool>
    func saveExpense(expense: [ExpenseData]) -> Promise<Bool>
    func fetchExpense(predicate: NSPredicate?) -> Promise<[Expense]?>
    func updateExpense(withId id: Int64, update: [ExpenseData]) -> Promise<Expense?>
}


final public class DataManager {
    lazy var dataStackCommons = DataStackCommons.shared
}

//MARK: - Events -
extension DataManager: EventsDataManagerProtocol {
    
    func fetchEvents(predicate: NSPredicate? = nil) -> Promise<[Event]?> {
        
        return Promise<[Event]?> { seal in
            let sort: [NSSortDescriptor] = [NSSortDescriptor(key: "id", ascending: false)]
            dataStackCommons.get(predicate: predicate, sortDescriptors: sort).done {(_: Events?, events: [Event]?) in
                seal.resolve(.fulfilled(events))
            }.cauterize()
        }
    }
    
    func saveEvents(events: [Json]) -> Promise<Bool> {
        return Promise<Bool> { seal in
            dataStackCommons.insert(json: events).done{ (arg: (Events?, Bool)) in
                let (_, success) = arg
                seal.resolve(.fulfilled(success))
            }.cauterize()
        }
    }
    
    func updateEvent(withId id: Int64, update: [EventData]) -> Promise<Event?> {
        let predicate = NSPredicate(format: "id == %d" , id)
        
        return Promise<Event?> { seal in
            dataStackCommons.batchUpdate(predicate: predicate, update: update.updateAsJson())
                .done { (arg: (Events?, Bool)) in
                    let (_, success) = arg
                    if success {
                        self.fetchEvents(predicate: predicate).done { events in
                            seal.resolve(.fulfilled(events?.first))
                        }.cauterize()
                    }
                    else {
                        seal.resolve(.fulfilled(nil))
                    }
                }.cauterize()
        }
    }
    
    func deleteEvent(withId id: Int64) -> Promise<Bool> {
        let predicate = NSPredicate(format: "id == %d" , id)
        
        return Promise<Bool> { seal in
            dataStackCommons.delete(predicate: predicate).done { (arg: (Events?, Bool)) in
                let (_, success) = arg
                
                seal.resolve(.fulfilled(success))
            }.cauterize()
        }
    }
}

extension DataManager: GuestsDataManagerProtocol {
    
    func updateGuest(guest: [GuestData], contact: [ContactData], withId id: Int64) -> Promise<Bool> {
        let predicate = NSPredicate(format: "%K == %d", #keyPath(Guests.event.id), id)

        return Promise<Bool> { seal in
            dataStackCommons.update(predicate: predicate, update: guest.updateAsJson())
                .done { (arg: (Guests?, Bool)) in
                    let (_, success) = arg
                    seal.resolve(.fulfilled(success))
                }.cauterize()
        }
    }
}

//MARK: - ToDo List -
extension DataManager: ToDoListDataManagerProtocol {
    
    func fetchToDos(predicate: NSPredicate? = nil) -> Promise<[ToDo]?> {
        
        return Promise<[ToDo]?> { seal in
            dataStackCommons.get(predicate: predicate).done {(_: ToDoList?, events: [ToDo]?) in
                seal.resolve(.fulfilled(events))
            }.cauterize()
        }
    }
    
    func saveToDos(todos: [ToDoData]) -> Promise<Bool> {
        return Promise<Bool> { seal in
            dataStackCommons.insert(json: todos.asJson()).done{ (arg: (ToDoList?, Bool)) in
                let (_, success) = arg
                seal.resolve(.fulfilled(success))
            }.cauterize()
        }
    }
    
    func updateToDo(withId id: Int64, update: [ToDoData]) -> Promise<ToDo?> {
        let predicate = NSPredicate(format: "id == %d" , id)
        
        return Promise<ToDo?> { seal in
            dataStackCommons.batchUpdate(predicate: predicate, update: update.updateAsJson())
                .done { (arg: (ToDoList?, Bool)) in
                    let (_, success) = arg
                    if success {
                        self.fetchToDos(predicate: predicate).done { todos in
                            seal.resolve(.fulfilled(todos?.first))
                            }.cauterize()
                    }
                    else {
                        seal.resolve(.fulfilled(nil))
                    }
                }.cauterize()
        }
    }
    
    func updateReminder(reminder: Bool, id: Int64) -> Promise<ToDo?> {
        let predicate = NSPredicate(format: "%K == %d", #keyPath(ToDoList.id), id)
        
        return Promise<ToDo?> { seal in
            dataStackCommons.batchUpdate(predicate: predicate, update: ToDo.updateReminderAsJson(reminder: reminder))
                .done { (arg: (ToDoList?, Bool)) in
                    let (_, success) = arg
                    
                    if success {
                        self.fetchToDos(predicate: predicate).done { todos in
                            seal.resolve(.fulfilled(todos?.first))
                        }.cauterize()
                    }
                    else {
                        seal.resolve(.fulfilled(nil))
                    }
                    
                }.cauterize()
        }
    }
    
    func updateCompleted(completed: Bool, id: Int64) -> Promise<ToDo?> {
        let predicate = NSPredicate(format: "%K == %d", #keyPath(ToDoList.id), id)
        
        return Promise<ToDo?> { seal in
            dataStackCommons.batchUpdate(predicate: predicate, update: ToDo.updateCompletedAsJson(completed: completed))
                .done { (arg: (ToDoList?, Bool)) in
                    let (_, success) = arg
                    
                    if success {
                        self.fetchToDos(predicate: predicate).done { todos in
                            seal.resolve(.fulfilled(todos?.first))
                            }.cauterize()
                    }
                    else {
                        seal.resolve(.fulfilled(nil))
                    }
                    
                }.cauterize()
        }
    }
    
    func deleteToDo(withId id: Int64) -> Promise<Bool> {
        let predicate = NSPredicate(format: "%K == %d", #keyPath(ToDoList.id), id)
        
        return Promise<Bool> { seal in
            dataStackCommons.delete(predicate: predicate).done { (arg: (ToDoList?, Bool)) in
                let (_, success) = arg
                
                seal.resolve(.fulfilled(success))
                }.cauterize()
        }
    }
}

//MARK: - Calculation -
extension DataManager: CalculationDataManagerProtocol {
    
    func deleteCalculation(withId id: Int64) -> Promise<Bool> {
        let predicate = NSPredicate(format: "%K == %d", #keyPath(Calculations.event.id), id)
        
        return Promise<Bool> { seal in
            
            dataStackCommons.update(predicate: predicate, update: CalculationData.deleteAsJson())
            .done { (arg: (Calculations?, Bool)) in
                let (_, success) = arg
                seal.resolve(.fulfilled(success))
            }.cauterize()
        }
    }
    
    func saveCalculation(withId id: Int64, calculation: [AnyObject]) -> Promise<Bool> {
        
        return Promise<Bool> { seal in
            guard let calculations: [CalculationData] = Array(calculation[1 ..< 9]) as? [CalculationData] else {
                seal.resolve(.fulfilled(false))
                return
            }
            
            let predicate = NSPredicate(format: "%K == %d", #keyPath(Calculations.event.id), id)
            dataStackCommons.update(predicate: predicate, update: calculations.asJson())
            .done { (arg: (Calculations?, Bool)) in
                let (_, success) = arg
                seal.resolve(.fulfilled(success))
            }.cauterize()
        }
    }
    
    func fetchCalculations(predicate: NSPredicate?) -> Promise<[Calculation]?> {
        
        return Promise<[Calculation]?> { seal in
            dataStackCommons.get(predicate: predicate).done {(_: Calculations?, calculation: [Calculation]?) in
                seal.resolve(.fulfilled(calculation))
                }.cauterize()
        }
    }
    
    func updateCalculation(withId id: Int64, calculation: [AnyObject]) -> Promise<Calculation?> {
    
        return Promise<Calculation?> { seal in
            guard let calculations: [CalculationData] = Array(calculation[1 ..< 9]) as? [CalculationData] else {
                seal.resolve(.fulfilled(nil))
                return
            }
        
            let predicate = NSPredicate(format: "%K == %d", #keyPath(Calculations.event.id), id)
            dataStackCommons.update(predicate: predicate, update: calculations.updateAsJson())
                .done { (arg: (Calculations?, Bool)) in
                    let (_, success) = arg
                    if success {
                        self.fetchCalculations(predicate: predicate).done { calculations in
                            seal.resolve(.fulfilled(calculations?.first))
                            }.cauterize()
                    }
                    else {
                        seal.resolve(.fulfilled(nil))
                    }
                }.cauterize()
        }
    }
}

//MARK: - Expense -
extension DataManager: ExpensesDataManagerProtocol {
    
    func deleteExpense(withId id: Int64) -> Promise<Bool> {
        let predicate = NSPredicate(format: "%K == %d", #keyPath(Expenses.id), id)
        
        return Promise<Bool> { seal in
            dataStackCommons.delete(predicate: predicate).done { (arg: (Expenses?, Bool)) in
                let (_, success) = arg
                
                seal.resolve(.fulfilled(success))
                }.cauterize()
        }
    }
    
    func saveExpense(expense: [ExpenseData]) -> Promise<Bool> {
        return Promise<Bool> { seal in
            dataStackCommons.insert(json: expense.asJson()).done{ (arg: (Expenses?, Bool)) in
                let (_, success) = arg
                seal.resolve(.fulfilled(success))
                }.cauterize()
        }
    }
    
    func fetchExpense(predicate: NSPredicate? = nil) -> Promise<[Expense]?> {
        return Promise<[Expense]?> { seal in
            dataStackCommons.get(predicate: predicate).done {(_: Expenses?, calculation: [Expense]?) in
                seal.resolve(.fulfilled(calculation))
                }.cauterize()
        }
    }
    
    func updateExpense(withId id: Int64, update: [ExpenseData]) -> Promise<Expense?> {
        let predicate = NSPredicate(format: "%K == %d", #keyPath(Expenses.id), id)
        
        return Promise<Expense?> { seal in
            dataStackCommons.batchUpdate(predicate: predicate, update: update.updateAsJson())
                .done { (arg: (Expenses?, Bool)) in
                    let (_, success) = arg
                    if success {
                        self.fetchExpense(predicate: predicate).done { expenses in
                            seal.resolve(.fulfilled(expenses?.first))
                            }.cauterize()
                    }
                    else {
                        seal.resolve(.fulfilled(nil))
                    }
                }.cauterize()
        }
    }
}
