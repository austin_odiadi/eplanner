//
//  ExpensesViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class ExpensesViewControllerInteractor: ExpensesViewControllerInInteractorInterface {
    
    var dataManager: DataManager?
    var presenter: ExpensesViewControllerOutInteractorInterface?
    
    init(dataManager: DataManager) {
        self.dataManager = dataManager
    }
    
    func fetchExpense() -> Promise<[Expense]?> {
        return (dataManager?.fetchExpense())!
    }
}
