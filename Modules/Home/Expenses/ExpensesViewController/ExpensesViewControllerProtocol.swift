//
//  ExpensesViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// P -> V
protocol ExpensesViewControllerInterface: class {
    func initialize()
}

// V -> P
protocol ExpensesViewControllerPresenterInterface: class {
    func viewDidLoad()
    func fetchExpense() -> Promise<[Expense]?>
    
    var router: ExpensesViewControllerRouterInterface? {get set}
    var interactor: ExpensesViewControllerInInteractorInterface? {get set};
}

// P -> R
protocol ExpensesViewControllerRouterInterface: class {
}

// P -> I
protocol ExpensesViewControllerInInteractorInterface: class {
    func fetchExpense() -> Promise<[Expense]?>
    
    var dataManager: DataManager? {get set}
    var presenter: ExpensesViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol ExpensesViewControllerOutInteractorInterface: class {
    func availableExpenses(expenses: [Event]?)
}
