//
//  ExpensesViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class ExpensesViewController: BaseViewController {
    
    var expenses: [Expense]?
    var presenter: ExpensesViewControllerPresenterInterface?
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    @objc func createExpense() {
        CreateExpenseViewControllerRouter.createModule(loadType: .new).done { view in
            guard let viewController = view.topViewController as? CreateExpenseViewController else {
                return
            }
            
            viewController.dismissed = { result in
                if (result as! Bool) {
                    self.updateData()
                }
            }
            self.present(view, animated: true, completion: nil)
            }.cauterize()
    }
    
    func updateData() {
        presenter?.fetchExpense().done { expenses in
            self.expenses = expenses
            self.tableView.reloadData()
        }.cauterize()
    }
}

extension ExpensesViewController: ExpensesViewControllerInterface {
    
    //MARK: - Initializer -
    
    func initialize() {
        view.backgroundColor = UIColor.plannerBackgroundColor()
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "addIcon"), style: .done, target: self, action: .createExpense)
        
        tableView.register(UINib(nibName: "ExpensesTableViewCell", bundle: nil), forCellReuseIdentifier: expensesCellIdentifier)
         updateData()
    }
}

extension ExpensesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expenses?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: expensesCellIdentifier, for: indexPath) as! ExpensesTableViewCell
        cell.title.text = expenses?[indexPath.row].name
        cell.subtitle.text = "COST: " + "\(expenses?[indexPath.row].cost ?? 0)".currencyFormat()
        cell.paidButton.isHidden = expenses?[indexPath.row].paid == 1 ? false : true
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return  UIView()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ExpenseDetailViewControllerRouter.createModule(expense: (expenses?[indexPath.row])!).done { view in

            view.dismissed = { result in
                if (result as! Bool) {
                    self.updateData()
                }
            }
            self.navigationController?.pushViewController(view, animated: true)
            }.cauterize()
    }
}

fileprivate extension Selector {
    static let createExpense = #selector(ExpensesViewController.createExpense)
}
