//
//  ExpensesViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class ExpensesViewControllerPresenter: ExpensesViewControllerPresenterInterface {
    
    var router: ExpensesViewControllerRouterInterface?
    private weak var view: ExpensesViewControllerInterface?
    var interactor: ExpensesViewControllerInInteractorInterface?
    
    init(interactor: ExpensesViewControllerInInteractorInterface,
         viewController: ExpensesViewControllerInterface,
         router: ExpensesViewControllerRouterInterface) {
        
        self.router     = router
        self.interactor = interactor
        self.view       = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func fetchExpense() -> Promise<[Expense]?> {
        return (interactor?.fetchExpense())!
    }
}

extension ExpensesViewControllerPresenter: ExpensesViewControllerOutInteractorInterface {
    func availableExpenses(expenses: [Event]?) {
    }
}
