//
//  ExpensesViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class ExpensesViewControllerRouter: ExpensesViewControllerRouterInterface, MainTabBarViewProtocol {
    
    var tabIcon: UIImage = UIImage(named:"expensesIcon")!
    var tabTitle: String = "Expenses"
    var navTitle: String = "Expenses"
    
    class func createModule() -> Promise<ExpensesViewController> {
        
        return firstly { () -> Promise<ExpensesViewController> in
            let view = BaseViewController.viewControllerWithIdentifier(.expenses)
            
            guard let viewController = view.value as? ExpensesViewController else { return .value(ExpensesViewController())}
            
            let router: ExpensesViewControllerRouterInterface           = ExpensesViewControllerRouter()
            let interactor: ExpensesViewControllerInInteractorInterface = ExpensesViewControllerInteractor(dataManager: DataManager())
            let presenter: ExpensesViewControllerPresenterInterface     = ExpensesViewControllerPresenter(interactor: interactor, viewController: viewController as ExpensesViewControllerInterface, router: router)
            
            viewController.presenter = presenter
            interactor.presenter = presenter as? ExpensesViewControllerOutInteractorInterface
            
            return .value(viewController)
        }
    }
    
    func configuredViewController() -> UIViewController {
        let view = BaseViewController.viewControllerWithIdentifier(.expenses)
        
        guard let viewController = view.value as? ExpensesViewController else { return ExpensesViewController()}
        
        let interactor: ExpensesViewControllerInInteractorInterface = ExpensesViewControllerInteractor(dataManager: DataManager())
        let presenter: ExpensesViewControllerPresenterInterface     = ExpensesViewControllerPresenter(interactor: interactor, viewController: viewController as ExpensesViewControllerInterface, router: self)
        
        viewController.presenter = presenter
        interactor.presenter = presenter as? ExpensesViewControllerOutInteractorInterface
        
        return viewController
    }
}
