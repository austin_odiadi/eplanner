//
//  ExpenseDetailViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 19/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class ExpenseDetailViewControllerPresenter: ExpenseDetailViewControllerPresenterInterface {
    
    var router: ExpenseDetailViewControllerRouterInterface?
    private weak var view: ExpenseDetailViewControllerInterface?
    var interactor: ExpenseDetailViewControllerInInteractorInterface?
    
    init(interactor: ExpenseDetailViewControllerInInteractorInterface,
         viewController: ExpenseDetailViewControllerInterface,
         router: ExpenseDetailViewControllerRouterInterface) {
        
        self.router     = router
        self.interactor = interactor
        self.view       = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func deleteExpense(with id: Int64) -> Promise<Bool> {
        return (interactor?.deleteExpense(with: id))!
    }
}

extension ExpenseDetailViewControllerPresenter: ExpenseDetailViewControllerOutInteractorInterface {
    
}
