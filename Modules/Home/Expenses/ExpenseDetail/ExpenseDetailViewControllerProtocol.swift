//
//  ExpenseDetailViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 19/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// P -> V
protocol ExpenseDetailViewControllerInterface: class {
    func initialize()
}

// V -> P
protocol ExpenseDetailViewControllerPresenterInterface: class {
    
    func viewDidLoad()
    func deleteExpense(with id: Int64) -> Promise<Bool>
    
    var router: ExpenseDetailViewControllerRouterInterface? {get set}
    var interactor: ExpenseDetailViewControllerInInteractorInterface? {get set};
}

// P -> R
protocol ExpenseDetailViewControllerRouterInterface: class {
}

// P -> I
protocol ExpenseDetailViewControllerInInteractorInterface: class {
    
    func deleteExpense(with id: Int64) -> Promise<Bool>
    
    var dataManager: DataManager? {get set}
    var presenter: ExpenseDetailViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol ExpenseDetailViewControllerOutInteractorInterface: class {
}

