//
//  ExpenseDetailViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 19/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class ExpenseDetailViewController: BaseViewController {

    var expense: Expense?
    var isUpdated: Bool = false
    var hasDeposit: Bool = false
    var presenter: ExpenseDetailViewControllerPresenterInterface?
    
    @IBOutlet weak var deleteButtonContainer: UIView!
    @IBOutlet weak var costContainer: UIView!
    @IBOutlet weak var costTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }

    override func willMove(toParent parent: UIViewController?) {
        if (parent == nil) {
            dismissed?(isUpdated)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        deleteButtonContainer.layoutIfNeeded()
        deleteButtonContainer.roundedCorners(radius: 5)
        
        costContainer.layoutIfNeeded()
        costContainer.roundedCorners(radius: 5)
        costContainer.dropShadow(color: .lightGray, radius: 5, scale: true)
    }
    
    @objc func editExpense() {
        CreateExpenseViewControllerRouter.createModule(expense: expense, loadType: .existing).done { view in
            guard let viewController = view.topViewController as? CreateExpenseViewController else {
                return
            }
            
            viewController.dismissed = { result in
                guard let expense = result as? Expense else  {
                    return
                }
                
                self.isUpdated = true
                self.expense = expense
                self.update()
            }
            self.present(view, animated: true, completion: nil)
            }.cauterize()
    }
    
    func update() {
        costTextField.text = "\(expense?.cost ?? 0)".currencyFormat()
        hasDeposit = expense?.deposit != 0
        tableView.reloadData()
    }
    
    @IBAction func deleteButton(_ sender: UIButton) {
        presenter?.deleteExpense(with: (expense?.id)!).done { success in
            self.isUpdated = success
            self.navigationController?.popViewController(animated: true)
            }.cauterize()
    }
}

extension ExpenseDetailViewController: ExpenseDetailViewControllerInterface {
    
    func initialize() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Edit", style: .done, target: self, action: .editExpense)
        
        costTextField.isUserInteractionEnabled = false
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "TextfieldTableViewCell", bundle: nil), forCellReuseIdentifier: textFieldIdentifier)

        update()
    }
}

extension ExpenseDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hasDeposit ? 4 : 3
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: textFieldIdentifier, for: indexPath) as! TextfieldTableViewCell

        if indexPath.row == 0 {
            cell.textField.text = "Name: " + (expense?.name)!
        }
        else if indexPath.row == 1 {
            cell.textField.text = "Paid: " + expensePaidOption[Int(expense?.paid ?? 0)]
        }
        else if indexPath.row == 2 {
            if hasDeposit {
                cell.textField.text = "Deposit: " + "\(expense?.deposit ?? 0)".currencyFormat()
            }
            else {
                cell.textField.text = "Deposit: " + (expense?.note)!
            }
        }
        else if indexPath.row == 3 {
            cell.textField.text = "Deposit: " + (expense?.note)!
        }
        
        cell.textField.isUserInteractionEnabled = false
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return  UIView()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

fileprivate extension Selector {
    static let editExpense = #selector(ExpenseDetailViewController.editExpense)
}

