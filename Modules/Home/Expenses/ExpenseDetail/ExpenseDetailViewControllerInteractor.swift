//
//  ExpenseDetailViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 19/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class ExpenseDetailViewControllerInteractor: ExpenseDetailViewControllerInInteractorInterface {
    
    var dataManager: DataManager?
    var presenter: ExpenseDetailViewControllerOutInteractorInterface?
    
    init(dataManager: DataManager) {
        self.dataManager = dataManager
    }
    
    func deleteExpense(with id: Int64) -> Promise<Bool> {
        return (dataManager?.deleteExpense(withId: id))!
    }
}
