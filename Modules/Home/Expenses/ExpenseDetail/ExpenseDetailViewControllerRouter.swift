//
//  ExpenseDetailViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 19/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class ExpenseDetailViewControllerRouter: ExpenseDetailViewControllerRouterInterface {
    
    class func createModule(expense: Expense) -> Promise<ExpenseDetailViewController> {
        
        return firstly { () -> Promise<ExpenseDetailViewController> in
            let view = BaseViewController.viewControllerWithIdentifier(.expenseDetail)
            
            guard let viewController = view.value as? ExpenseDetailViewController else { return .value(ExpenseDetailViewController())}
            
            let router: ExpenseDetailViewControllerRouterInterface           = ExpenseDetailViewControllerRouter()
            let interactor: ExpenseDetailViewControllerInInteractorInterface = ExpenseDetailViewControllerInteractor(dataManager: DataManager())
            let presenter: ExpenseDetailViewControllerPresenterInterface     = ExpenseDetailViewControllerPresenter(interactor: interactor, viewController: viewController as ExpenseDetailViewControllerInterface, router: router)
            
            viewController.expense = expense
            viewController.presenter = presenter
            interactor.presenter = presenter as? ExpenseDetailViewControllerOutInteractorInterface
            
            return .value(viewController)
        }
    }
}
