//
//  CreateExpenseViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 19/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class CreateExpenseViewControllerInteractor: CreateExpenseViewControllerInInteractorInterface {
    
    var dataManager: DataManager?
    var presenter: CreateExpenseViewControllerOutInteractorInterface?
    
    init(dataManager: DataManager) {
        self.dataManager = dataManager
    }
    
    func paidOptions() -> [String] {
        return ["No", "Yes", "Deposit"]
    }
    
    func newExpenseData() -> [ExpenseData] {
        return ExpenseData.data()
    }
    
    func existingExpenseData(expense: Expense) -> [ExpenseData] {
        return ExpenseData.data(with: expense)
    }
    
    func saveExpense(expense: [ExpenseData]) -> Promise<Bool> {
        return (dataManager?.saveExpense(expense: expense))!
    }
    
    func updateExpense(withId id: Int64, expense: [ExpenseData]) -> Promise<Expense?> {
        return (dataManager?.updateExpense(withId: id, update: expense))!
    }
    
}
