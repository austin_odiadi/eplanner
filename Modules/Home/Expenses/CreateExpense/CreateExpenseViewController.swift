//
//  CreateExpenseViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 19/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class CreateExpenseViewController: BaseViewController {
    
    var expense: Expense?
    var deposit: ExpenseData?
    var expenseData: [ExpenseData]?
    var isPickerVisible: Bool = false
    var loadType: CreateExpenseLoadType?
    var presenter: CreateExpenseViewControllerPresenterInterface?

    @IBOutlet weak var costTextField: UITextField!
    @IBOutlet weak var pickerView: PickerView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickerContainerTop: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    @objc func saveExpense() {
        view.endEditing(true)
        
        if (deposit != nil) {
            if (deposit?.value?.isEmpty)! {
                deposit?.value = "\(0)".currencyFormat()
            }
            else {
                deposit?.value = deposit?.value?.currencyFormat()
            }
            
            let index = expenseData!.index(where: { (item) -> Bool in
                item.index == 2
            })
            
            if index == nil && deposit != nil {
                expenseData?.insert(deposit!, at: 2)
            }
            else {
                expenseData?[index!].value = deposit?.value
            }
        }
    
        expenseData?.last?.value = expenseData?.last?.value?.currencyFormat()
        
        switch loadType {
        case .new?:
            if let paidOption = expenseData?[1].value, paidOption.isEmpty {
                expenseData?[1].value = "No"
            }
            
            presenter?.saveExpense(expense: expenseData!).done { success in
                
                self.dismissed?(success)
                self.dismiss(animated: true, completion: nil)
                
            }.cauterize()
            
            break
        case .existing?:
            presenter?.updateExpense(withId: (expense?.id)!, expense: expenseData!).done { expense in
                self.dismissed?(expense as Any)
                self.dismiss(animated: true, completion: nil)
            }.cauterize()
            break
        default: break
        }
    }
    
    @objc func cancelExpense() {
        dismissed?(false)
        dismiss(animated: true, completion: nil)
    }
    
    func hidePicker(end selection: Bool? = false, hidden: @escaping () -> Void) {
        if !isPickerVisible {
            return
        }
        
        pickerContainerTop = pickerContainerTop.setMultiplier(multiplier: 1)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.endEditing(true)
            self.pickerView.layoutIfNeeded()
        }) { (_) in
            self.isPickerVisible = false
            hidden()
        }
    }
    
    func showDeposit(show: Bool) {
        let index = expenseData!.index(where: { (item) -> Bool in
            item.index == 2
        })
        
        if show {
            if (index == nil) {
                expenseData?.insert(deposit!, at: 2)
                tableView.insertRows(at: [IndexPath(row: 2, section: 0)], with: .fade)
            }
        }
        else {
            if index != nil {
                deposit = expenseData![index!]
                expenseData?.remove(at: index!)
                deposit?.value = ""
                tableView.deleteRows(at:  [IndexPath(row: 2, section: 0)], with: .fade)
            }
        }
    }
    
    @IBAction func textFieldEndedEditing(_ sender: Any) {
        let costIndex = expenseData!.index(where: { (item) -> Bool in
            item.index == 4
        })
        
        if let found = costIndex {
            expenseData![found].value = (sender as! UITextField).text?.currencyFormat()
            costTextField.text = expenseData![found].value?.currencyFormat()
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if !(textField.text?.removeCurrencyFormatIfAny().doubleValue != nil) {
            textField.text = "\(textField.text?.dropLast() ?? "")"
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension CreateExpenseViewController: CreateExpenseViewControllerInterface {
    
    func initialize() {
        view.backgroundColor = UIColor.plannerBackgroundColor()
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Save", style: .done, target: self, action: .saveExpense)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "Cancel", style: .done, target: self, action: .cancelExpense)
        
        costTextField.delegate = self
        costTextField.addTarget(self, action: .textFieldDidChange, for: .editingChanged)
        
        pickerView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "TextfieldTableViewCell", bundle: nil), forCellReuseIdentifier: textFieldIdentifier)

        switch loadType {
        case .new?:
            expenseData = (presenter?.newExpenseData())!
            break
        case .existing?:
            if let existingExpense = expense {
                expenseData = (presenter?.existingExpenseData(expense: existingExpense))!
            }
            break
        default: break
        }
        
        let depositIndex = expenseData!.index(where: { (item) -> Bool in
            item.index == 2
        })
        
        if let found = depositIndex, let value = expenseData![found].value {
            
            if value.isEmpty {
                deposit = expenseData![found]
                expenseData?.remove(at: found)
            }
        }
        
        let costIndex = expenseData!.index(where: { (item) -> Bool in
            item.index == 4
        })
        
        if let found = costIndex {
            costTextField.text = expenseData![found].value?.currencyFormat()
        }
    }
}

extension CreateExpenseViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = expenseData?.count else {
            return 0
        }
        
        return count - 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: textFieldIdentifier, for: indexPath) as! TextfieldTableViewCell
        
        if indexPath.row == 2 {
            cell.textField.text = expenseData?[indexPath.row].value?.currencyFormat()
        }
        else {
            cell.textField.text = expenseData?[indexPath.row].value
        }
        cell.textField.placeholder = expenseData?[indexPath.row].title
        cell.textField.isUserInteractionEnabled = expenseData?[indexPath.row].index != 1
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            
            self.view.endEditing(true)
            self.pickerView.loadPicker(picker: Picker(data: [(self.presenter?.paidOptions())!]))
            
            self.pickerContainerTop = self.pickerContainerTop.setMultiplier(multiplier: 0.63)
            UIView.animate(withDuration: 0.25, animations: {
                self.view.layoutIfNeeded()
            }) { (_) in
                self.isPickerVisible = true
            }
        }
    }
}

extension CreateExpenseViewController: PickerViewDelegate {
    func picker(_ view: PickerView, _ selection: Any?, _ done: Bool, _ type: PickerType) {
        
        if let selected = selection as? String {
            expenseData?[1].value = selected
            showDeposit(show: selected.lowercased() == "Deposit".lowercased())
            tableView.reloadData()
        }
        
        hidePicker(end: true, hidden: { })
    }
    
    func picker(_ view: PickerView, _ selection: Any?, type: PickerType) {
        
        if let selected = selection as? String {
            expenseData?[1].value = selected
            showDeposit(show: selected.lowercased() == "Deposit".lowercased())
            tableView.reloadData()
        }
    }
}

extension CreateExpenseViewController: TextfieldTableViewCellDelegate {
    func cell(_ cell: TextfieldTableViewCell, didEndEditing title: String) {
        
        if let indexPath = tableView.indexPath(for: cell) {
            let index = expenseData!.index(where: { (item) -> Bool in
                item.index == 2
            })
            
            expenseData?[indexPath.row].value = indexPath.row == 2 && (index != nil) ? title.currencyFormat() : title
            tableView.reloadData()
        }
    }
    
    func cell(_ cell: TextfieldTableViewCell, didBeginEditing title: String) {
        hidePicker(hidden: { })
    }
}

extension CreateExpenseViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}

fileprivate extension Selector {
    static let cancelExpense = #selector(CreateExpenseViewController.cancelExpense)
    static let saveExpense = #selector(CreateExpenseViewController.saveExpense)
    static let textFieldDidChange = #selector(CreateExpenseViewController.textFieldDidChange)
}
