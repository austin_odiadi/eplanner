//
//  CreateExpenseViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 19/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class CreateExpenseViewControllerPresenter: CreateExpenseViewControllerPresenterInterface {
    
    var router: CreateExpenseViewControllerRouterInterface?
    private weak var view: CreateExpenseViewControllerInterface?
    var interactor: CreateExpenseViewControllerInInteractorInterface?
    
    init(interactor: CreateExpenseViewControllerInInteractorInterface,
         viewController: CreateExpenseViewControllerInterface,
         router: CreateExpenseViewControllerRouterInterface) {
        
        self.router     = router
        self.interactor = interactor
        self.view       = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func paidOptions() -> [String] {
        return (interactor?.paidOptions())!
    }
    
    func newExpenseData() -> [ExpenseData] {
        return (interactor?.newExpenseData())!
    }
    
    func existingExpenseData(expense: Expense) -> [ExpenseData] {
        return (interactor?.existingExpenseData(expense: expense))!
    }
    
    func saveExpense(expense: [ExpenseData]) -> Promise<Bool> {
        return (interactor?.saveExpense(expense: expense))!
    }
    
    func updateExpense(withId id: Int64, expense: [ExpenseData]) -> Promise<Expense?> {
        return (interactor?.updateExpense(withId: id, expense: expense))!
    }
}
