//
//  CreateExpenseViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 19/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// P -> V
protocol CreateExpenseViewControllerInterface: class {
    func initialize()
}

// V -> P
protocol CreateExpenseViewControllerPresenterInterface: class {
    
    func viewDidLoad()
    func paidOptions() -> [String]
    func newExpenseData() -> [ExpenseData]
    func existingExpenseData(expense: Expense) -> [ExpenseData]
    
    func saveExpense(expense: [ExpenseData]) -> Promise<Bool>
    func updateExpense(withId id: Int64, expense: [ExpenseData]) -> Promise<Expense?>
    
    var router: CreateExpenseViewControllerRouterInterface? {get set}
    var interactor: CreateExpenseViewControllerInInteractorInterface? {get set};
}

// P -> R
protocol CreateExpenseViewControllerRouterInterface: class {
}

// P -> I
protocol CreateExpenseViewControllerInInteractorInterface: class {
    
    func paidOptions() -> [String]
    func newExpenseData() -> [ExpenseData]
    func existingExpenseData(expense: Expense) -> [ExpenseData]
    
    func saveExpense(expense: [ExpenseData]) -> Promise<Bool>
    func updateExpense(withId id: Int64, expense: [ExpenseData]) -> Promise<Expense?>
    
    var dataManager: DataManager? {get set}
    var presenter: CreateExpenseViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol CreateExpenseViewControllerOutInteractorInterface: class {
}

