//
//  CreateExpenseViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 19/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class CreateExpenseViewControllerRouter: CreateExpenseViewControllerRouterInterface {
    
    class func createModule(expense: Expense? = nil, loadType: CreateExpenseLoadType? = .new) -> Promise<UINavigationController> {
        
        return firstly { () -> Promise<UINavigationController> in
            let view = BaseViewController.viewControllerWithIdentifier(.createExpense)
            
            guard let viewController = view.value as? CreateExpenseViewController else { return .value(UINavigationController(rootViewController: CreateExpenseViewController()))}
            
            let router: CreateExpenseViewControllerRouterInterface           = CreateExpenseViewControllerRouter()
            let interactor: CreateExpenseViewControllerInInteractorInterface = CreateExpenseViewControllerInteractor(dataManager: DataManager())
            let presenter: CreateExpenseViewControllerPresenterInterface     = CreateExpenseViewControllerPresenter(interactor: interactor, viewController: viewController as CreateExpenseViewControllerInterface, router: router)
            
            
            viewController.expense = expense
            viewController.loadType = loadType
            viewController.presenter = presenter
            
            viewController.modalTransitionStyle = .coverVertical
            interactor.presenter = presenter as? CreateExpenseViewControllerOutInteractorInterface
            
            return .value(UINavigationController(rootViewController: viewController))
        }
    }
}
