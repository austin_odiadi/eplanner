//
//  EventsViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 02/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit

class EventsViewControllerRouter: EventsViewControllerRouterInterface, MainTabBarViewProtocol {
    
    var barButtonItem: UIBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "EditImage"), style: .done, target: nil, action: nil)
    var tabIcon: UIImage = UIImage(named: "eventsIcon")!
    var tabTitle: String = "Events"
    var navTitle: String = "Events"
    
    
    class func createModule() -> Promise<EventsViewController> {
        
        return firstly { () -> Promise<EventsViewController> in
            let view = BaseViewController.viewControllerWithIdentifier(.events)
            
            guard let viewController = view.value as? EventsViewController else { return .value(EventsViewController())}
            
            let router: EventsViewControllerRouterInterface           = EventsViewControllerRouter()
            let interactor: EventsViewControllerInInteractorInterface = EventsViewControllerInteractor(dataManager: DataManager())
            let presenter: EventsViewControllerPresenterInterface     = EventsViewControllerPresenter(interactor: interactor, viewController: viewController as EventsViewControllerInterface, router: router)
            
            viewController.presenter = presenter
            interactor.presenter = presenter as? EventsViewControllerOutInteractorInterface
            
            return .value(viewController)
        }
    }
    
    func configuredViewController() -> UIViewController {
        let view = BaseViewController.viewControllerWithIdentifier(.events)
        
        guard let viewController = view.value as? EventsViewController else { return EventsViewController()}
        
        let interactor: EventsViewControllerInInteractorInterface = EventsViewControllerInteractor(dataManager: DataManager())
        let presenter: EventsViewControllerPresenterInterface     = EventsViewControllerPresenter(interactor: interactor, viewController: viewController as EventsViewControllerInterface, router: self)
        
        viewController.presenter = presenter
        interactor.presenter = presenter as? EventsViewControllerOutInteractorInterface
        
        return viewController
    }
}
