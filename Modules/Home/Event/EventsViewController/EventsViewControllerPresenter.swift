//
//  EventsViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 02/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class EventsViewControllerPresenter: EventsViewControllerPresenterInterface {

    var router: EventsViewControllerRouterInterface?
    private weak var view: EventsViewControllerInterface?
    var interactor: EventsViewControllerInInteractorInterface?
    
    init(interactor: EventsViewControllerInInteractorInterface,
         viewController: EventsViewControllerInterface,
         router: EventsViewControllerRouterInterface) {
        
        self.router     = router
        self.interactor = interactor
        self.view       = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func getEvents() -> Promise<[Event]> {
        return interactor!.fetchEvents()
    }
}

extension EventsViewControllerPresenter: EventsViewControllerOutInteractorInterface {

}
