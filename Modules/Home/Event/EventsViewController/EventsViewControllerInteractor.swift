//
//  EventsViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 02/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class EventsViewControllerInteractor: EventsViewControllerInInteractorInterface {
    
    var dataManager: DataManager?
    var presenter: EventsViewControllerOutInteractorInterface?
    
    init(dataManager: DataManager) {
        self.dataManager = dataManager
    }
    
    func fetchEvents() -> Promise<[Event]> {
        return Promise<[Event]> { seal in
            
            dataManager?.fetchEvents().done { events in
                seal.resolve(.fulfilled(events ?? [Event]()))
            }.cauterize()
        }
    }
    
    func saveEvents(events: [Json]) {
        dataManager?.saveEvents(events: events).done { _ in }.cauterize()
    }
}
