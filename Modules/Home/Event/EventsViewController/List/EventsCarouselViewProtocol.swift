//
//  EventsCarouselViewProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 04/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// MARK: - Delegates -

protocol EventsCarouselViewDelegate: class {
    func events() -> Promise<[Event]>
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath, event: Event)
}

