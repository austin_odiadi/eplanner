//
//  EventsCollectionViewCell.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 04/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class EventsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var eventView: UIView!
    @IBOutlet weak var eventIcon: UIImageView!
    @IBOutlet weak var designContainer: UIView!
    
    @IBOutlet weak var eventDateAndTime: UILabel!
    @IBOutlet weak var eventType: UILabel!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib() 
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        eventView.layoutIfNeeded()
        designContainer.layoutIfNeeded()
        
        eventView.roundedCorners(radius: 5)
        designContainer.roundedCorners(radius: 5)
        //designContainer.dropShadow(color: .lightGray, radius: 5, scale: true)
    }

}
