//
//  EventsCarouselFlowLayout.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 04/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class EventsCarouselFlowLayout: UICollectionViewFlowLayout {
    
    init(size: CGSize) {
        super.init()
        
        self.sectionInset = UIEdgeInsets(top:10, left:0, bottom:5, right:0)
        self.itemSize = size
        self.scrollDirection = .vertical
        self.minimumInteritemSpacing = 0
        self.minimumLineSpacing = 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        
    }
}
