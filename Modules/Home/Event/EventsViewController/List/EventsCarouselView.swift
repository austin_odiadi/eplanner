//
//  EventsCarouselView.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class EventsCarouselView: UIView {

@IBOutlet weak var collectionView: UICollectionView!
    static let cellIdentifier: String = "EventsCellIdentifier"
    
    private var events: [Event]?
    internal weak var delegate: EventsCarouselViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadNib()
    }

    public func initialize(delegate: EventsCarouselViewDelegate) {
        self.delegate = delegate

        let size: CGSize = CGSize(width: self.frame.size.width, height: self.frame.size.height * 0.3)
        collectionView!.collectionViewLayout = EventsCarouselFlowLayout(size: size)
        collectionView?.register(UINib(nibName: "EventsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: EventsCarouselView.cellIdentifier)
        
        reloadData()
    }
    
    func reloadData() {
        delegate?.events()
            .done { event in
                self.events = event
            }.ensure {
                self.collectionView.reloadData()
            }.cauterize()
    }
}

extension EventsCarouselView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return events?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EventsCarouselView.cellIdentifier, for: indexPath) as! EventsCollectionViewCell
        
        if let event = events?[indexPath.row] {
            
            cell.eventType.text = event.type
            cell.eventTitle.text = event.title
            cell.eventDateAndTime.text = event.formattedDate() + " at " + event.time()
            
            if let type = event.type, type != optionDisplayText {
                cell.eventIcon.image = EventIcons.icon(for: eventTypes.index(of: type)!)
            }
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.collectionView(collectionView, didSelectItemAt: indexPath, event: events![indexPath.row])
    }
}
