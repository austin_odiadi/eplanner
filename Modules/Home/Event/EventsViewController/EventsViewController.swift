//
//  EventsViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 02/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import PromiseKit

class EventsViewController: BaseViewController {
    
    @IBOutlet weak var carouselView: EventsCarouselView?
    var presenter: EventsViewControllerPresenterInterface?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func createEvent() {
        CreateEventViewControllerRouter.createModule(loadAs: .new).done { (view) in
            guard let viewController = view.topViewController as? CreateEventViewController else {
                return
            }
            
            viewController.dismissed = { result in
                self.carouselView?.reloadData()
            }
            self.present(view, animated: true, completion: nil)
        }.cauterize()
    }
    
    func showEventDetails(for event: Event) {
        EventDetailsViewControllerRouter.createModule(event: event).done{ view in
            
            view.dismissed = { result in
                if (result as! Bool) {
                    self.carouselView?.reloadData()
                }
            }
            self.navigationController?.pushViewController(view, animated: true)
        }.cauterize()
    }
}

extension EventsViewController: EventsViewControllerInterface {

    //MARK: - Initializer -
    func initialize() {
        carouselView?.initialize(delegate: self)
        carouselView?.backgroundColor = UIColor.plannerBackgroundColor()
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "addIcon"), style: .done, target: self, action: .createEvent)
        
        /*
        let searchController = UISearchController(searchResultsController: nil)
        //searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.scopeButtonTitles = ["All", "Open", "Overdue"]

        searchController.searchBar.showsScopeBar = false
        navigationItem.hidesSearchBarWhenScrolling = true

        navigationItem.searchController = searchController
        definesPresentationContext = true
        */
    }
}

extension EventsViewController: EventsCarouselViewDelegate {
    func events() -> Promise<[Event]> { return presenter!.getEvents() }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath, event: Event) {
        showEventDetails(for: event)
    }    
}

extension EventsViewController: UIToolbarDelegate {
    
    func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
    
    func showBorderLine() {
        findBorderLine().isHidden = false
    }
    
    func hideBorderLine() {
        findBorderLine().isHidden = true
    }
    
    private func findBorderLine() -> UIImageView! {
        return view.subviews
            .flatMap { $0.subviews }
            .compactMap { $0 as? UIImageView }
            .filter { $0.bounds.size.width == view.bounds.size.width }
            .filter { $0.bounds.size.height <= 2 }
            .first
    }
}


fileprivate extension Selector {
    static let createEvent = #selector(CreateEventViewController.createEvent)
}
