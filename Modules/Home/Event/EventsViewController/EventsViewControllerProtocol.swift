//
//  EventsViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 02/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// P -> V
protocol EventsViewControllerInterface: class {
    func initialize()
}

// V -> P
protocol EventsViewControllerPresenterInterface: class {
    
    func viewDidLoad()
    func getEvents() -> Promise<[Event]>
    
    var router: EventsViewControllerRouterInterface? {get set}
    var interactor: EventsViewControllerInInteractorInterface? {get set};
}

// P -> R
protocol EventsViewControllerRouterInterface: class {
}

// P -> I
protocol EventsViewControllerInInteractorInterface: class {
    func fetchEvents() -> Promise<[Event]>
    
    var dataManager: DataManager? {get set}
    var presenter: EventsViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol EventsViewControllerOutInteractorInterface: class {
    //func availableEvents(events: Promise<[Event]?>)
}

