//
//  CreateEventViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 04/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit

class CreateEventViewControllerRouter: CreateEventViewControllerRouterInterface {
    
    class func createModule(event: Event? = Event(), loadAs loadType: CreateEventLoadType) -> Promise<UINavigationController> {
        
        return firstly { () -> Promise<UINavigationController> in
            let view = BaseViewController.viewControllerWithIdentifier(.createEvent)
            
            guard let viewController = view.value as? CreateEventViewController else { return .value(UINavigationController(rootViewController: CreateEventViewController()))}
            
            let router: CreateEventViewControllerRouterInterface           = CreateEventViewControllerRouter()
            let interactor: CreateEventViewControllerInInteractorInterface = CreateEventViewControllerInteractor(dataManager: DataManager())
            let presenter: CreateEventViewControllerPresenterInterface     = CreateEventViewControllerPresenter(interactor: interactor, viewController: viewController as CreateEventViewControllerInterface, router: router)
            
            viewController.event = event
            viewController.loadType = loadType
            viewController.presenter = presenter
            viewController.modalTransitionStyle = .coverVertical
            interactor.presenter = presenter as? CreateEventViewControllerOutInteractorInterface
            
            return .value(UINavigationController(rootViewController: viewController))
        }
    }
    
    func datePicker() -> UIDatePicker {
        let datePicker: UIDatePicker = UIDatePicker()
        datePicker.datePickerMode = .dateAndTime
        
        return datePicker
    }
}

