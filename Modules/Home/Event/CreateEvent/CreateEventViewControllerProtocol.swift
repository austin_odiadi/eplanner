//
//  CreateEventViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 04/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// P -> V
protocol CreateEventViewControllerInterface: class {
    func initialize()
    func eventUpdated(with event : Event?);
}

// V -> P
protocol CreateEventViewControllerPresenterInterface: class {
    
    func viewDidLoad()
    func datePicker() -> UIDatePicker
    func dateAsString(date: Date) -> String?
    func saveEvent(events: [Json]) -> Promise<Bool>
    func getTableData(withEvent event: Event) -> PLTableData?
    func updateEvent(withId id: Int64, update: [EventData])
    
    var router: CreateEventViewControllerRouterInterface? {get set}
    var interactor: CreateEventViewControllerInInteractorInterface? {get set};
}

// P -> R
protocol CreateEventViewControllerRouterInterface: class {
    func datePicker() -> UIDatePicker 
}

// P -> I
protocol CreateEventViewControllerInInteractorInterface: class {
    
    func format(date: Date) -> String?
    func tableData(event: Event) -> PLTableData
    func saveEvent(events: [Json]) -> Promise<Bool>
    func updateEvent(withId id: Int64, update: [EventData])
    
    var dataManager: DataManager? {get set}
    var presenter: CreateEventViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol CreateEventViewControllerOutInteractorInterface: class {
    func eventUpdated(with event: Event?);
}
