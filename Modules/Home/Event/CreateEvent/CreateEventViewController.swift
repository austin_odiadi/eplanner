//
//  CreateEventViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 04/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class CreateEventViewController: BaseViewController {
    

    @IBOutlet weak var eventIcon: UIImageView!
    @IBOutlet weak var pickerView: PickerView!
    @IBOutlet weak var tableView: PLTableView!
    @IBOutlet weak var eventImageDesignContainer: UIView!
    @IBOutlet weak var pickerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var background: UIView!
    
    var visibleEventRow: EventRows?
    @IBOutlet weak var scroller: UIScrollView!
    var presenter: CreateEventViewControllerPresenterInterface?
    
    var event: Event?
    var loadType: CreateEventLoadType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    @objc func createEvent() {
        view.endEditing(true)
        if let data = tableView.tableData?.data as! [EventData]? {
            indicatorLoading(state: true)
            presenter?.saveEvent(events: data.asJson()).done{ _ in
                
                self.finishCreate()
                self.indicatorLoading(state: false)
                
                }.cauterize()
        }
    }
    
    @objc func saveUpdate() {
        view.endEditing(true)
        indicatorLoading(state: true)
        presenter?.updateEvent(withId: (event?.id)!, update: tableView.tableData?.data as! [EventData])
    }
    
    @objc func cancelEvent() {
        finishCreate()
    }
    
    func finishCreate() {
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
        dismissed!(true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        eventImageDesignContainer.layoutIfNeeded()
        eventImageDesignContainer.roundedCorners(radius: 5)
        eventImageDesignContainer.dropShadow(color: .lightGray, radius: 5, scale: true)
    }
    
    func hidePicker(end selection: Bool? = false, endEditing: Bool? = true, hidden: @escaping () -> Void) {
        pickerTopConstraint = pickerTopConstraint.setMultiplier(multiplier: 1)
        
        UIView.animate(withDuration: 0.25, animations: {
            if endEditing! {
                self.view.endEditing(true)
            }
            if selection! {
                self.scroller.contentOffset = CGPoint(x: 0, y: -64)
            }
            self.pickerView.layoutIfNeeded()
        }) { (_) in
            hidden()
        }
    }
    
    func updateWithPickerSelection(selection: Any?, _ type: PickerType) {
        switch type {
        case .date:
            guard let date: Date = selection as? Date,
                let dateString = presenter?.dateAsString(date: date) else {
                    return
            }
            
            tableView.update(object: dateString, at: IndexPath(row: 2, section: 0))
            break
        case .custom:
            guard let selected = selection else {
                return
            }
            
            eventIcon.image = EventIcons.icon(for: eventTypes.index(of: selected as! EventType)!)
            tableView.update(object: selected, at: IndexPath(row: 1, section: 0))
            break
        }
    }
}

extension CreateEventViewController: CreateEventViewControllerInterface {
    
    func initialize() {
        
        switch loadType {
        case .new?:
            self.navigationItem.setHidesBackButton(true, animated:true)
            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Create", style: .done, target: self, action: .createEvent)
            break
        case .existing?:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Save", style: .done, target: self, action: .saveUpdateEvent)
            break
        case .none: break
        }

        if let type = event?.type, type != optionDisplayText {
            eventIcon.image = EventIcons.icon(for: eventTypes.index(of: type)!)
        }
        
        background.backgroundColor = UIColor.plannerBackgroundColor()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "Cancel", style: .done, target: self, action: .cancelEvent)
        pickerView.delegate = self
        
        tableView.delegate = self
        tableView.scroll(enable: false)
        tableView.loadData(data: presenter?.getTableData(withEvent: event!))
    }
    
    func eventUpdated(with event: Event?) {
        
        view.endEditing(true)
        indicatorLoading(state: false)

        dismiss(animated: true, completion: nil)
        dismissed?(event as Any)
    }
}

extension CreateEventViewController: PickerViewDelegate {
    func picker(_ view: PickerView, _ selection: Any?, _ done: Bool, _ type: PickerType) {
        updateWithPickerSelection(selection: selection, type)
        
        hidePicker(end: true, hidden: {
            self.visibleEventRow = .none
        })
    }
    
    func picker(_ view: PickerView, _ selection: Any?, type: PickerType) {
        updateWithPickerSelection(selection: selection, type)
    }
}

extension CreateEventViewController: TextfieldTableViewCellDelegate {
    func cell(_ cell: TextfieldTableViewCell, didEndEditing title: String) {
        tableView.update(object: title, at: IndexPath(row: 0, section: 0))
    }
    
    func cell(_ cell: TextfieldTableViewCell, didBeginEditing title: String) {
        hidePicker(end: true, endEditing: false, hidden: {
            self.visibleEventRow = .none
        })
    }
}

extension CreateEventViewController: PLTableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath, object: Any?) {
        guard let event: EventData = object as? EventData else {
            return
        }
        
        if visibleEventRow == event.type {
            return
        }
        
        visibleEventRow = EventRows(rawValue: indexPath.row)!
        hidePicker(end: event.type == EventRows.eventTitle, hidden: {
            
            switch event.type {
            case .eventType?:
                self.pickerView.loadPicker(picker: Picker(data: [eventTypes]))
                break
            case .eventDateAndTime?:
                self.pickerView.loadDatePicker(picker: (self.presenter?.datePicker())!)
                break
            default: return
            }
            
            self.pickerTopConstraint = self.pickerTopConstraint.setMultiplier(multiplier: 0.63)
            UIView.animate(withDuration: 0.25) { self.view.layoutIfNeeded() }
        })
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {

    }
}

fileprivate extension Selector {
    static let cancelEvent = #selector(CreateEventViewController.cancelEvent)
    static let createEvent = #selector(CreateEventViewController.createEvent)
    static let saveUpdateEvent = #selector(CreateEventViewController.saveUpdate)
}

