//
//  CreateEventViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 04/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit


class CreateEventViewControllerInteractor: CreateEventViewControllerInInteractorInterface {
    
    var dataManager: DataManager?
    var presenter: CreateEventViewControllerOutInteractorInterface?

    init(dataManager: DataManager) {
        self.dataManager = dataManager
    }
    
    func tableData(event: Event) -> PLTableData {
 
        return PLTableData(data: EventData.data(with: [.eventTitle, .eventType, .eventDateAndTime],  event: event),
                           configuredCell: { (view, tableView, indexPath, data) -> (UITableViewCell)? in
                                var cell = UITableViewCell()
                            
                                let objectAtRow = (data as! [EventData])[indexPath.row]
                            
                                switch EventRows(rawValue: indexPath.row) {
                                case .eventTitle?:
                                    cell = tableView.dequeueReusableCell(withIdentifier: textFieldIdentifier, for: indexPath)
                                    
                                    if let cast = cell as? TextfieldTableViewCell {
                                        cast.textField.text = objectAtRow.value
                                        cast.delegate = view.delegate as? TextfieldTableViewCellDelegate
                                    }
                                    
                                    break
                                default:
                                    cell = tableView.dequeueReusableCell(withIdentifier: labelIdentifier, for: indexPath)
                                    
                                    if let cast = cell as? LabelTableViewCell {
                                        cast.title.text = objectAtRow.title
                                        cast.selectButton.setTitle(objectAtRow.value, for: .normal)
                                    }
                                    break
                                }
                            
                                return cell
                            
                            }, registeredCell: { tableView in
            
                                tableView.register(UINib(nibName: "LabelTableViewCell", bundle: nil), forCellReuseIdentifier: labelIdentifier)
                                tableView.register(UINib(nibName: "TextfieldTableViewCell", bundle: nil), forCellReuseIdentifier: textFieldIdentifier)
                                
                                return ()
                            }, sizes: nil,
                            updater: { (dataSource, update, indexPath) in
                                
                                if let data = dataSource as? [EventData] {
                                    for (index, event) in data.enumerated() {
                                        if index == indexPath.row {
                                            event.value = update as? String
                                        }
                                    }
                                    
                                    //data.filter { $0.type == EventRows(rawValue: indexPath.row)}.first?.value = update as? String
                                }
                                return ()
                            })
    }
    
    func format(date: Date) -> String? {
       return PlannerCommons.format(date: date)
    }
    
    func saveEvent(events: [Json]) -> Promise<Bool> {
        return (dataManager?.saveEvents(events: events))!
    }
    
    func updateEvent(withId id: Int64, update: [EventData]) {
        dataManager?.updateEvent(withId: id, update: update).done { event in
            self.presenter?.eventUpdated(with: event)
        }.cauterize()
    }
}
