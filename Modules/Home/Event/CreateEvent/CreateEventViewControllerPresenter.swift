//
//  CreateEventViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 04/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class CreateEventViewControllerPresenter: CreateEventViewControllerPresenterInterface {
   
    

    var router: CreateEventViewControllerRouterInterface?
    private weak var view: CreateEventViewControllerInterface?
    var interactor: CreateEventViewControllerInInteractorInterface?
    
    init(interactor: CreateEventViewControllerInInteractorInterface,
         viewController: CreateEventViewControllerInterface,
         router: CreateEventViewControllerRouterInterface) {
        
        self.router     = router
        self.interactor = interactor
        self.view       = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func getTableData(withEvent event: Event) -> PLTableData? {
        return interactor?.tableData(event: event)
    }
    
    func datePicker() -> UIDatePicker {
        return (router?.datePicker())!
    }
    
    func dateAsString(date: Date) -> String? {
        return interactor?.format(date: date)
    }
    
    func saveEvent(events: [Json]) -> Promise<Bool> {
        return (interactor?.saveEvent(events: events))!
    }
    
    func updateEvent(withId id: Int64, update: [EventData]) {
        interactor?.updateEvent(withId: id, update: update)
    }
}

extension CreateEventViewControllerPresenter: CreateEventViewControllerOutInteractorInterface {
    func eventUpdated(with event: Event?) {
        view?.eventUpdated(with: event)
    }
}
