//
//  LabelTableViewCell.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 05/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class LabelTableViewCell: UITableViewCell {

    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
