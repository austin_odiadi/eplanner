//
//  TexfieldTableViewCell.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 05/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

protocol TextfieldTableViewCellDelegate: class {
    func cell(_ cell: TextfieldTableViewCell, didEndEditing title: String)
    func cell(_ cell: TextfieldTableViewCell, didBeginEditing title: String)
}

class TextfieldTableViewCell: UITableViewCell {
    
    weak var delegate: TextfieldTableViewCellDelegate?
    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension TextfieldTableViewCell: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        
        delegate?.cell(self, didEndEditing: textField.text!)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.cell(self, didBeginEditing: textField.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}
