//
//  EventDetailsViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 07/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit


class EventDetailsViewControllerInteractor: EventDetailsViewControllerInInteractorInterface {
    
    var dataManager: DataManager?
    var presenter: EventDetailsViewControllerOutInteractorInterface?

    init(dataManager: DataManager) {
        self.dataManager = dataManager
    }
    
    func tableData(event: Event) -> PLTableData {
        let data = EventData.data(with: [.eventType, .eventDate, .eventTime], event: event)
        
        return
            PLTableData(data: data, configuredCell: { (view, tableView, indexPath, data) -> (UITableViewCell)? in
        
                let cell = tableView.dequeueReusableCell(withIdentifier: eventDetailsIdentifier, for: indexPath) as! EventDetailsTableViewCell
            
                let objectAtRow = (data as! [EventData])[indexPath.row]
                cell.title.text = objectAtRow.value
                cell.icon.image = objectAtRow.icon
            
                cell.selectionStyle = .none
                return cell
            
            }, registeredCell: { tableView in

                tableView.register(UINib(nibName: "EventDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: eventDetailsIdentifier)
                return ()
            }, sizes: PLTableDataSize(44, 0, 20))
    }
    
    func deleteEvent(withId id: Int64) -> Promise<Bool> {
        return (dataManager?.deleteEvent(withId: id))!
    }
}
