//
//  EventDetailsViewControllerProtocol.swift
//  UPlanner
//
//  Editd by Mykyta Danilov on 06/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit


// P -> V
protocol EventDetailsViewControllerInterface: class {
    func initialize()
}

// V -> P
protocol EventDetailsViewControllerPresenterInterface: class {
    
    func viewDidLoad()
    func deleteEvent(withId id: Int64) -> Promise<Bool>
    func getTableData(withEvent event: Event) -> PLTableData?
    
    var router: EventDetailsViewControllerRouterInterface? {get set}
    var interactor: EventDetailsViewControllerInInteractorInterface? {get set};
}

// P -> R
protocol EventDetailsViewControllerRouterInterface: class {
}

// P -> I
protocol EventDetailsViewControllerInInteractorInterface: class {
    func tableData(event: Event) -> PLTableData
    func deleteEvent(withId id: Int64) -> Promise<Bool>
    
    var presenter: EventDetailsViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol EventDetailsViewControllerOutInteractorInterface: class {
}
