//
//  EventDetailsViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 06/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class EventDetailsViewController: BaseViewController {
    var presenter: EventDetailsViewControllerPresenterInterface?
    
    var event: Event?
    var isUpdated: Bool = false
    
    @IBOutlet weak var scroller: UIScrollView!
    @IBOutlet weak var tableView: PLTableView!
    @IBOutlet weak var eventImageDesignContainer: UIView!
    
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var countdown: CountDown!
    
    @IBOutlet weak var eventIcon: UIImageView!
    override func loadView() {
        super.loadView()
        countdown.startCountDown(event: CDObject(eventStart: nil, eventEnd: (event?.date)!))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func willMove(toParent parent: UIViewController?) {
        if (parent == nil) {
            dismissed?(isUpdated)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        imageContainer.layoutIfNeeded()
        eventImageDesignContainer.layoutIfNeeded()
       
        imageContainer.roundedCorners(radius: 5)
        eventImageDesignContainer.roundedCorners(radius: 5)
        eventImageDesignContainer.dropShadow(color: .lightGray, radius: 5, scale: true)
    }
    
    @objc func editEvent() {
        CreateEventViewControllerRouter.createModule(event: event, loadAs: .existing).done { (view) in
            guard let viewController = view.topViewController as? CreateEventViewController else {
                return
            }
            
            viewController.dismissed = { result in
                guard let event = result as? Event else  {
                    return
                }
                
                self.event = event
                self.isUpdated = true
                self.update()
            }
            self.present(view, animated: true, completion: nil)
            }.cauterize()
    }
    
    @IBAction func deletePushed(_ sender: UIButton) {
        indicatorLoading(state: true)
        presenter?.deleteEvent(withId: (event?.id)!).done { success in
            self.indicatorLoading(state: false)
            if success {
                self.dismissed?(true)
                self.navigationController?.popViewController(animated: true)
            }
        }.cauterize()
    }
    
    func update() {
        eventTitle.text = event?.title
        if let type = event?.type, type != optionDisplayText {
            eventIcon.image = EventIcons.icon(for: eventTypes.index(of: type)!)
        }
        
        tableView.loadData(data: presenter?.getTableData(withEvent: event!))
        countdown.startCountDown(event: CDObject(eventStart: nil, eventEnd: (event?.date)!))
    }
}

extension EventDetailsViewController: EventDetailsViewControllerInterface {
    func initialize() {
        self.navigationItem.setHidesBackButton(false, animated: true)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Edit", style: .done, target: self, action: .editEvent)

        tableView.delegate = self
        tableView.scroll(enable: false)
        update()
    }
}

extension EventDetailsViewController: PLTableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath, object: Any?) {
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }
}


fileprivate extension Selector {
    static let editEvent = #selector(EventDetailsViewController.editEvent)
}
