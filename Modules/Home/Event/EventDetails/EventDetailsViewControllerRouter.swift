//
//  EventDetailsViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 07/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class EventDetailsViewControllerRouter: EventDetailsViewControllerRouterInterface {
    
    class func createModule(event: Event) -> Promise<EventDetailsViewController> {
        
        return firstly { () -> Promise<EventDetailsViewController> in
            let view = BaseViewController.viewControllerWithIdentifier(.eventDetails)
            
            guard let viewController = view.value as? EventDetailsViewController else { return .value(EventDetailsViewController())}
            
            let router: EventDetailsViewControllerRouterInterface           = EventDetailsViewControllerRouter()
            let interactor: EventDetailsViewControllerInInteractorInterface = EventDetailsViewControllerInteractor(dataManager: DataManager())
            let presenter: EventDetailsViewControllerPresenterInterface     = EventDetailsViewControllerPresenter(interactor: interactor, viewController: viewController as EventDetailsViewControllerInterface, router: router)
            
            viewController.event = event
            viewController.presenter = presenter
            interactor.presenter = presenter as? EventDetailsViewControllerOutInteractorInterface
            
            return .value(viewController)
        }
    }
}

