//
//  EventDetailsViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 07/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class EventDetailsViewControllerPresenter: EventDetailsViewControllerPresenterInterface {
    
    var router: EventDetailsViewControllerRouterInterface?
    private weak var view: EventDetailsViewControllerInterface?
    var interactor: EventDetailsViewControllerInInteractorInterface?
    
    init(interactor: EventDetailsViewControllerInInteractorInterface,
         viewController: EventDetailsViewControllerInterface,
         router: EventDetailsViewControllerRouterInterface) {
        
        self.router     = router
        self.interactor = interactor
        self.view       = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func getTableData(withEvent event: Event) -> PLTableData? {
       return interactor?.tableData(event: event)
    }
    
    func deleteEvent(withId id: Int64) -> Promise<Bool> {
        return (interactor?.deleteEvent(withId: id))!
    }
}
