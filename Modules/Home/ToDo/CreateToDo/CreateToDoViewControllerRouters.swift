//
//  CreateToDoViewControllerRouters.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 13/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class CreateToDoViewControllerRouter: CreateToDoViewControllerRouterInterface {
    
    class func createModule(todo: ToDo? = nil, loadType: CreateToDoLoadType? = .new) -> Promise<UINavigationController> {
        
        return firstly { () -> Promise<UINavigationController> in
            let view = BaseViewController.viewControllerWithIdentifier(.createToDo)
            
            guard let viewController = view.value as? CreateToDoViewController else { return .value(UINavigationController(rootViewController: CreateToDoViewController()))}
            
            let router: CreateToDoViewControllerRouterInterface           = CreateToDoViewControllerRouter()
            let interactor: CreateToDoViewControllerInInteractorInterface = CreateToDoViewControllerInteractor(dataManager: DataManager())
            let presenter: CreateToDoViewControllerPresenterInterface     = CreateToDoViewControllerPresenter(interactor: interactor, viewController: viewController as CreateToDoViewControllerInterface, router: router)
            
            viewController.todo = todo
            viewController.loadType = loadType
            viewController.presenter = presenter
            viewController.modalTransitionStyle = .coverVertical
            interactor.presenter = presenter as? CreateToDoViewControllerOutInteractorInterface
            
            return .value(UINavigationController(rootViewController: viewController))
        }
    }
    
    func datePicker() -> UIDatePicker {
        let datePicker: UIDatePicker = UIDatePicker()
        datePicker.datePickerMode = .dateAndTime
        
        return datePicker
    }
}
