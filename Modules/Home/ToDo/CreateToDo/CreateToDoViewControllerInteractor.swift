//
//  CreateToDoViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 13/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit


class CreateToDoViewControllerInteractor: CreateToDoViewControllerInInteractorInterface {
    
    var dataManager: DataManager?
    var presenter: CreateToDoViewControllerOutInteractorInterface?
    
    init(dataManager: DataManager) {
        self.dataManager = dataManager
    }
    
    func newToDoData() -> [[ToDoData]] {
        var description: [ToDoData] = [ToDoData]()
        description.append(ToDoData(title: "To-Do Title", value: "", index: 0))
        description.append(ToDoData(title: "Due Date", value: "", index: 1))
        
        var note: [ToDoData] = [ToDoData]()
        note.append(ToDoData(title: "E.g. Remind me to call the tailor", value: "", index: 2))
        
        return [description, note]
    }
    
    func existingToDoData(todo: ToDo) -> [[ToDoData]] {
        return ToDoData.data(with: todo)
    }
    
    func format(date: Date) -> String? {
        return PlannerCommons.format(date: date)
    }
    
    func saveToDoList(todos: [ToDoData]) -> Promise<Bool> {
        return (dataManager?.saveToDos(todos: todos))!
    }
    
    func updateToDo(withId id: Int64, update: [ToDoData]) -> Promise<ToDo?> {
        return (dataManager?.updateToDo(withId: id, update: update))!
    }
}
