//
//  CreateToDoViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 13/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// P -> V
protocol CreateToDoViewControllerInterface: class {
    func initialize()
}

// V -> P
protocol CreateToDoViewControllerPresenterInterface: class {
    
    func viewDidLoad()
    func datePicker() -> UIDatePicker
    func newToDoData() -> [[ToDoData]]
    func dateAsString(date: Date) -> String?
    func existingToDoData(todo: ToDo) -> [[ToDoData]] 
    func saveToDoList(todos: [ToDoData]) -> Promise<Bool>
    func updateToDo(withId id: Int64, update: [ToDoData]) -> Promise<ToDo?>
    
    var router: CreateToDoViewControllerRouterInterface? {get set}
    var interactor: CreateToDoViewControllerInInteractorInterface? {get set};
}

// P -> R
protocol CreateToDoViewControllerRouterInterface: class {
    func datePicker() -> UIDatePicker
}

// P -> I
protocol CreateToDoViewControllerInInteractorInterface: class {
    func newToDoData() -> [[ToDoData]]
    func format(date: Date) -> String?
    func existingToDoData(todo: ToDo) -> [[ToDoData]] 
    func saveToDoList(todos: [ToDoData]) -> Promise<Bool>
    func updateToDo(withId id: Int64, update: [ToDoData]) -> Promise<ToDo?>
    
    var dataManager: DataManager? {get set}
    var presenter: CreateToDoViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol CreateToDoViewControllerOutInteractorInterface: class {
}

