//
//  CreateToDoViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 13/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit

class CreateToDoViewControllerPresenter: CreateToDoViewControllerPresenterInterface {
    
    var router: CreateToDoViewControllerRouterInterface?
    private weak var view: CreateToDoViewControllerInterface?
    var interactor: CreateToDoViewControllerInInteractorInterface?
    
    init(interactor: CreateToDoViewControllerInInteractorInterface,
         viewController: CreateToDoViewControllerInterface,
         router: CreateToDoViewControllerRouterInterface) {
        
        self.router     = router
        self.interactor = interactor
        self.view       = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func datePicker() -> UIDatePicker {
        return (router?.datePicker())!
    }
    
    func newToDoData() -> [[ToDoData]] {
        return (interactor?.newToDoData())!
    }
    
    func dateAsString(date: Date) -> String? {
        return interactor?.format(date: date)
    }
    
    func saveToDoList(todos: [ToDoData]) -> Promise<Bool> {
        return (interactor?.saveToDoList(todos: todos))!
    }
    
    func existingToDoData(todo: ToDo) -> [[ToDoData]] {
        return (interactor?.existingToDoData(todo: todo))!
    }
    
    func updateToDo(withId id: Int64, update: [ToDoData]) -> Promise<ToDo?> {
        return (interactor?.updateToDo(withId: id, update: update))!
    }
}
