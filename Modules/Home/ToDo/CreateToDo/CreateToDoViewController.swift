//
//  CreateToDoViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 13/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class CreateToDoViewController: BaseViewController {
    var presenter: CreateToDoViewControllerPresenterInterface?
    var toDoData: [[ToDoData]] = [[ToDoData]]()
    
    var todo: ToDo?
    var loadType: CreateToDoLoadType?
    var isPickerVisible: Bool = false
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickerContainerTop: NSLayoutConstraint!
    @IBOutlet weak var pickerView: PickerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    @objc func cancelToDo() {
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
    
    @objc func saveToDo() {
        view.endEditing(true)
        indicatorLoading(state: true)
        switch loadType {
        case .new?:
            presenter?.saveToDoList(todos: toDoData.reduce([], +)).done { success in
                self.indicatorLoading(state: true)
                
                self.dismissed?(success)
                self.dismiss(animated: true, completion: nil)
                
                }.cauterize()
            break
        default:
            presenter?.updateToDo(withId: (todo?.id)!, update: toDoData.reduce([], +)).done { todo in
                
                self.dismissed?(todo as Any)
                self.dismiss(animated: true, completion: nil)
                
            }.cauterize()
            break
        }
    }
    
    func hidePicker(end selection: Bool? = false, hidden: @escaping () -> Void) {
        if !isPickerVisible {
            return
        }
        
        pickerContainerTop = pickerContainerTop.setMultiplier(multiplier: 1)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.endEditing(true)
            self.pickerView.layoutIfNeeded()
        }) { (_) in
            self.isPickerVisible = false
            hidden()
        }
    }
    
    func updatedSelection(selection :Any) {
        guard let date: Date = selection as? Date,
            let dateString = presenter?.dateAsString(date: date) else {
                return
        }
        
        toDoData.first?.last?.value = dateString
        tableView.reloadData()
    }
}

extension CreateToDoViewController: CreateToDoViewControllerInterface {
    func initialize() {
        view.backgroundColor = UIColor.plannerBackgroundColor()
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Save", style: .done, target: self, action: .saveToDo)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "Cancel", style: .done, target: self, action: .cancelToDo)
        
        pickerView.delegate = self
        tableView.register(UINib(nibName: "TextfieldTableViewCell", bundle: nil), forCellReuseIdentifier: textFieldIdentifier)
        
        switch loadType {
        case .new?:
            toDoData = (presenter?.newToDoData())!
            break
        case .existing?:
            if let existingToDo = todo {
                toDoData = (presenter?.existingToDoData(todo: existingToDo))!
            }
            break
        case .none:
            break
        }
    }
}

extension CreateToDoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDoData[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return toDoData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: textFieldIdentifier, for: indexPath) as! TextfieldTableViewCell
        
        cell.textField.isUserInteractionEnabled = true
        cell.textField.text = toDoData[indexPath.section][indexPath.row].value
        cell.textField.placeholder = toDoData[indexPath.section][indexPath.row].title
        
        if indexPath.section == 0 {
            if indexPath.row == 1 {
                cell.textField.isUserInteractionEnabled = false
            }
        }
        else {
            cell.textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.top
        }
        
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 44 : 200
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return HeaderView(title: section == 0 ? "" : "DESCRIPTION", showButton: false)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 1{
            
            self.view.endEditing(true)
            self.pickerView.loadDatePicker(picker: (self.presenter?.datePicker())!)
            
            self.pickerContainerTop = self.pickerContainerTop.setMultiplier(multiplier: 0.63)
            UIView.animate(withDuration: 0.25, animations: {
                self.view.layoutIfNeeded()
            }) { (_) in
                self.isPickerVisible = true
            }
        }
    }
}

extension CreateToDoViewController: TextfieldTableViewCellDelegate {
    func cell(_ cell: TextfieldTableViewCell, didEndEditing title: String) {
        
        if let indexPath = tableView.indexPath(for: cell) {
            if indexPath.section == 0 && indexPath.row == 0 {
                toDoData[indexPath.section][indexPath.row].value = title
            }
            else if indexPath.section == 1 {
                toDoData[indexPath.section][indexPath.row].value = title
            }
        }
    }
    
    func cell(_ cell: TextfieldTableViewCell, didBeginEditing title: String) {
        
        if let indexPath = tableView.indexPath(for: cell) {
            if indexPath.section == 0 && indexPath.row == 1 {
                
            }
            else {
                hidePicker(hidden: { })
            }
        }
    }
}

extension CreateToDoViewController: PickerViewDelegate {
    func picker(_ view: PickerView, _ selection: Any?, _ done: Bool, _ type: PickerType) {

        updatedSelection(selection: selection as Any)
        hidePicker(end: true, hidden: { })
    }
    
    func picker(_ view: PickerView, _ selection: Any?, type: PickerType) {
        updatedSelection(selection: selection as Any)
    }
}

fileprivate extension Selector {
    static let cancelToDo = #selector(CreateToDoViewController.cancelToDo)
    static let saveToDo = #selector(CreateToDoViewController.saveToDo)
}

