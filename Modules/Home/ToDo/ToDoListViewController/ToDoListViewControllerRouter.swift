//
//  ToDoListViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit

class ToDoListViewControllerRouter: ToDoListViewControllerRouterInterface, MainTabBarViewProtocol {

    var tabIcon: UIImage = UIImage(named:"todoIcon")!
    var tabTitle: String = "To-Do List"
    var navTitle: String = "To-Do List"
    
    class func createModule() -> Promise<ToDoListViewController> {
        
        return firstly { () -> Promise<ToDoListViewController> in
            let view = BaseViewController.viewControllerWithIdentifier(.todoList)
            
            guard let viewController = view.value as? ToDoListViewController else { return .value(ToDoListViewController())}
            
            let router: ToDoListViewControllerRouterInterface           = ToDoListViewControllerRouter()
            let interactor: ToDoListViewControllerInInteractorInterface = ToDoListViewControllerInteractor(dataManager: DataManager())
            let presenter: ToDoListViewControllerPresenterInterface     = ToDoListViewControllerPresenter(interactor: interactor, viewController: viewController as ToDoListViewControllerInterface, router: router)
            
            viewController.presenter = presenter
            interactor.presenter = presenter as? ToDoListViewControllerOutInteractorInterface
            
            return .value(viewController)
        }
    }
    
    func configuredViewController() -> UIViewController {
        let view = BaseViewController.viewControllerWithIdentifier(.todoList)
        
        guard let viewController = view.value as? ToDoListViewController else { return ToDoListViewController()}
        
        let interactor: ToDoListViewControllerInInteractorInterface = ToDoListViewControllerInteractor(dataManager: DataManager())
        let presenter: ToDoListViewControllerPresenterInterface     = ToDoListViewControllerPresenter(interactor: interactor, viewController: viewController as ToDoListViewControllerInterface, router: self)
        
        viewController.presenter = presenter
        interactor.presenter = presenter as? ToDoListViewControllerOutInteractorInterface
        
        return viewController
    }
}
