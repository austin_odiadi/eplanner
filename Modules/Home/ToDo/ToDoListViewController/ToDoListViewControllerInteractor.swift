//
//  ToDoListViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit


class ToDoListViewControllerInteractor: ToDoListViewControllerInInteractorInterface {
    
    var dataManager: DataManager?
    var presenter: ToDoListViewControllerOutInteractorInterface?
    
    init(dataManager: DataManager) {
        self.dataManager = dataManager
    }
    
    func tableData() -> Promise<PLTableData?> {
        
        return Promise<PLTableData?> { seal in
            dataManager?.fetchToDos().done { events in
                seal.resolve(.fulfilled(
                    PLTableData(data: events, configuredCell: { (view, tableView, indexPath, data) -> (UITableViewCell)? in
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: todoIdentifier, for: indexPath) as! ToDoTableViewCell
                        
                        let todo = (data as! [ToDo])[indexPath.row]
                        cell.title.text     = todo.name
                        cell.subTitle.text  = todo.subtitle()
                        cell.completedButton.isHidden = todo.completed == 0 ? true : false
                        
                        if !cell.completedButton.isHidden {
                            cell.container.backgroundColor = UIColor.init(rgb: 0xCCE5FF)
                        }
                        else {
                            cell.container.backgroundColor = .white
                        }
                        cell.selectionStyle = .none
                        return cell
                        
                    }, registeredCell: { tableView in
                        
                        tableView.separatorColor = .clear
                        tableView.register(UINib(nibName: "ToDoTableViewCell", bundle: nil), forCellReuseIdentifier: todoIdentifier)
                        return ()
                    }, sizes: PLTableDataSize(70, 8)) { (view, dataSource, section) -> (UIView)? in
                        return HeaderView(title: "", showButton: false)
                    }
                    ))
                }.cauterize()
        }
    }
}
