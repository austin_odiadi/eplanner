//
//  ToDoListViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// P -> V
protocol ToDoListViewControllerInterface: class {
    func initialize()
}

// V -> P
protocol ToDoListViewControllerPresenterInterface: class {
    func viewDidLoad()
    func getTableData() -> Promise<PLTableData?>
    
    var router: ToDoListViewControllerRouterInterface? {get set}
    var interactor: ToDoListViewControllerInInteractorInterface? {get set}
}

// P -> R
protocol ToDoListViewControllerRouterInterface: class {
}

// P -> I
protocol ToDoListViewControllerInInteractorInterface: class {
    func tableData() -> Promise<PLTableData?>
    
    var dataManager: DataManager? {get set}
    var presenter: ToDoListViewControllerOutInteractorInterface? {get set}
}

// I -> P
protocol ToDoListViewControllerOutInteractorInterface: class {
    func availableToDoList(todos: [ToDo]?)
}
