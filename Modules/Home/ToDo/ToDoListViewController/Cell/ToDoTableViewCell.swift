//
//  ToDoTableViewCell.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 13/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class ToDoTableViewCell: UITableViewCell {

    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var completedButton: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        container.layoutIfNeeded()
        container.roundedCorners(radius: 5)
        container.dropShadow(color: .lightGray, radius: 5, scale: true)
    }
}
