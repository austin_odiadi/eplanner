//
//  ToDoListViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class ToDoListViewControllerPresenter: ToDoListViewControllerPresenterInterface {
    
    var router: ToDoListViewControllerRouterInterface?
    private weak var view: ToDoListViewControllerInterface?
    var interactor: ToDoListViewControllerInInteractorInterface?
    
    init(interactor: ToDoListViewControllerInInteractorInterface,
         viewController: ToDoListViewControllerInterface,
         router: ToDoListViewControllerRouterInterface) {
        
        self.router     = router
        self.interactor = interactor
        self.view       = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func getTableData() -> Promise<PLTableData?> {
        return (interactor?.tableData())!
    }
}

extension ToDoListViewControllerPresenter: ToDoListViewControllerOutInteractorInterface {
    
    func availableToDoList(todos: [ToDo]?) {
    }
}
