//
//  ToDoListViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class ToDoListViewController: BaseViewController {
    
    var searchController: UISearchController?
    var presenter: ToDoListViewControllerPresenterInterface?
    @IBOutlet weak var tableView: PLTableView!
    
    var fullData: PLTableData?
    var filteredData: PLTableData?
    var selectedScopeButton: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchController?.searchBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        searchController?.searchBar.isHidden = true
    }
    
    @objc func createToDo() {
        CreateToDoViewControllerRouter.createModule().done { view in
            guard let viewController = view.topViewController as? CreateToDoViewController else {
                return
            }
            
            viewController.dismissed = { result in
                if (result as! Bool) {
                    self.updateData()
                }
            }
            
            self.present(view, animated: true, completion: nil)
        }.cauterize()
    }
    
    func updateData() {
        presenter?.getTableData().done { data in
            self.fullData = data?.copy() as? PLTableData
            self.filteredData = data
            self.reloadList(data: data!)
        }.cauterize()
    }
    
    func reloadList(data: PLTableData) {
        self.tableView.loadData(data: data)
    }
    
    
    func searchBarIsEmpty() -> Bool {
        return searchController?.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        if let data: [ToDo] = fullData?.data as? [ToDo] {
            
            let filtered = data.filter{( todo : ToDo) -> Bool in
                if searchBarIsEmpty() {
                    return scope == "All" ? true : todo.statusMatch() == scope
                }
                
                if (todo.name?.lowercased().contains(searchText.lowercased()))! {
                    return scope == "All" ? true : todo.statusMatch() == scope
                }
                
                return false
            }
            
            filteredData?.data = filtered
            reloadList(data: filteredData!)
        }
    }
}


extension ToDoListViewController: ToDoListViewControllerInterface {
    
    //MARK: - Initializer -
    func initialize() {
        view.backgroundColor = UIColor.plannerBackgroundColor()
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "addIcon"), style: .done, target: self, action: .createToDo)
        
        searchController = UISearchController(searchResultsController: nil)
        searchController?.searchResultsUpdater = self
        searchController?.obscuresBackgroundDuringPresentation = false
        searchController?.searchBar.scopeButtonTitles = ["All", "Open", "Overdue"]
    
        searchController?.searchBar.showsScopeBar = false
        searchController?.searchBar.delegate = self
        navigationItem.hidesSearchBarWhenScrolling = false
        
        // Add the search controller to the nav item
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        tableView.delegate = self
        self.updateData()
    }
}

extension ToDoListViewController: PLTableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath, object: Any?) {
        
        ToDoDetailsViewControllerRouter.createModule(todo: object as! ToDo).done { view in
       
            view.dismissed = { result in
                if (result as! Bool) {
                    self.updateData()
                }
            }
            
            self.navigationController?.pushViewController(view, animated: true)
        }.cauterize()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }
}

extension ToDoListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!, scope: searchController.searchBar.scopeButtonTitles![selectedScopeButton])
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        selectedScopeButton = 0;
    }
}

extension ToDoListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        selectedScopeButton = selectedScope
        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
}


fileprivate extension Selector {
    static let createToDo = #selector(ToDoListViewController.createToDo)
}
