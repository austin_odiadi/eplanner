//
//  ToDoDetailsViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 14/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// P -> V
protocol ToDoDetailsViewControllerInterface: class {
    func initialize()
}

// V -> P
protocol ToDoDetailsViewControllerPresenterInterface: class {
    
    func viewDidLoad()
    func deleteToDo(with id: Int64) -> Promise<Bool>
    func updateReminder(_ reminder: Bool, id: Int64) -> Promise<ToDo?>
    func updateCompleted(_ completed: Bool,  id: Int64) -> Promise<ToDo?>
    
    var router: ToDoDetailsViewControllerRouterInterface? {get set}
    var interactor: ToDoDetailsViewControllerInInteractorInterface? {get set};
}

// P -> R
protocol ToDoDetailsViewControllerRouterInterface: class {
}

// P -> I
protocol ToDoDetailsViewControllerInInteractorInterface: class {
    
    func deleteToDo(with id: Int64) -> Promise<Bool>
    func updateReminder(_ reminder: Bool, id: Int64) -> Promise<ToDo?>
    func updateCompleted(_ completed: Bool,  id: Int64) -> Promise<ToDo?>
    
    var dataManager: DataManager? {get set}
    var presenter: ToDoDetailsViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol ToDoDetailsViewControllerOutInteractorInterface: class {
}

