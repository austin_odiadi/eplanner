//
//  ToDoDetailsViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 14/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class ToDoDetailsViewControllerPresenter: ToDoDetailsViewControllerPresenterInterface {

    var router: ToDoDetailsViewControllerRouterInterface?
    private weak var view: ToDoDetailsViewControllerInterface?
    var interactor: ToDoDetailsViewControllerInInteractorInterface?
    
    init(interactor: ToDoDetailsViewControllerInInteractorInterface,
         viewController: ToDoDetailsViewControllerInterface,
         router: ToDoDetailsViewControllerRouterInterface) {
        
        self.router     = router
        self.interactor = interactor
        self.view       = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func updateReminder(_ reminder: Bool, id: Int64) -> Promise<ToDo?> {
        return (interactor?.updateReminder(reminder, id: id))!
    }
    
    func updateCompleted(_ completed: Bool, id: Int64) -> Promise<ToDo?> {
        return (interactor?.updateCompleted(completed, id: id))!
    }
    
    func deleteToDo(with id: Int64) -> Promise<Bool> {
        return (interactor?.deleteToDo(with: id))!
    }
}

extension ToDoDetailsViewControllerPresenter: ToDoDetailsViewControllerOutInteractorInterface {
    
}
