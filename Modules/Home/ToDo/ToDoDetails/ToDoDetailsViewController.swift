//
//  ToDoDetailsViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 14/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class ToDoDetailsViewController: BaseViewController {

    @IBOutlet weak var deleteButtonContainer: UIView!
    @IBOutlet weak var textViewContainer: UIView!
    @IBOutlet weak var reminderSwitch: UISwitch!
    @IBOutlet weak var completedSwitch: UISwitch!
    
    var todo: ToDo?
    var isUpdated: Bool = false
    var presenter: ToDoDetailsViewControllerPresenterInterface?
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var dueInLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        
    }
    
    override func willMove(toParent parent: UIViewController?) {
        if (parent == nil) {
            dismissed?(isUpdated)
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        textViewContainer.layoutIfNeeded()
        deleteButtonContainer.layoutIfNeeded()
        
        textViewContainer.roundedCorners(radius: 5)
        deleteButtonContainer.roundedCorners(radius: 5)
        textViewContainer.dropShadow(color: .lightGray, radius: 5, scale: true)
    }
    
    @objc func editToDo() {
        CreateToDoViewControllerRouter.createModule(todo: todo, loadType: .existing).done { view in
            guard let viewController = view.topViewController as? CreateToDoViewController else {
                return
            }
            
            viewController.dismissed = { result in
                guard let todo = result as? ToDo else  {
                    return
                }
                
                self.todo = todo
                self.isUpdated = true
                self.update()
            }
            
            self.present(view, animated: true, completion: nil)
        }.cauterize()
    }
    
    @IBAction func deletButtonPushed(_ sender: UIButton) {
        presenter?.deleteToDo(with: todo!.id!).done { success in
            self.isUpdated = success
            self.navigationController?.popViewController(animated: true)
        }.cauterize()
    }
    
    @IBAction func reminderChanged(_ sender: UISwitch) {
        presenter?.updateReminder(sender.isOn, id: (todo?.id)!).done { result in
            guard let todo = result else  {
                return
            }
            
            self.todo = todo
            self.isUpdated = true
            self.update()
            
        }.cauterize()
    }
    
    @IBAction func taskCompletedChanged(_ sender: UISwitch) {
        presenter?.updateCompleted(sender.isOn, id: (todo?.id)!).done { result in
            guard let todo = result else  {
                return
            }
            
            self.todo = todo
            self.isUpdated = true
            self.update()
            
        }.cauterize()
    }
    
    
    func update() {
        name.text = todo?.name
        textView.text = todo?.note
        dueInLabel.text = todo?.subtitle()
        reminderSwitch.isOn = todo?.reminder == 1 ? true : false
        completedSwitch.isOn = todo?.completed == 1 ? true : false
    }
}

extension ToDoDetailsViewController: ToDoDetailsViewControllerInterface {
    func initialize() {
        reminderSwitch.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        completedSwitch.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        
        update()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Edit", style: .done, target: self, action: .editToDo)
    }
}

fileprivate extension Selector {
    static let editToDo = #selector(ToDoDetailsViewController.editToDo)
}
