//
//  ToDoDetailsViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 14/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class ToDoDetailsViewControllerRouter: ToDoDetailsViewControllerRouterInterface {
    
    class func createModule(todo: ToDo) -> Promise<ToDoDetailsViewController> {
        
        return firstly { () -> Promise<ToDoDetailsViewController> in
            let view = BaseViewController.viewControllerWithIdentifier(.toDoDetails)
            
            guard let viewController = view.value as? ToDoDetailsViewController else { return .value(ToDoDetailsViewController())}
            
            let router: ToDoDetailsViewControllerRouterInterface           = ToDoDetailsViewControllerRouter()
            let interactor: ToDoDetailsViewControllerInInteractorInterface = ToDoDetailsViewControllerInteractor(dataManager: DataManager())
            let presenter: ToDoDetailsViewControllerPresenterInterface     = ToDoDetailsViewControllerPresenter(interactor: interactor, viewController: viewController as ToDoDetailsViewControllerInterface, router: router)
            
            viewController.todo = todo
            viewController.presenter = presenter
            interactor.presenter = presenter as? ToDoDetailsViewControllerOutInteractorInterface
            
            return .value(viewController)
        }
    }
}
