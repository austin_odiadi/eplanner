//
//  ToDoDetailsViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 14/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class ToDoDetailsViewControllerInteractor: ToDoDetailsViewControllerInInteractorInterface {
   
    var dataManager: DataManager?
    var presenter: ToDoDetailsViewControllerOutInteractorInterface?
    
    init(dataManager: DataManager) {
        self.dataManager = dataManager
    }
    
    func updateReminder(_ reminder: Bool, id: Int64) -> Promise<ToDo?> {
        return (dataManager?.updateReminder(reminder: reminder, id: id))!
    }
    
    func updateCompleted(_ completed: Bool, id: Int64) -> Promise<ToDo?> {
        return (dataManager?.updateCompleted(completed: completed, id: id))!
    }
    
    func deleteToDo(with id: Int64) -> Promise<Bool> {
        return (dataManager?.deleteToDo(withId: id))!
    }
}
