//
//  CalculationsViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class CalculationsViewControllerPresenter: CalculationsViewControllerPresenterInterface {
    
    var router: CalculationsViewControllerRouterInterface?
    private weak var view: CalculationsViewControllerInterface?
    var interactor: CalculationsViewControllerInInteractorInterface?
    
    init(interactor: CalculationsViewControllerInInteractorInterface,
         viewController: CalculationsViewControllerInterface,
         router: CalculationsViewControllerRouterInterface) {
        
        self.router     = router
        self.interactor = interactor
        self.view       = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func fetchCalculations() -> Promise<[Calculation]?> {
        return (interactor?.fetchCalculations())!
    }
}

extension CalculationsViewControllerPresenter: CalculationsViewControllerOutInteractorInterface {
    func availableCalculations(calculations: [Event]?) {
    }
}
