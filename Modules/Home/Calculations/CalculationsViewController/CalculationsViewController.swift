//
//  CalculationsViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class CalculationsViewController: BaseViewController {
    
    var calculations: [Calculation]?
    var presenter:CalculationsViewControllerPresenterInterface?
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        update()
    }
    
    @objc func createCalculation() {
        CreateCalculationViewControllerRouter.createModule(loadType: .new).done { view in
            guard let viewController = view.topViewController as? CreateCalculationViewController else {
                return
            }
            
            viewController.dismissed = { result in
                if (result as! Bool) {
                    self.update()
                }
            }
            self.present(view, animated: true, completion: nil)
        }.cauterize()
    }
    
    func update() {
        presenter?.fetchCalculations().done { calculations in
            self.calculations = calculations!
            self.tableView.reloadData()
        }.cauterize()
    }
}

extension CalculationsViewController: CalculationsViewControllerInterface {
    
    //MARK: - Initializer -
    
    func initialize() {
        view.backgroundColor = UIColor.plannerBackgroundColor()
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "addIcon"), style: .done, target: self, action: .createCalculation)
        
        tableView.register(UINib(nibName: "CalculationTableViewCell", bundle: nil), forCellReuseIdentifier: calculationCellIdentifier)
        update()
    }
}

extension CalculationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return calculations?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: calculationCellIdentifier, for: indexPath) as! CalculationTableViewCell
        let calculation = calculations?[indexPath.row]
        
        cell.title.text = calculation?.event?.title
        cell.subtitle.text = calculation?.event?.type
        cell.drinks.text = calculation?.numberOfDrinks()
        cell.drinkHabit.text = calculation?.drinkHabit()
        cell.duration.text = calculation?.durationValue()
        
        if let type = calculation?.event?.type, type != optionDisplayText {
            cell.icon.image = EventIcons.icon(for: eventTypes.index(of: type)!)
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.size.height * 0.17
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 18
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        CalculationDetailViewControllerRouter.createModule(calculation: (calculations?[indexPath.row])!).done { view in
            
            view.dismissed = { result in
                if (result as! Bool) {
                    self.update()
                }
            }
            
            self.navigationController?.pushViewController(view, animated: true)
        }.cauterize()
    }
}

fileprivate extension Selector {
    static let createCalculation = #selector(CalculationsViewController.createCalculation)
}
