//
//  CalculationTableViewCell.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 17/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class CalculationTableViewCell: UITableViewCell {

    @IBOutlet weak var drinksCircle: UIView!
    @IBOutlet weak var durationContainer: UIView!
    @IBOutlet weak var drinksBehaviourCircle: UIView!
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var drinks: UILabel!
    @IBOutlet weak var drinkHabit: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        container.layoutIfNeeded()
        container.roundedCorners(radius: 5)
        
        drinksCircle.layoutIfNeeded()
        drinksCircle.makeCircular()
        
        durationContainer.layoutIfNeeded()
        durationContainer.makeCircular()
        
        drinksBehaviourCircle.layoutIfNeeded()
        drinksBehaviourCircle.makeCircular()
        
    }
    
}
