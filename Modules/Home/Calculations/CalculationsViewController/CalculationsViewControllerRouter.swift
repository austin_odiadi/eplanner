//
//  CalculationsViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class CalculationsViewControllerRouter: CalculationsViewControllerRouterInterface, MainTabBarViewProtocol {

    var tabIcon: UIImage = UIImage(named:"drinksIcon")!
    var tabTitle: String = "Calculations"
    var navTitle: String = "Calculations"
    
    class func createModule() -> Promise<CalculationsViewController> {
        
        return firstly { () -> Promise<CalculationsViewController> in
            let view = BaseViewController.viewControllerWithIdentifier(.calculations)
            
            guard let viewController = view.value as? CalculationsViewController else { return .value(CalculationsViewController())}
            
            let router: CalculationsViewControllerRouterInterface           = CalculationsViewControllerRouter()
            let interactor: CalculationsViewControllerInInteractorInterface = CalculationsViewControllerInteractor(dataManager: DataManager())
            let presenter: CalculationsViewControllerPresenterInterface     = CalculationsViewControllerPresenter(interactor: interactor, viewController: viewController as CalculationsViewControllerInterface, router: router)
            
            viewController.presenter = presenter
            interactor.presenter = presenter as? CalculationsViewControllerOutInteractorInterface
            
            return .value(viewController)
        }
    }
    
    func configuredViewController() -> UIViewController {
        let view = BaseViewController.viewControllerWithIdentifier(.calculations)
        
        guard let viewController = view.value as? CalculationsViewController else { return CalculationsViewController()}
        
        let interactor: CalculationsViewControllerInInteractorInterface = CalculationsViewControllerInteractor(dataManager: DataManager())
        let presenter: CalculationsViewControllerPresenterInterface     = CalculationsViewControllerPresenter(interactor: interactor, viewController: viewController as CalculationsViewControllerInterface, router: self)
        
        viewController.presenter = presenter
        interactor.presenter = presenter as? CalculationsViewControllerOutInteractorInterface
        
        return viewController
    }
}
