//
//  CalculationsViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class CalculationsViewControllerInteractor: CalculationsViewControllerInInteractorInterface {
    
    var dataManager: DataManager?
    var presenter: CalculationsViewControllerOutInteractorInterface?
    
    init(dataManager: DataManager) {
        self.dataManager = dataManager
    }
    
    func fetchCalculations() -> Promise<[Calculation]?> {
        let predicate = NSPredicate(format: "%K == %d", #keyPath(Calculations.isSet), 1)
        return (dataManager?.fetchCalculations(predicate: predicate))!
    }
}

