//
//  CalculationsViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// P -> V
protocol CalculationsViewControllerInterface: class {
    func initialize()
}

// V -> P
protocol CalculationsViewControllerPresenterInterface: class {
    func viewDidLoad()
    func fetchCalculations() -> Promise<[Calculation]?>
    
    var router: CalculationsViewControllerRouterInterface? {get set}
    var interactor: CalculationsViewControllerInInteractorInterface? {get set};
}

// P -> R
protocol CalculationsViewControllerRouterInterface: class {
}

// P -> I
protocol CalculationsViewControllerInInteractorInterface: class {
    func fetchCalculations() -> Promise<[Calculation]?>
    
    var dataManager: DataManager? {get set}
    var presenter: CalculationsViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol CalculationsViewControllerOutInteractorInterface: class {
    func availableCalculations(calculations: [Event]?)
}
