//
//  CalculationDetailViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 18/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit


// P -> V
protocol CalculationDetailViewControllerInterface: class {
    func initialize()
}

// V -> P
protocol CalculationDetailViewControllerPresenterInterface: class {
    
    func viewDidLoad()
    func deleteCalculation(withId id: Int64) -> Promise<Bool>
    
    var router: CalculationDetailViewControllerRouterInterface? {get set}
    var interactor: CalculationDetailViewControllerInInteractorInterface? {get set};
}

// P -> R
protocol CalculationDetailViewControllerRouterInterface: class {
}

// P -> I
protocol CalculationDetailViewControllerInInteractorInterface: class {
    func deleteCalculation(withId id: Int64) -> Promise<Bool>
    
    var presenter: CalculationDetailViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol CalculationDetailViewControllerOutInteractorInterface: class {
}
