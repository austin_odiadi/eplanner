//
//  CalculationCarouselFlowLayout.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 18/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class CalculationCarouselFlowLayout: UICollectionViewFlowLayout {
    
    init(size: CGSize) {
        super.init()
        
        self.sectionInset = UIEdgeInsets(top:0, left:0, bottom:0, right:0)
        self.itemSize = size
        self.scrollDirection = .vertical
        self.minimumInteritemSpacing = 0.5
        self.minimumLineSpacing = 0.5
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        
    }
}
