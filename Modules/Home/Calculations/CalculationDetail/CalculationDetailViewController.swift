//
//  CalculationDetailViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 18/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class CalculationDetailViewController: BaseViewController {
    
    var isUpdated: Bool = false
    var calculation: Calculation?
    var presenter: CalculationDetailViewControllerPresenterInterface?
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var buttonContainer: UIView!
    @IBOutlet weak var topContainer: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        topContainer.layoutIfNeeded()
        topContainer.roundedCorners(radius: 5)
        
        buttonContainer.layoutIfNeeded()
        buttonContainer.roundedCorners(radius: 5)
    }
    
    @objc func editCalculation() {
        CreateCalculationViewControllerRouter.createModule(calculation: calculation, loadType: .existing).done { view in
            guard let viewController = view.topViewController as? CreateCalculationViewController else {
                return
            }
            
            viewController.dismissed = { result in
                guard let calculation = result as? Calculation else  {
                    return
                }
                
                self.calculation = calculation
                self.isUpdated = true
                self.collectionView.reloadData()
            }
            
            self.present(view, animated: true, completion: nil)
            }.cauterize()
    }
    
    func data(at indexPath: IndexPath) -> (String, UIImage?) {
        var count: String = "\(0)"
        let type = CalculationTypes(rawValue: indexPath.row)
        
        switch type {
        case .wine?: count = "\((calculation?.wine)!)"; break
        case .beer?: count = "\((calculation?.beer)!)";
        case .water?: count = "\((calculation?.water)!)"; break
        case .juice?: count = "\((calculation?.juice)!)"; break
        case .liquor?: count = "\((calculation?.liquor)!)"; break
        case .sparkling?: count = "\((calculation?.sparkling)!)"; break
        case .duration?: count = (calculation?.fullDurationValue())!; break
        case .behavoir?: count = (calculation?.drinkHabit())!; break
        default: break
        }
        
        return (count, UIImage(named: (type?.icon())!))
    }
    
    @IBAction func deleteButton(_ sender: UIButton) {
        presenter?.deleteCalculation(withId: (calculation?.event?.id)!).done { success in
            if success {
                self.dismissed?(success)
                self.navigationController?.popViewController(animated: true)
            }
        }.cauterize()
    }
}

extension CalculationDetailViewController: CalculationDetailViewControllerInterface {
    func initialize() {
        self.navigationItem.setHidesBackButton(false, animated: true)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Edit", style: .done, target: self, action: .editCalculation)
        
        eventTitle.text = calculation?.event?.title
        subtitle.text = calculation?.event?.type
        
        if let type = calculation?.event?.type, type != optionDisplayText {
            icon.image = EventIcons.icon(for: eventTypes.index(of: type)!)
        }
        collectionView.backgroundColor = UIColor.plannerBackgroundColor()
        collectionView.register(UINib(nibName: "CalculationCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: calculationDetailIdentifer)
        
        var topSafeAreaHeight: CGFloat = 0

        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.windows[0]
            let safeFrame = window.safeAreaLayoutGuide.layoutFrame
            topSafeAreaHeight = safeFrame.minY
        }
        collectionView.layoutIfNeeded()
        let size: CGSize = CGSize(width:(collectionView.frame.size.width / 3)-0.5, height:(collectionView.frame.size.height / 3) - topSafeAreaHeight + 1)
        collectionView!.collectionViewLayout = CalculationCarouselFlowLayout(size: size)
    }
}

extension CalculationDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: calculationDetailIdentifer, for: indexPath) as! CalculationCollectionViewCell
        
        if indexPath.row <= 7 {
            let (count, image) = data(at: indexPath)
            cell.icon.image = image
            cell.title.text = count
        }
        else {
            cell.icon.image = nil
            cell.title.text = nil
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

fileprivate extension Selector {
    static let editCalculation = #selector(CalculationDetailViewController.editCalculation)
}

