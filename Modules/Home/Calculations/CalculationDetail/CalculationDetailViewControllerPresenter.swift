//
//  CalculationDetailViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 18/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class CalculationDetailViewControllerPresenter: CalculationDetailViewControllerPresenterInterface {
   
    var router: CalculationDetailViewControllerRouterInterface?
    private weak var view: CalculationDetailViewControllerInterface?
    var interactor: CalculationDetailViewControllerInInteractorInterface?
    
    init(interactor: CalculationDetailViewControllerInInteractorInterface,
         viewController: CalculationDetailViewControllerInterface,
         router: CalculationDetailViewControllerRouterInterface) {
        
        self.router     = router
        self.interactor = interactor
        self.view       = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func deleteCalculation(withId id: Int64) -> Promise<Bool> {
        return (interactor?.deleteCalculation(withId: id))!
    }
}
