//
//  CalculationDetailViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 18/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit


class CalculationDetailViewControllerInteractor: CalculationDetailViewControllerInInteractorInterface {

    var dataManager: DataManager?
    var presenter: CalculationDetailViewControllerOutInteractorInterface?
    
    init(dataManager: DataManager) {
        self.dataManager = dataManager
    }
    
    func deleteCalculation(withId id: Int64) -> Promise<Bool> {
        return (dataManager?.deleteCalculation(withId: id))!
    }
}

