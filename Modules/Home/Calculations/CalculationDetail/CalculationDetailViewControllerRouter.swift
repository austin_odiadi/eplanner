//
//  CalculationDetailViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 18/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class CalculationDetailViewControllerRouter: CalculationDetailViewControllerRouterInterface {
    
    class func createModule(calculation: Calculation) -> Promise<CalculationDetailViewController> {
        
        return firstly { () -> Promise<CalculationDetailViewController> in
            let view = BaseViewController.viewControllerWithIdentifier(.calculationDetail)
            
            guard let viewController = view.value as? CalculationDetailViewController else { return .value(CalculationDetailViewController())}
            
            let router: CalculationDetailViewControllerRouterInterface           = CalculationDetailViewControllerRouter()
            let interactor: CalculationDetailViewControllerInInteractorInterface = CalculationDetailViewControllerInteractor(dataManager: DataManager())
            let presenter: CalculationDetailViewControllerPresenterInterface     = CalculationDetailViewControllerPresenter(interactor: interactor, viewController: viewController as CalculationDetailViewControllerInterface, router: router)
            
            viewController.calculation = calculation
            viewController.presenter = presenter
            interactor.presenter = presenter as? CalculationDetailViewControllerOutInteractorInterface
            
            return .value(viewController)
        }
    }
}
