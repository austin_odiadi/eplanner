//
//  CreateCalculationViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 16/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class CreateCalculationViewControllerInteractor: CreateCalculationViewControllerInInteractorInterface {
    
    var dataManager: DataManager?
    var presenter: CreateCalculationViewControllerOutInteractorInterface?
    
    init(dataManager: DataManager) {
        self.dataManager = dataManager
    }
    
    func fetchEvents() -> Promise<[Event]?> {
        let predicate = NSPredicate(format: "%K == %d", #keyPath(Events.calculations.isSet), 0)
        return (dataManager?.fetchEvents(predicate: predicate))!
    }
    
    func newCalculationData() -> [[AnyObject]] {
        
        var dataSource: [[AnyObject]] = CalculationData.newCalculation()
        dataSource.insert([EventData(type: .eventTitle)], at: 0)
        
        return dataSource
    }
    
    func headerDescriptions(loadType: CreateCalculationLoadType) -> [String] {
        return [loadType == .new ? "SELECT EVENT" : "EVENT", "DRINKS", "DRINK HABIT", "EVENT DURATION"]
    }
    
    func existingCalculationData(calculation: Calculation) -> [[AnyObject]] {
        
        var dataSource: [[AnyObject]] = CalculationData.data(with: calculation)
        dataSource.insert([EventData(event: calculation.event, type: .eventTitle)], at: 0)
        
        return dataSource
    }
    
    func saveCalculation(withId id: Int64, calculation: [AnyObject]) -> Promise<Bool> {
        return (dataManager?.saveCalculation(withId: id, calculation: calculation))!
    }
    
    func updateCalculation(withId id: Int64, calculation: [AnyObject]) -> Promise<Calculation?> {
        return (dataManager?.updateCalculation(withId: id, calculation: calculation))!
    }
}
