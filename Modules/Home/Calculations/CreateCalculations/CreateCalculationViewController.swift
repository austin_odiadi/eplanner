//
//  CreateCalculationViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 16/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class CreateCalculationViewController: BaseViewController {
    
    let headerHeight = CGFloat(38)
    var calculation: Calculation?
    var events: [Event] = [Event]()
    var isPickerVisible: Bool = false
    var loadType: CreateCalculationLoadType?
    var calculationHeaderData: [String] = [String]()
    var calculationData: [[AnyObject]] = [[AnyObject]]()
    var presenter: CreateCalculationViewControllerPresenterInterface?
    
    @IBOutlet weak var pickerView: PickerView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickerContainerTop: NSLayoutConstraint!
    
    let drinking: [String] = ["VERY FEW", "FEW", "NORMAL", "PLENTY", "BINGE"]
    let duration: [String] = ["2 HOURS", "2-4 HOURS", "4-6 HOURS", "6-8 HOURS", "OPEN END"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func cancelCalculation() {
        dismissed?(false)
        dismiss(animated: true, completion: nil)
    }
    
    @objc func saveCalculation() {
        
        switch loadType {
        case .new?:
            guard let event: EventData = calculationData.first?.first! as? EventData, !(event.value?.isEmpty)! else {
                return
            }
            
            let index: Int64 = (events.filter { $0.title == event.value}.first?.id)!
            presenter?.saveCalculation(withId: index, calculation: calculationData.reduce([], +))
            .done { success in
                
                self.dismissed?(success)
                self.dismiss(animated: true, completion: nil)
                
            }.cauterize()
            break
        case .existing?:
            presenter?.updateCalculation(withId: (calculation?.event?.id)!, calculation: calculationData.reduce([], +))
            .done { result in
                
                self.dismissed?(result as Any)
                self.dismiss(animated: true, completion: nil)
                
            }.cauterize()
            break
        default: break
        }
    }
    
    func hidePicker(end selection: Bool? = false, hidden: @escaping () -> Void) {
        if !isPickerVisible {
            return
        }
        
        pickerContainerTop = pickerContainerTop.setMultiplier(multiplier: 1)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.endEditing(true)
            self.pickerView.layoutIfNeeded()
        }) { (_) in
            self.isPickerVisible = false
            hidden()
        }
    }
}

extension CreateCalculationViewController: CreateCalculationViewControllerInterface {
    func initialize() {
        view.backgroundColor = UIColor.plannerBackgroundColor()
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Save", style: .done, target: self, action: .saveCalculation)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "Cancel", style: .done, target: self, action: .cancelCalculation)
        
        pickerView.delegate = self
        
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: headerHeight))
        tableView.contentInset = UIEdgeInsets(top: -headerHeight, left: 0, bottom: 0, right: 0)
        tableView.register(UINib(nibName: "TextfieldTableViewCell", bundle: nil), forCellReuseIdentifier: textFieldIdentifier)
        tableView.register(UINib(nibName: "DrinksTableViewCell", bundle: nil), forCellReuseIdentifier: drinksIdentifier)
        tableView.register(UINib(nibName: "SliderTableViewCell", bundle: nil), forCellReuseIdentifier: sliderIdentifier)
        
        presenter?.fetchEvents().done { events in
            self.events = events ?? []
        }.cauterize()
        
        calculationHeaderData = (presenter?.headerDescriptions(loadType: loadType!))!
        switch loadType {
        case .new?:
            calculationData = (presenter?.newCalculationData())!
            break
        case .existing?:
            if let existingCalcuation = calculation {
                calculationData = (presenter?.existingCalculationData(calculation: existingCalcuation))!
            }
            break
        default: break
        }
    }
}

extension CreateCalculationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return calculationData[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return calculationData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: textFieldIdentifier, for: indexPath) as! TextfieldTableViewCell
            
            let event = calculationData[indexPath.section][indexPath.row] as! EventData
            
            cell.textField.text = event.value
            cell.selectionStyle = .none
            cell.textField.isUserInteractionEnabled = false
            return cell
        }
        else if (indexPath.section == 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: drinksIdentifier, for: indexPath) as! DrinksTableViewCell
            cell.selectionStyle = .none
            
            let calculation = calculationData[indexPath.section][indexPath.row] as! CalculationData
            cell.title.text = calculation.title
            cell.icon.image = calculation.icon
            cell.textField.text = "\(calculation.value ?? 0)"
            
            cell.delegate = self
            return cell
            
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: sliderIdentifier, for: indexPath) as! SliderTableViewCell
            cell.selectionStyle = .none
            
            let calculation = calculationData[indexPath.section][indexPath.row] as! CalculationData
            cell.slider(data: indexPath.section == 2 ? drinking : duration, index: calculation.value!)
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section <= 1 ? 44 : 55
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return HeaderView(title: calculationHeaderData[section], showButton: false)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            self.view.endEditing(true)
           
            guard let events: [String] = events.map({ $0.title! }) as [String]?, !events.isEmpty, loadType != .existing else {
                return
            }
            
            pickerView.loadPicker(picker: Picker(data: [events]))
            self.pickerContainerTop = self.pickerContainerTop.setMultiplier(multiplier: 0.63)
            UIView.animate(withDuration: 0.25, animations: {
                self.view.layoutIfNeeded()
            }) { (_) in
                self.isPickerVisible = true
            }
        }
    }
}

extension CreateCalculationViewController: PickerViewDelegate {
    func picker(_ view: PickerView, _ selection: Any?, _ done: Bool, _ type: PickerType) {
        
        if let event: EventData = calculationData.first?.first! as? EventData {
            event.value = selection as? String
            tableView.reloadData()
        }
        
        hidePicker(end: true, hidden: { })
    }
    
    func picker(_ view: PickerView, _ selection: Any?, type: PickerType) {
        if let event: EventData = calculationData.first?.first! as? EventData {
            event.value = selection as? String
            tableView.reloadData()
        }
    }
}

extension CreateCalculationViewController: DrinksTableViewCellDelegate {
    func update(cell: DrinksTableViewCell, withIndex index: Int64) {
        
        if let indexPath = tableView.indexPath(for: cell),
            let calculation: CalculationData = calculationData[indexPath.section][indexPath.row] as? CalculationData {
            calculation.value = index
            tableView.reloadData()
        }
    }
}

extension CreateCalculationViewController: SliderTableViewCellDelegate {
    func update(cell: SliderTableViewCell, withIndex index: Int64, value: Any) {
        if let indexPath = tableView.indexPath(for: cell),
            let calculation: CalculationData = calculationData[indexPath.section][indexPath.row] as? CalculationData {
            calculation.value = index
            tableView.reloadData()
        }
    }
}

fileprivate extension Selector {
    static let cancelCalculation = #selector(CreateCalculationViewController.cancelCalculation)
    static let saveCalculation = #selector(CreateCalculationViewController.saveCalculation)
}
