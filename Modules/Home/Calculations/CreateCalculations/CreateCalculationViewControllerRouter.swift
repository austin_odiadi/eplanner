//
//  CreateCalculationViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 16/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class CreateCalculationViewControllerRouter: CreateCalculationViewControllerRouterInterface {
    
    class func createModule(calculation: Calculation? = nil, loadType: CreateCalculationLoadType? = .new) -> Promise<UINavigationController> {
        
        return firstly { () -> Promise<UINavigationController> in
            let view = BaseViewController.viewControllerWithIdentifier(.createCalculation)
            
            guard let viewController = view.value as? CreateCalculationViewController else { return .value(UINavigationController(rootViewController: CreateCalculationViewController()))}
            
            let router: CreateCalculationViewControllerRouterInterface           = CreateCalculationViewControllerRouter()
            let interactor: CreateCalculationViewControllerInInteractorInterface = CreateCalculationViewControllerInteractor(dataManager: DataManager())
            let presenter: CreateCalculationViewControllerPresenterInterface     = CreateCalculationViewControllerPresenter(interactor: interactor, viewController: viewController as CreateCalculationViewControllerInterface, router: router)
            
            
            viewController.loadType = loadType
            viewController.presenter = presenter
            viewController.calculation = calculation
            viewController.modalTransitionStyle = .coverVertical
            interactor.presenter = presenter as? CreateCalculationViewControllerOutInteractorInterface
            
            return .value(UINavigationController(rootViewController: viewController))
        }
    }
}
