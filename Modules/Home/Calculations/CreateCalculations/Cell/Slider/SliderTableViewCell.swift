//
//  SliderTableViewCell.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 16/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

protocol SliderTableViewCellDelegate {
    func update(cell: SliderTableViewCell, withIndex index: Int64, value: Any)
}

class SliderTableViewCell: UITableViewCell {
    
    var data: [String]?
    var delegate: SliderTableViewCellDelegate?

    @IBOutlet weak var slider: StepSlider!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        slider.adjustLabel         = true
        slider.labelOffset         = 2.0
        slider.trackHeight         = 2.86
        slider.trackColor          = UIColor(rgb: 0xEFEFF4)
        slider.tintColor           = .brown
        slider.trackCircleRadius   = 7
        slider.sliderCircleImage   = UIImage(named: "sliderIcon")
        slider.labelFont           = UIFont(name: "Lato", size: 8)
        slider.labelColor          = .darkGray
        
        slider.setIndex(0, animated: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func slider(data: [String], index: Int64) {
        self.data = data
        slider.labels   = data
        slider.maxCount = UInt(data.count)
        slider.index    = UInt(index)
    }
    
    @IBAction func pickerValueChanged(_ sender: StepSlider) {
        delegate?.update(cell: self, withIndex: Int64(sender.index), value: self.data?[Int(sender.index)] as Any)
    }
}
