//
//  DrinksTableViewCell.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 16/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

protocol DrinksTableViewCellDelegate {
    func update(cell: DrinksTableViewCell, withIndex index: Int64)
}

class DrinksTableViewCell: UITableViewCell {
    var delegate: DrinksTableViewCellDelegate?
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.delegate = self
        textField.addTarget(self, action: .textFieldDidChange, for: .editingChanged)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func addButton(_ sender: UIButton) {
        textField.text = "\(Int64(textField.text!)! + 1)"
        delegate?.update(cell: self, withIndex: Int64(textField.text!)!)
    }
    
    @IBAction func removeButton(_ sender: UIButton) {
        let index = Int64(textField.text!)! - 1
        textField.text = "\(index < 0 ? 0 : index)"
        delegate?.update(cell: self, withIndex: Int64(textField.text!)!)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if !(textField.text?.isNumeric)! {
            textField.text = "\(textField.text?.dropLast() ?? "")"
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension DrinksTableViewCell: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        
        delegate?.update(cell: self, withIndex: Int64(textField.text!)!)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}

fileprivate extension Selector {
    static let textFieldDidChange = #selector(DrinksTableViewCell.textFieldDidChange)
}
