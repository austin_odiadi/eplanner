//
//  CreateCalculationViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 16/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class CreateCalculationViewControllerPresenter: CreateCalculationViewControllerPresenterInterface {
    
    var router: CreateCalculationViewControllerRouterInterface?
    private weak var view: CreateCalculationViewControllerInterface?
    var interactor: CreateCalculationViewControllerInInteractorInterface?
    
    init(interactor: CreateCalculationViewControllerInInteractorInterface,
         viewController: CreateCalculationViewControllerInterface,
         router: CreateCalculationViewControllerRouterInterface) {
        
        self.router     = router
        self.interactor = interactor
        self.view       = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func fetchEvents() -> Promise<[Event]?> {
        return (interactor?.fetchEvents())!
    }
    
    func headerDescriptions(loadType: CreateCalculationLoadType) -> [String] {
        return (interactor?.headerDescriptions(loadType: loadType))!
    }
    
    func newCalculationData() -> [[AnyObject]] {
        return (interactor?.newCalculationData())!
    }
    
    func existingCalculationData(calculation: Calculation) -> [[AnyObject]] {
        return (interactor?.existingCalculationData(calculation: calculation))!
    }
    
    func saveCalculation(withId id: Int64, calculation: [AnyObject]) -> Promise<Bool> {
        return ((interactor?.saveCalculation(withId: id, calculation: calculation))!)
    }
    
    func updateCalculation(withId id: Int64, calculation: [AnyObject]) -> Promise<Calculation?> {
        return (interactor?.updateCalculation(withId: id, calculation: calculation))!
    }
}
