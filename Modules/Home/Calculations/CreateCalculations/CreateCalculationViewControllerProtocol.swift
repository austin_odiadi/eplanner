//
//  CreateCalculationViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 16/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// P -> V
protocol CreateCalculationViewControllerInterface: class {
    func initialize()
}

// V -> P
protocol CreateCalculationViewControllerPresenterInterface: class {
    
    func viewDidLoad()
    func newCalculationData() -> [[AnyObject]]
    func existingCalculationData(calculation: Calculation) -> [[AnyObject]]
    func headerDescriptions(loadType: CreateCalculationLoadType) -> [String]
    
    func fetchEvents() -> Promise<[Event]?>
    func saveCalculation(withId id: Int64, calculation: [AnyObject]) -> Promise<Bool>
    func updateCalculation(withId id: Int64, calculation: [AnyObject]) -> Promise<Calculation?>
    
    var router: CreateCalculationViewControllerRouterInterface? {get set}
    var interactor: CreateCalculationViewControllerInInteractorInterface? {get set};
}

// P -> R
protocol CreateCalculationViewControllerRouterInterface: class {
}

// P -> I
protocol CreateCalculationViewControllerInInteractorInterface: class {
    
    func newCalculationData() -> [[AnyObject]]
    func existingCalculationData(calculation: Calculation) -> [[AnyObject]]
    func headerDescriptions(loadType: CreateCalculationLoadType) -> [String]
    
    func fetchEvents() -> Promise<[Event]?>
    func saveCalculation(withId id: Int64, calculation: [AnyObject]) -> Promise<Bool>
    func updateCalculation(withId id: Int64, calculation: [AnyObject]) -> Promise<Calculation?>
    
    var dataManager: DataManager? {get set}
    var presenter: CreateCalculationViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol CreateCalculationViewControllerOutInteractorInterface: class {
}

