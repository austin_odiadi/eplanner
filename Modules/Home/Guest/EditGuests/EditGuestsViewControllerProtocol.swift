//
//  EditGuestsViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 10/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// P -> V
protocol EditGuestsViewControllerInterface: class {
    func initialize()
    func guestUpdated(with success: Bool);
}

// V -> P
protocol EditGuestsViewControllerPresenterInterface: class {
    
    func viewDidLoad()
    func updateGuest(guest: [GuestData], contact: [ContactData], withId id: Int64)
    
    var router: EditGuestsViewControllerRouterInterface? {get set}
    var interactor: EditGuestsViewControllerInInteractorInterface? {get set};
}

// P -> R
protocol EditGuestsViewControllerRouterInterface: class {
}

// P -> I
protocol EditGuestsViewControllerInInteractorInterface: class {
    func updateGuest(guest: [GuestData], contact: [ContactData], withId id: Int64)
    
    var dataManager: DataManager? {get set}
    var presenter: EditGuestsViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol EditGuestsViewControllerOutInteractorInterface: class {
    func guestUpdated(with success: Bool);
}

