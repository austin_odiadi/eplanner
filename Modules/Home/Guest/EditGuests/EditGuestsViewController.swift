//
//  EditGuestsViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 09/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class EditGuestsViewController: BaseViewController {

    var event: Event?
    var isUpdated: Bool = false
    internal var guests: [GuestData]?
    internal var contacts: [ContactData]?
    var presenter: EditGuestsViewControllerPresenterInterface?
    
   
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func willMove(toParent parent: UIViewController?) {
        if (parent == nil) {
            dismissed?(isUpdated)
        }
    }
    
    @objc func saveGuests() {
        view.endEditing(true)
        indicatorLoading(state: true)
        presenter?.updateGuest(guest: guests!, contact: contacts!, withId: event!.id!)
    }
    
    func addContacts(contacts: [Int : String]) {
        if contacts.isEmpty {
            return
        }
        
        contacts.values.forEach {
            self.contacts?.append(ContactData(name: $0))
        }
    
        tableView.reloadData()
    }
}

extension EditGuestsViewController: EditGuestsViewControllerInterface {
    
    func initialize() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Save", style: .done, target: self, action: .saveGuests)

        tableView.register(UINib(nibName: "ContactsTableViewCell", bundle: nil), forCellReuseIdentifier: contactsIdentifier)
        tableView.register(UINib(nibName: "EditGuestTableViewCell", bundle: nil), forCellReuseIdentifier: editGuestIdentifier)
        guests = GuestData.data(with: event!)
        contacts = ContactData.data(with: event!)
        
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.plannerBackgroundColor()
        tableView.reloadData()
    }
    
    func guestUpdated(with success: Bool) {
        isUpdated = success
        
        indicatorLoading(state: false)
        self.navigationController?.popViewController(animated: true)
    }
}

extension EditGuestsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 0 ? guests?.count : contacts?.count)!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: editGuestIdentifier, for: indexPath) as! EditGuestTableViewCell
            cell.title.text = guests?[indexPath.row].title
            cell.icon.image = guests?[indexPath.row].icon()
            cell.textField.text = "\(guests?[indexPath.row].value ?? 0)"
            cell.selectionStyle = .none
            cell.delegate = self
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: contactsIdentifier, for: indexPath) as! ContactsTableViewCell
            cell.checkbox.isHidden = true
            cell.title.text = contacts?[indexPath.row].name
            cell.selectionStyle = .none
            
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return HeaderView(title: section == 0 ? "" : "CONTACTS", showButton: section == 0 ? false : true, delegate: self)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        
        return view
    }
}

extension EditGuestsViewController: HeaderViewDelegate {
    func headerView(_ headerView: HeaderView, didPushButton button: UIButton) {
        
        ContactsViewControllerRouter.createModule().done { view in
            
            guard let viewController = view.topViewController as? ContactsViewController else {
                return
            }
            
            viewController.dismissed = { result in
                self.addContacts(contacts: result as! [Int : String])
            }
            self.present(view, animated: true, completion: nil)
            
            
        }.cauterize()
    }
}

extension EditGuestsViewController: EditGuestTableViewCellDelegate {
    func update(cell: EditGuestTableViewCell, withIndex index: Int64) {
        let indexPath =  tableView.indexPath(for: cell)
        
        guests?[(indexPath?.row)!].value = index
    }
}

fileprivate extension Selector {
    static let saveGuests = #selector(EditGuestsViewController.saveGuests)
}


