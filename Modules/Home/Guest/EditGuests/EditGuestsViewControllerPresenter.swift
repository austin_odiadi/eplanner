//
//  EditGuestsViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 10/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation

class EditGuestsViewControllerPresenter: EditGuestsViewControllerPresenterInterface {
    
    var router: EditGuestsViewControllerRouterInterface?
    private weak var view: EditGuestsViewControllerInterface?
    var interactor: EditGuestsViewControllerInInteractorInterface?
    
    init(interactor: EditGuestsViewControllerInInteractorInterface,
         viewController: EditGuestsViewControllerInterface,
         router: EditGuestsViewControllerRouterInterface) {
        
        self.router     = router
        self.interactor = interactor
        self.view       = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func updateGuest(guest: [GuestData], contact: [ContactData], withId id: Int64) {
        interactor?.updateGuest(guest: guest, contact: contact, withId: id)
    }
}


extension EditGuestsViewControllerPresenter: EditGuestsViewControllerOutInteractorInterface {
    func guestUpdated(with success: Bool) {
        view?.guestUpdated(with: success)
    }
    
    
}
