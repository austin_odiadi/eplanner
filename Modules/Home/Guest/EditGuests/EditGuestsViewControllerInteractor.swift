//
//  EditGuestsViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 10/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation

class EditGuestsViewControllerInteractor: EditGuestsViewControllerInInteractorInterface {
    
    var dataManager: DataManager?
    var presenter: EditGuestsViewControllerOutInteractorInterface?
    
    init(dataManager: DataManager) {
        self.dataManager = dataManager
    }
    
    func tableData(event: Event) -> PLTableData {
        let data = [GuestData.data(with: event), ContactData.data(with: event)] as [Any]
        return
            PLTableData(data: data, configuredCell: { (view, tableView, indexPath, data) -> (UITableViewCell)? in
                let cell = tableView.dequeueReusableCell(withIdentifier: editGuestIdentifier, for: indexPath) as! EditGuestTableViewCell
                
                if indexPath.section == 0 {
                    let guest = (data as! [Any])[indexPath.section] as! [GuestData]
                    cell.title.text = guest[indexPath.row].title
                    cell.textField.text = "\(guest[indexPath.row].value ?? 0)"
                    cell.icon.image = guest[indexPath.row].icon()
                }
                else {
                    print("")
                }
                
                cell.selectionStyle = .none
                return cell
            }, registeredCell: { tableView in
                tableView.register(UINib(nibName: "EditGuestTableViewCell", bundle: nil), forCellReuseIdentifier: editGuestIdentifier)
                return ()
            }, sizes: PLTableDataSize(44, 38, 20), updater: { (dataSource, update, indexPath) in
                return ()
            }, indexForRow: { (dataSource: [Any], section) -> (Int)? in
                return (dataSource[section] as! [Any]).count
            }, indexForSection: { (dataSource: [Any]) -> (Int)? in
                return dataSource.count
            }) { (view, dataSource, section) -> (UIView)? in
                return  section == 0 ? UIView() : HeaderView(title: "CONTACTS", delegate: view.delegate as? HeaderViewDelegate)
        }
    }
    
    func updateGuest(guest: [GuestData], contact: [ContactData], withId id: Int64) {
        dataManager?.updateGuest(guest: guest, contact: contact, withId: id).done { success in
            self.presenter?.guestUpdated(with: success)
        }.cauterize()
    }
}
