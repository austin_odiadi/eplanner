//
//  EditGuestsViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 10/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit

class EditGuestsViewControllerRouter: EditGuestsViewControllerRouterInterface {
    
    class func createModule(event: Event) -> Promise<EditGuestsViewController> {
        
        return firstly { () -> Promise<EditGuestsViewController> in
            let view = BaseViewController.viewControllerWithIdentifier(.editGuests)
            
            guard let viewController = view.value as? EditGuestsViewController else { return .value(EditGuestsViewController())}
            
            let router: EditGuestsViewControllerRouterInterface           = EditGuestsViewControllerRouter()
            let interactor: EditGuestsViewControllerInInteractorInterface = EditGuestsViewControllerInteractor(dataManager: DataManager())
            let presenter: EditGuestsViewControllerPresenterInterface     = EditGuestsViewControllerPresenter(interactor: interactor, viewController: viewController as EditGuestsViewControllerInterface, router: router)
            
            viewController.event = event
            viewController.presenter = presenter
            interactor.presenter = presenter as? EditGuestsViewControllerOutInteractorInterface
            
            return .value(viewController)
        }
    }
}
