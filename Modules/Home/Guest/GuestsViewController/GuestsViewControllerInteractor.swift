//
//  GuestsViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class GuestsViewControllerInteractor: GuestsViewControllerInInteractorInterface {
    
    var dataManager: DataManager?
    var presenter: GuestsViewControllerOutInteractorInterface?
    static let guestsIdentifier = "guestsIdentifier"
    
    init(dataManager: DataManager) {
        self.dataManager = dataManager
    }
    
    func tableData() -> Promise<PLTableData?> {

        return Promise<PLTableData?> { seal in
            dataManager?.fetchEvents().done { events in
                 seal.resolve(.fulfilled(
                    PLTableData(data: events, configuredCell: { (view, tableView, indexPath, data) -> (UITableViewCell)? in
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: GuestsViewControllerInteractor.guestsIdentifier, for: indexPath) as! GuestsTableViewCell
                        
                        cell.selectionStyle = .none
                        cell.separatorInset = UIEdgeInsets(top: 0, left: cell.bounds.size.width, bottom: 0, right: 0);
                        
                        let event = (data as! [Event])[indexPath.row]
                        cell.title.text = event.title ?? ""
                        cell.menCount.text = "\(event.guests?.men ?? 0)"
                        cell.womenCount.text = "\(event.guests?.women ?? 0)"
                        cell.childrenCount.text = "\(event.guests?.children ?? 0)"
                        cell.contactsCount.text = "\(event.guests?.contacts?.count ?? 0)"
                        
                        if let type = event.type, type != optionDisplayText {
                            cell.eventIcon.image = EventIcons.icon(for: eventTypes.index(of: type)!)
                        }
                        
                        return cell
                        
                    }, registeredCell: { tableView in
                        
                        tableView.register(UINib(nibName: "GuestsTableViewCell", bundle: nil), forCellReuseIdentifier: GuestsViewControllerInteractor.guestsIdentifier)
                        return ()
                    }, sizes: PLTableDataSize(UIScreen.main.bounds.size.height * 0.2, 18)) { (view, dataSource, section) -> (UIView)? in
                        return HeaderView(title: "", showButton: false)
                    }
                ))
            }.cauterize()
        }
    }

}
