//
//  GuestsViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// P -> V
protocol GuestsViewControllerInterface: class {
    func initialize()
}

// V -> P
protocol GuestsViewControllerPresenterInterface: class {
    func viewDidLoad()
    func getTableData() -> Promise<PLTableData?>
    
    var router: GuestsViewControllerRouterInterface? {get set}
    var interactor: GuestsViewControllerInInteractorInterface? {get set};
}

// P -> R
protocol GuestsViewControllerRouterInterface: class {
}

// P -> I
protocol GuestsViewControllerInInteractorInterface: class {
     func tableData() -> Promise<PLTableData?>
    
    var dataManager: DataManager? {get set}
    var presenter: GuestsViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol GuestsViewControllerOutInteractorInterface: class {
    func availableGuests(guests: [Event]?)
}
