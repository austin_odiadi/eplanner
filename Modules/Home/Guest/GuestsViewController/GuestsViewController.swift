//
//  GuestsViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class GuestsViewController: BaseViewController {
    var presenter: GuestsViewControllerPresenterInterface?
    var events: [Event]?
    
    @IBOutlet weak var tableView: PLTableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateTable()
        
    }
    
    func updateTable() {
        indicatorLoading(state: true)
        presenter?.getTableData().done { events in
            self.tableView.loadData(data: events)
            self.indicatorLoading(state: false)
        }.cauterize()
    }
}

extension GuestsViewController: GuestsViewControllerInterface {
    
    //MARK: - Initializer -
    
    func initialize() {
        self.navigationItem.setHidesBackButton(true, animated:true);
        view.backgroundColor = UIColor.plannerBackgroundColor()
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.showsScopeBar = true
        navigationItem.hidesSearchBarWhenScrolling = true
        
        // Add the search controller to the nav item
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        tableView.delegate = self
    }
}

extension GuestsViewController: PLTableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath, object: Any?) {
        EditGuestsViewControllerRouter.createModule(event: object as! Event).done { view in
            
            view.dismissed = { success in
                if (success as! Bool) {
                    self.updateTable()
                }
            }
            self.navigationController?.pushViewController(view, animated: true)
        }.cauterize()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }
}
