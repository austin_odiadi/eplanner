//
//  GuestsTableViewCell.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 09/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class GuestsTableViewCell: UITableViewCell {

    @IBOutlet weak var eventView: UIView!
    @IBOutlet weak var eventIcon: UIImageView!
    @IBOutlet weak var designContainer: UIView!
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var menCount: UILabel!
    @IBOutlet weak var womenCount: UILabel!
    @IBOutlet weak var childrenCount: UILabel!
    @IBOutlet weak var contactsCount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        eventView.layoutIfNeeded()
        designContainer.layoutIfNeeded()
        
        eventView.roundedCorners(radius: 5)
        designContainer.roundedCorners(radius: 5)
        //designContainer.dropShadow(color: .lightGray, radius: 5, scale: true)
    }

    
}
