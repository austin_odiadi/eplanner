//
//  GuestsViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class GuestsViewControllerRouter: GuestsViewControllerRouterInterface, MainTabBarViewProtocol {
    
    var tabIcon: UIImage = UIImage(named:"guestsIcon")!
    var tabTitle: String = "Guests"
    var navTitle: String = "Guests"
    
    class func createModule() -> Promise<GuestsViewController> {
        
        return firstly { () -> Promise<GuestsViewController> in
            let view = BaseViewController.viewControllerWithIdentifier(.guests)
            
            guard let viewController = view.value as? GuestsViewController else { return .value(GuestsViewController())}
            
            let router: GuestsViewControllerRouterInterface           = GuestsViewControllerRouter()
            let interactor: GuestsViewControllerInInteractorInterface = GuestsViewControllerInteractor(dataManager: DataManager())
            let presenter: GuestsViewControllerPresenterInterface     = GuestsViewControllerPresenter(interactor: interactor, viewController: viewController as GuestsViewControllerInterface, router: router)
            
            viewController.presenter = presenter
            interactor.presenter = presenter as? GuestsViewControllerOutInteractorInterface
            
            return .value(viewController)
        }
    }
    
    func configuredViewController() -> UIViewController {
        let view = BaseViewController.viewControllerWithIdentifier(.guests)
        
        guard let viewController = view.value as? GuestsViewController else { return GuestsViewController()}
        
        let interactor: GuestsViewControllerInInteractorInterface = GuestsViewControllerInteractor(dataManager: DataManager())
        let presenter: GuestsViewControllerPresenterInterface     = GuestsViewControllerPresenter(interactor: interactor, viewController: viewController as GuestsViewControllerInterface, router: self)
        
        viewController.presenter = presenter
        interactor.presenter = presenter as? GuestsViewControllerOutInteractorInterface
        
        return viewController
    }
}
