//
//  GuestsViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 03/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation

import PromiseKit

class GuestsViewControllerPresenter: GuestsViewControllerPresenterInterface {
    
    var router: GuestsViewControllerRouterInterface?
    private weak var view: GuestsViewControllerInterface?
    var interactor: GuestsViewControllerInInteractorInterface?
    
    init(interactor: GuestsViewControllerInInteractorInterface,
         viewController: GuestsViewControllerInterface,
         router: GuestsViewControllerRouterInterface) {
        
        self.router     = router
        self.interactor = interactor
        self.view       = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func getTableData() -> Promise<PLTableData?> {
        return (interactor?.tableData())!
    }
}

extension GuestsViewControllerPresenter: GuestsViewControllerOutInteractorInterface {
    func availableGuests(guests: [Event]?) {
    }
}
