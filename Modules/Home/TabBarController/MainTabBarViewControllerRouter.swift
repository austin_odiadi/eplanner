//
//  MainTabBarControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 01/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class MainTabBarViewControllerRouter: MainTabBarViewControllerRouterInterface {
    
    class func createModule() -> Promise<MainTabBarViewController> {
        
        return firstly { () -> Promise<MainTabBarViewController> in
            let view = BaseViewController.viewControllerWithIdentifier(.mainTab)
            
            guard let viewController = view.value as? MainTabBarViewController else { return .value(MainTabBarViewController())}
            
            var viewControllers = [UIViewController]()
            subRoutes().forEach {
                
                let tabBarItem      = UITabBarItem()
                tabBarItem.image    = $0.tabIcon
                tabBarItem.title    = $0.tabTitle
                
                let nav             = UINavigationController(rootViewController: $0.configuredViewController())
                nav.tabBarItem      = tabBarItem
                nav.title           = $0.tabTitle

                let navBar                  = nav.navigationBar
                navBar.topItem?.title       = $0.navTitle
                navBar.barStyle             = UIBarStyle.default
                navBar.titleTextAttributes  = [NSAttributedString.Key.foregroundColor: UIColor.black]
                
                nav.setNavigationBarHidden(false, animated: false)
                viewControllers.append(nav)
            }
            
            viewController.viewControllers = viewControllers
            viewController.selectedIndex = 2
            return .value(viewController)
        }
    }
    
    class func subRoutes() -> [MainTabBarViewProtocol] {
        var subRoutes = [MainTabBarViewProtocol]()
        
        let todo: ToDoListViewControllerRouterInterface = ToDoListViewControllerRouter()
        subRoutes.append(todo as! MainTabBarViewProtocol)
        
        let guests: GuestsViewControllerRouterInterface = GuestsViewControllerRouter()
        subRoutes.append(guests as! MainTabBarViewProtocol)
        
        let events: EventsViewControllerRouterInterface = EventsViewControllerRouter()
        subRoutes.append(events as! MainTabBarViewProtocol)
        
        let calculations: CalculationsViewControllerRouterInterface = CalculationsViewControllerRouter()
        subRoutes.append(calculations as! MainTabBarViewProtocol)

        let expenses: ExpensesViewControllerRouterInterface = ExpensesViewControllerRouter()
        subRoutes.append(expenses as! MainTabBarViewProtocol)

        return subRoutes
    }
}
