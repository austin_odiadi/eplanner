//
//  MainTabBarController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 01/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import PromiseKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        PlannerCommons.setInitialLoadComplete(loaded: true)
        .done { _ in
            // Saved
        }.cauterize()
    }
}

extension MainTabBarViewController: UITabBarControllerDelegate {
    
    // UITabBarDelegate
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
    }
    
    // UITabBarControllerDelegate
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    }
}
