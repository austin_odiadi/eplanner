//
//  MainTabBarControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 01/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit

protocol MainTabBarViewProtocol {
    var tabIcon:UIImage { get }
    var tabTitle:String { get }
    var navTitle:String { get }
    //var barButtonItem: UIBarButtonItem { get }
    
    func configuredViewController() -> UIViewController
}

protocol MainTabBarViewControllerRouterInterface: class {
}
