//
//  SplashViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 30/03/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// P -> V
protocol SplashViewControllerInterface: class {
    func initialize(intialLoad loaded: Bool)
}

// V -> P
protocol SplashViewControllerPresenterInterface: class {
    func viewDidLoad()
    
    var router: SplashViewControllerRouterInterface? {get set}
    var interactor: SplashViewControllerInInteractorInterface? {get set};
    
    func isIntialLoadCompleted()
}

// P -> R
protocol SplashViewControllerRouterInterface: class {
}

// P -> I
protocol SplashViewControllerInInteractorInterface: class {
    var presenter: SplashViewControllerOutInteractorInterface? {get set} ;
    func deviceInitialLoadComplete()
}

// I -> P
protocol SplashViewControllerOutInteractorInterface: class {
    func deviceInitialLoadComplete(loaded: Bool)
}

// MARK: - Delegates -

protocol SplashViewControllerInteractorDelegate: class {
}

protocol SplashViewControllerDelegate: class {
}

