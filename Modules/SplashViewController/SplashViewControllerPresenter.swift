//
//  SplashViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 30/03/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import PromiseKit

class SplashViewControllerPresenter: SplashViewControllerPresenterInterface {
    
    var router: SplashViewControllerRouterInterface?
    private weak var view: SplashViewControllerInterface?
    private weak var delegate: SplashViewControllerDelegate?
    var interactor: SplashViewControllerInInteractorInterface?
    
    init(interactor: SplashViewControllerInInteractorInterface,
         viewController: SplashViewControllerInterface,
         router: SplashViewControllerRouterInterface,
         delegate: SplashViewControllerDelegate? = nil) {
        
        self.router         = router
        self.interactor     = interactor
        self.view           = viewController
    }
    
    func viewDidLoad() {
        isIntialLoadCompleted()
    }
    
    func isIntialLoadCompleted() {
        self.interactor?.deviceInitialLoadComplete()
    }
}

extension SplashViewControllerPresenter: SplashViewControllerOutInteractorInterface {
    func deviceInitialLoadComplete(loaded: Bool) {
        view?.initialize(intialLoad: loaded)
    }
}

