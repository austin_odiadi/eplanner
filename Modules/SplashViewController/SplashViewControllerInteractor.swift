//
//  SplashViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 30/03/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import PromiseKit

class SplashViewControllerInteractor: SplashViewControllerInInteractorInterface {
    var presenter: SplashViewControllerOutInteractorInterface?
    
    func deviceInitialLoadComplete() {
        _ = PlannerCommons.initialLoadComplete().done { (loaded)  in
            self.presenter?.deviceInitialLoadComplete(loaded: loaded)
        }
    }
}
