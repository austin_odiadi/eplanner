//
//  SplashViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 30/03/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import PromiseKit

class SplashViewController: BaseViewController {
    var presenter: SplashViewControllerPresenterInterface?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar()
    }
    
    // MARK: - Helpers -
    
    func setupView() {
        
        self.titleContainer.roundedCorners(radius: 5)
        self.titleLabel.attributedText = attributedTitle();
        self.titleContainer.dropShadow(color: .lightGray, radius: 5, scale: true)
    }
    
    func attributedTitle () -> NSMutableAttributedString {
        
        let attrTitle = NSMutableAttributedString(string: titleLabel.text!)
        let bigBoldFont = UIFont(name: "Lato-Bold", size: titleLabel.font.pointSize)
        attrTitle.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: bigBoldFont!, range: NSMakeRange(5, 2))
        
        return attrTitle
    }
}

extension SplashViewController: SplashViewControllerInterface {
    func initialize(intialLoad loaded: Bool) {
    
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            if loaded {
                MainTabBarViewControllerRouter.createModule().done{ (view) in
                    self.navigationController?.pushViewController(view, animated: true)
                }.cauterize()
            }
            else {
                SurfViewControllerRouter.createModule().done{ (view) in
                    self.navigationController?.pushViewController(view, animated: true)
                }.cauterize()
            }
        })
    }
    
    func debugInsert() {
        let events: [Json] = [["id": 100], ["id": 200], ["id": 300], ["id": 400]]
        
        DataStackCommons.shared.insert(json: events).done{ (arg: (Events?, Bool)) in
            let (_, _) = arg
            }.cauterize()
    }
}
