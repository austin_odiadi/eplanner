//
//  SplashViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 30/03/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import PromiseKit

class SplashViewControllerRouter: SplashViewControllerRouterInterface {
    
    class func createModule() -> Promise<SplashViewController> {
        
        return firstly { () -> Promise<SplashViewController> in
            let view = BaseViewController.viewControllerWithIdentifier(.splash)
            
            guard let viewController = view.value as? SplashViewController else { return .value(SplashViewController())}
            
            let router: SplashViewControllerRouterInterface           = SplashViewControllerRouter()
            let interactor: SplashViewControllerInInteractorInterface = SplashViewControllerInteractor()
            let presenter: SplashViewControllerPresenterInterface     = SplashViewControllerPresenter(interactor: interactor, viewController: viewController as SplashViewControllerInterface, router: router)
            
            viewController.presenter = presenter
            interactor.presenter = presenter as? SplashViewControllerOutInteractorInterface
            
            return .value(viewController)
        }
    }
}
