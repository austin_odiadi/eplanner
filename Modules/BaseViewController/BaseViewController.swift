//
//  BaseViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 30/03/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import PromiseKit
import JTMaterialSpinner

protocol BaseViewControllerProtocol:  class {

    func showNavigationBar() -> Void
    func hideNavigationBar() -> Void
    func indicatorLoading(state: Bool) -> Void
}

protocol BaseViewControllerRouterInterface:  class {
    static func storyboard() -> UIStoryboard
    static func viewControllerWithIdentifier(_ identifier: PlannerViewController) -> Promise<UIViewController>
}

class BaseViewController: UIViewController {
    var spinnerView: JTMaterialSpinner!
    var dismissed: ((Any) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        spinnerView = JTMaterialSpinner(frame: CGRect(x: view.center.x, y: view.center.y, width: 30, height: 30))
        spinnerView.animationDuration = 2.5
        spinnerView.circleLayer.lineWidth = 2.0
        spinnerView.circleLayer.strokeColor = UIColor.lightGray.cgColor
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIView.animate(withDuration: 0.25) {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
}

extension BaseViewController: BaseViewControllerProtocol {
   
    func indicatorLoading(state: Bool) {
        if state {
            if !spinnerView.isDescendant(of: view) {
                view.addSubview(spinnerView)
            }
            
            spinnerView.beginRefreshing()
        }
        else {
            spinnerView.endRefreshing()
            spinnerView.removeFromSuperview()
        }
    }
    
    func hideNavigationBar(){
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func showNavigationBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

extension BaseViewController: BaseViewControllerRouterInterface {
    static func storyboard() -> UIStoryboard {
        return UIStoryboard(name:"Main", bundle:nil)
    }
    
    class func viewControllerWithIdentifier(_ identifier: PlannerViewController) -> Promise<UIViewController> {
        return Promise { seal in
            
            let view = storyboard().instantiateViewController(withIdentifier: "\(identifier.description())ViewController");
            seal.resolve(.fulfilled(view))
        }
    }
}

