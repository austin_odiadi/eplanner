//
//  SurfViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 31/03/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

// P -> V
protocol SurfViewControllerInterface: class {
    func initialize()
    func didUpdatePage(count: Int)
    func didUpdatePage(index: Int)
    func goto(viewController view: MainTabBarViewController?)
}

// V -> P
protocol SurfViewControllerPresenterInterface: class {
    func viewDidLoad()
    func surf(toPage index: Int)
    func setSurfPageView(from seque: UIStoryboardSegue)
    func onButtonTap(button: UIButton, with gesture: SurfPageViewTapGestures)
    
    var router: SurfViewControllerRouterInterface? {get set}
    var surfPageViewController: SurfPageViewController? {get}
    var interactor: SurfViewControllerInInteractorInterface? {get set};
}

// P -> R
protocol SurfViewControllerRouterInterface: class {
    static func createModule() -> Promise<SurfViewController>
    func surfPageModule(from segue: UIStoryboardSegue) -> Promise<SurfPageViewController>
}

// P -> I
protocol SurfViewControllerInInteractorInterface: class {
    var presenter: SurfViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol SurfViewControllerOutInteractorInterface: class {
}
