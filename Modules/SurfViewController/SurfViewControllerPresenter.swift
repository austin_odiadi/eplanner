//
//  SurfViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 31/03/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation

enum SurfPageViewTapGestures {
    case none, next, close
}

class SurfViewControllerPresenter:  SurfViewControllerPresenterInterface {

    var surfPageViewController: SurfPageViewController? {
        didSet {
            surfPageViewController?.surfPageDelegate = self
        }
    }
    
    var router: SurfViewControllerRouterInterface?
    private weak var view: SurfViewControllerInterface?
    var interactor: SurfViewControllerInInteractorInterface?
    
    init(interactor: SurfViewControllerInInteractorInterface,
         viewController: SurfViewControllerInterface,
         router: SurfViewControllerRouterInterface) {
        
        self.router         = router
        self.interactor     = interactor
        self.view           = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func setSurfPageView(from seque: UIStoryboardSegue) {
        surfPageViewController = router?.surfPageModule(from: seque).value
    }
    
    func onButtonTap(button: UIButton, with gesture: SurfPageViewTapGestures) {
        switch gesture {
        case .none: break
        case .next: surfPageViewController?.next(); break
        case .close: view?.goto(viewController: MainTabBarViewControllerRouter.createModule().value); break
        }
    }
    
    func surf(toPage index: Int) {
        surfPageViewController!.scrollToViewController(index: index)
    }
}

extension SurfViewControllerPresenter: SurfPageViewControllerDelegate {
    func didUpdatePage(count: Int) {
        view?.didUpdatePage(count: count)
    }
    
    func didUpdatePage(index: Int) {
        view?.didUpdatePage(index: index)
    }
}
