//
//  SurfViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 31/03/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit

class SurfViewControllerRouter: SurfViewControllerRouterInterface {

    class func createModule() -> Promise<SurfViewController> {
        
        return firstly { () -> Promise<SurfViewController> in
            let view = BaseViewController.viewControllerWithIdentifier(.surf)
            
            guard let viewController = view.value as? SurfViewController else { return .value(SurfViewController())}
            
            let router: SurfViewControllerRouterInterface           = SurfViewControllerRouter()
            let interactor: SurfViewControllerInInteractorInterface = SurfViewControllerInteractor()
            let presenter: SurfViewControllerPresenterInterface     = SurfViewControllerPresenter(interactor: interactor, viewController: viewController as SurfViewControllerInterface, router: router)
            
            viewController.presenter = presenter
            interactor.presenter = presenter as? SurfViewControllerOutInteractorInterface
            
            return .value(viewController)
        }
    }
    
    func surfPageModule(from segue: UIStoryboardSegue) -> Promise<SurfPageViewController> {
        return SurfPageViewControllerRouter.createModule(from: segue)
    }
}
