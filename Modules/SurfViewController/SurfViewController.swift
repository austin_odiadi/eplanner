//
//  SurfViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 31/03/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

enum SurfState: Int {
    case active = 10001, end
}

class SurfViewController: BaseViewController {
    var surfState: SurfState = .active
    var presenter: SurfViewControllerPresenterInterface?
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    //MARK: - Segue -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        presenter?.setSurfPageView(from: segue)
    }
    
    //MARK: - IBActions -
    @IBAction func closeButton(_ sender: UIButton) {
        presenter?.onButtonTap(button: sender, with: .close)
    }
    
    @IBAction func nextButton(_ sender: UIButton) {
        presenter?.onButtonTap(button: sender, with: surfState == .active ? .next : .close)
    }
    
    //MARK: - Helper Methods -
    @objc func changedPageControlValue() {
        presenter?.surf(toPage: pageControl.currentPage)
    }
    
    func updateSurfingIfNeeded(state: SurfState) {
        if surfState != state {
            
            surfState = state
            PlannerCommons.View.show(view: closeButton, show: surfState == .end ? false : true, animated: true)
            nextButton.setTitle(surfState == .end ? "Go" : "Next", for: .normal)
        }
    }
}

fileprivate extension Selector {
    static let changedPageControlValue = #selector(SurfViewController.changedPageControlValue)
}

extension SurfViewController: SurfViewControllerInterface {

    //MARK: - Initialize -
    func initialize() {
        pageControl.addTarget(self, action: .changedPageControlValue, for: .valueChanged)
    }
    
    //MARK: - Page control -
    func didUpdatePage(count: Int) {
        pageControl.numberOfPages = count
    }
    
    func didUpdatePage(index: Int) {
        pageControl.currentPage = index
        updateSurfingIfNeeded(state: index == pageControl.numberOfPages-1 ? .end : .active)
    }
    
    func goto(viewController view: MainTabBarViewController?) {
        if let mainTabBar = view {
            self.navigationController?.pushViewController(mainTabBar, animated: true)
        }
    }
}
