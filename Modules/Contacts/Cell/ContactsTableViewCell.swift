//
//  ContactsTableViewCell.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 11/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class ContactsTableViewCell: UITableViewCell {

    var checked: Bool = false
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var checkbox: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    @IBAction func checkButtonPushed(_ sender: UIButton) {
        checkButton()
    }
    
    func checkButton() {
        checked = !checked
        checkbox.setImage(UIImage(named: (checked ? "" : "un") + "checked"), for: .normal)
        
    }
}
