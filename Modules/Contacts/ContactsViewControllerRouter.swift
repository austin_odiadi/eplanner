//
//  ContactsViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 11/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation
import PromiseKit

class ContactsViewControllerRouter: ContactsViewControllerRouterInterface {
    
    class func createModule() -> Promise<UINavigationController> {
        
        return firstly { () -> Promise<UINavigationController> in
            let view = BaseViewController.viewControllerWithIdentifier(.contacts)
            
            guard let viewController = view.value as? ContactsViewController else {
                return .value(UINavigationController(rootViewController: ContactsViewController()))
            }
            
            return .value(UINavigationController(rootViewController: viewController))
        }
    }
}
