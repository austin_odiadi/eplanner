//
//  SurfPageViewControllerRouter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 01/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit

class SurfPageViewControllerRouter: SurfPageViewControllerRouterInterface {
    
    class func createModule(from segue: UIStoryboardSegue) -> Promise<SurfPageViewController> {
        return firstly { () -> Promise<SurfPageViewController> in
            
            guard let viewController = segue.destination as? SurfPageViewController else {
                return .value(SurfPageViewController())
            }
            
            let router: SurfPageViewControllerRouterInterface           = SurfPageViewControllerRouter()
            let interactor: SurfPageViewControllerInInteractorInterface = SurfPageViewControllerInteractor()
            let presenter: SurfPageViewControllerPresenterInterface     = SurfPageViewControllerPresenter(interactor: interactor, viewController: viewController as SurfPageViewControllerInterface, router: router)
            
            viewController.presenter = presenter
            interactor.presenter = presenter as? SurfPageViewControllerOutInteractorInterface
            
            return .value(viewController)
        }
    }
    
    func surfModule(view type: PlannerViewController) -> Promise<UIViewController> {
        
        return firstly { () -> Promise<UIViewController> in
            return BaseViewController.viewControllerWithIdentifier(type)
        }
    }
}
