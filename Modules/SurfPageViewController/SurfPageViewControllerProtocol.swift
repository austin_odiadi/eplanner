//
//  SurfPageViewControllerProtocol.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 01/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit

// P -> V
protocol SurfPageViewControllerInterface: class {
    func initialize()
}

// V -> P
protocol SurfPageViewControllerPresenterInterface: class {
    
    func viewDidLoad()
    func numberOfSurfViews() -> Int
    func surfViewLast() -> UIViewController?
    func surfViewFirst() -> UIViewController?
    func indexOf(surfView: UIViewController) -> Int?
    func surfView(atIndex index: Int) -> UIViewController?
    var router: SurfPageViewControllerRouterInterface? {get set}
    var interactor: SurfPageViewControllerInInteractorInterface? {get set}
}

// P -> R
protocol SurfPageViewControllerRouterInterface: class {
    func surfModule(view type: PlannerViewController) -> Promise<UIViewController>
}

// P -> I
protocol SurfPageViewControllerInInteractorInterface: class {
    var presenter: SurfPageViewControllerOutInteractorInterface? {get set} ;
}

// I -> P
protocol SurfPageViewControllerOutInteractorInterface: class {
}

// MARK: - Delegates -

protocol SurfPageViewControllerDelegate: class {
    
    func didUpdatePage(count: Int)
    func didUpdatePage(index: Int)
}
