//
//  SurfPageViewControllerInteractor.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 01/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import Foundation

class SurfPageViewControllerInteractor: SurfPageViewControllerInInteractorInterface {
    var presenter: SurfPageViewControllerOutInteractorInterface?
}
