//
//  SurfPageViewController.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 01/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit

class SurfPageViewController: UIPageViewController {
    var presenter: SurfPageViewControllerPresenterInterface?
    weak var surfPageDelegate: SurfPageViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    //MARK: - DataSource / Delegate -
    fileprivate func setDelegate(){
        dataSource = self
        delegate = self
    }
    
    //MARK: - Helpers -
    
    func next() {
        if let visibleViewController = viewControllers?.first,
            let nextViewController = pageViewController(self, viewControllerAfter: visibleViewController) {
            scrollToViewController(nextViewController, direction: .forward)
        }
    }
    
    func prev() {
        if let visibleViewController = viewControllers?.first,
            let nextViewController = pageViewController(self, viewControllerBefore: visibleViewController) {
            scrollToViewController(nextViewController, direction: .reverse)
        }
    }
    
    fileprivate func scrollToViewController(_ viewController: UIViewController, direction: UIPageViewController.NavigationDirection) {
        setViewControllers([viewController], direction: direction, animated: true, completion: { (finished) -> Void in
            self.surfViewDidChange()
        })
    }
    
    func scrollToNextViewController(){
        if let visibleViewController = viewControllers?.first,
            let nextViewController = pageViewController(self, viewControllerAfter: visibleViewController) {
            scrollToViewController(nextViewController, direction: .forward)
        }
    }
    
    func scrollToViewController(index newIndex: Int) {
        if let first = viewControllers?.first, let index = presenter?.indexOf(surfView: first) {
            scrollToViewController((presenter?.surfView(atIndex: index))!, direction: newIndex >= index ? .forward : .reverse)
        }
    }
    
    fileprivate func surfViewDidChange() {
        if let first = viewControllers?.first, let index = presenter?.indexOf(surfView: first) {
            surfPageDelegate?.didUpdatePage(index: index)
        }
    }
}

extension SurfPageViewController: SurfPageViewControllerInterface {
    
    //MARK: - Initialzer -
    func initialize() {
        if let firstView = presenter?.surfViewFirst() {
            scrollToViewController(firstView, direction:.forward)
        }
        
        surfPageDelegate?.didUpdatePage(count: presenter?.numberOfSurfViews() ?? 0)
        self.setDelegate()
    }
}

extension SurfPageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    //MARK: - DataSource -
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = presenter?.indexOf(surfView: viewController)
            else {
                return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0
            else {
                return presenter?.surfViewLast()
        }
        
        guard (presenter?.numberOfSurfViews())! > previousIndex
            else {
                return nil
        }
        
        return presenter?.surfView(atIndex: previousIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = presenter?.indexOf(surfView: viewController)
            else {
                return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = (presenter?.numberOfSurfViews())!
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex
            else {
                return presenter?.surfViewFirst()
        }
        
        guard orderedViewControllersCount > nextIndex
            else {
                return nil
        }
        
        return presenter?.surfView(atIndex: nextIndex)
    }

    //MARK: - DataSource -
    
    func pageViewController(_ pageViewController: UIPageViewController,didFinishAnimating finished: Bool,previousViewControllers: [UIViewController],transitionCompleted completed: Bool) {
        surfViewDidChange()
    }
}
