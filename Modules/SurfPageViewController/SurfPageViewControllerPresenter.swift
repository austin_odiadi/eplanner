//
//  SurfPageViewControllerPresenter.swift
//  UPlanner
//
//  Created by Mykyta Danilov on 01/04/2019.
//  Copyright © 2019 Mykyta Danilov. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit

class SurfPageViewControllerPresenter: SurfPageViewControllerPresenterInterface {
    
    var router: SurfPageViewControllerRouterInterface?
    private weak var view: SurfPageViewControllerInterface?
    var interactor: SurfPageViewControllerInInteractorInterface?
    
    lazy var viewControllersInOrderOfPresentation: [UIViewController] = {
        var orderedViewController: [UIViewController] = [UIViewController]()
        
        // The view controllers in order of appearance
        [.first, .second, .third, .fourth].forEach { orderedViewController.append((router?.surfModule(view: $0).value)!) }
        return orderedViewController
    }()
    
    init(interactor: SurfPageViewControllerInInteractorInterface,
         viewController: SurfPageViewControllerInterface,
         router: SurfPageViewControllerRouterInterface) {
        
        self.router         = router
        self.interactor     = interactor
        self.view           = viewController
    }
    
    func viewDidLoad() {
        view?.initialize()
    }
    
    func numberOfSurfViews() -> Int {
        return viewControllersInOrderOfPresentation.count
    }
    
    func surfView(atIndex index: Int) -> UIViewController?  {
        if viewControllersInOrderOfPresentation.count > index {
            return viewControllersInOrderOfPresentation[index]
        }
        
        return nil
    }
    
    func surfViewLast() -> UIViewController? {
        return viewControllersInOrderOfPresentation.last ?? nil
    }
    
    func surfViewFirst() -> UIViewController? {
        return viewControllersInOrderOfPresentation.first ?? nil
    }
    
    func indexOf(surfView: UIViewController) -> Int? {
        return viewControllersInOrderOfPresentation.index(of: surfView)
    }
}
